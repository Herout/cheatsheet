package App::q;
use 5.26.0;
use warnings;

use DBI;
use Getopt::Long;
use YAML::XS;
use Try::Tiny;
use Path::Tiny;
use File::Find::Rule;
use Moo;
use open OUT  => ":encoding(win-1250)" ;
use CLI::Osprey (added_order => 1, desc => 'This application queries tag metadata for my notes', 
usage_string => 'This application indexes folders given in --corpus option and provides simple 
interface to query metadata of notes.');

option sqlite => (
    is => 'ro',
    default => 'dbname=d:\temp\tips.sqlite'
);

option tags => (
    is => 'ro',
    format => 's',
    repeatable => 1,
    short => 't',
    default => sub {[]},
    doc => 'tags to query (for subcommand tags only)',    
);

option sql => (
    is => 'rw',
    format => 's',    
    doc => 'sql to run against corpus; mutually exclusive with --tags',
);


option corpus => (
    is => 'ro',
    format => 's',
    short => 'c',
    doc => 'dir to index, default is c:\gitlab\cheatsheet\content',    
    default => sub { [ 'c:\gitlab\cheatsheet\content', 'c:\BI_Domain\bimain\env' ] },
    repeatable => 1,
);


option formatter => (
    is => 'rw',
    format => 's',
    short => 'f',
    doc => 'commant to format',
);




has dbh => ( is => 'rw' );

sub _connect {
    my ($self) = @_;
    return $self->dbh if $self->dbh;
    my $db = $self->sqlite;
    my $dbh = DBI->connect("dbi:SQLite:$db", {
          RaiseError => 1,
    });
    $self->dbh($dbh);
    return $dbh;
}

sub build_corpus {
    my ($self) = @_;    
    my $corpus = path($self->corpus);    

    #use YAML::XS 'Load', 'LoadFile';
    #
    #my $config = LoadFile('./config.yaml');
    #my @facets = sort(($config->{'q'}{'post-facets'}||[])->@*);
        
    
    my $dbh = $self->_connect;

    $dbh->begin_work;
        
    $dbh->do(<<~"SQL");
       CREATE TABLE articles (
          url,
          title,
          date,
          filename
       )
    SQL

    $dbh->do(<<~'SQL');
       CREATE TABLE article_tag ( filename, tag )
    SQL

    $dbh->do(<<~"SQL");
       CREATE VIEW _ ( url, title, date, filename, tag  ) AS
       SELECT url, title, date, a.filename, tag
       FROM articles a
       JOIN article_tag at ON a.filename = at.filename
    SQL

    $dbh->commit;    

    $dbh->begin_work;

    my $qs = join ', ', map '?', qw(url title date filename);
    my $sth_a = $dbh->prepare(<<~"SQL");
      INSERT INTO articles (
         url, title, date, filename
      ) VALUES ($qs)
    SQL
    
    my $sth_a_t = $dbh->prepare(<<~'SQL');
          INSERT INTO article_tag (filename, tag) VALUES (?, ?)
    SQL

    foreach my $corpus_dir ($self->corpus->@*) {
        my @files = map { path($_) } File::Find::Rule->file()->name(qr/[.](md|sql)$/i)->in("$corpus_dir");
        my $cfg_file = path($corpus_dir)->sibling('q.yaml');
        my $c = { base_url => $corpus_dir . '\\' };
        $c = Load $cfg_file->slurp if ($cfg_file->is_file);
        
        
        FILES:
        for my $file (@files) {      
          my $cnt = 0;
          my $yaml = "";
          my $h = $file->openr;          
          my $i=0;
        LINES:
          while (my $line = <$h>) {
            chomp ($line);        
            last LINES if $i == 0 and $line !~ /^---\s*$/;
            $cnt ++ if $line =~ /^---\s*$/;
            last LINES if $cnt > 1;
            $yaml .= "$line\n";
            $i++;
          }
                
          my $data;      
          
          try {
            $data = YAML::XS::Load($yaml);
            $data->{tags} ||= [];
          } catch {
            warn "error processing $file: $_\n";
            next FILES
          };
          
          $data->{url} = $c->{base_url} . $file->basename;
          $sth_a->execute(
             $data->{url}, $data->{title}, $data->{date}, $file,
             map $data->{$_}
          );
          $sth_a_t->execute($file, $_) for @{$data->{tags}};
        }

    }    
    $dbh->commit;
    return $self;
}

sub _query {
    my ($self) = @_;
    my $sql = $self->sql;
    my $formatter = $self->formatter;    
    my $dbh = $self->_connect;
    my $sth = $dbh->prepare($sql || die "pass some SQL yo\n");
    $sth->execute;

    my $func = eval <<~'PERL'
    sub {
       no warnings 'uninitialized';
       my %r = %{$_[0]};
    PERL
     . ($formatter || 'join "\t", map $r{$_}, sort keys %r') . "\n}";
    die $@ if $@;

    for my $row (@{$sth->fetchall_arrayref({})}) {
       say $func->($row)
    }
    return $self;
}


sub run {
    my ($self) = @_;
    
    if (scalar $self->tags->@*) {
        die 'can not use `--tags` along with `--sql`' if $self->sql;
        my $tags = join ', ', map { uc "'$_'" } $self->tags->@*;
        my $tagscnt = scalar $self->tags->@*;
        $self->formatter(q/
                my $extra = (80 - length($r{url}) );
                $extra = 0 if $extra < 0;
                $r{url} . (' ' x $extra ). ' : ' . $r{title};
        /);
        my $sql = "
            SELECT url, title FROM (
            SELECT 
                url, title,
                row_number() OVER win1 as cnt 
            FROM _ 
            WHERE upper(tag) in ($tags) 
            WINDOW win1 AS (PARTITION BY url)
            ) a
            WHERE cnt = $tagscnt             
        ";
        $self->sql($sql);
    }
    
    say $self->osprey_man unless $self->sql;
    
    my $sqlite = path((split '=', $self->sqlite)[1] // '.') ;
    my $age = $sqlite->is_file ? time - $sqlite->stat->atime : 99999;
    if ( $age > 8 * 60 * 60 ) {
        $sqlite->remove if $sqlite->is_file;
        $self->build_corpus 
    } else {
    
    }
    
    $self->_query;
    say "-" x 80;
    say "LeftCtrl+Click on URL to open in Firefox under conemu.";
    
}

subcommand clean => sub {
    my ($self) = @_;
    my $file = (split /=/, $self->sqlite)[1];
    die "Where is the file? Set --sqlite to something reasonable." unless $file;
    die "Where is the file? Set --sqlite to something reasonable [$file]." unless path($file)->is_file;
    path($file)->remove;    
}, (
    desc => 'Deletes sqlite database, so that it will be recreated'
);
subcommand show => sub {

    say <<~"SQL";
    --------------------------------------------------------------------
    CREATE TABLE articles (
          url,
          title,
          date,
          filename
    )
    --------------------------------------------------------------------
    CREATE TABLE article_tag ( filename, tag )
    --------------------------------------------------------------------
    CREATE VIEW _ ( url, title, date, filename, tag  ) AS
       SELECT url, title, date, a.filename, tag
       FROM articles a
       JOIN article_tag at ON a.filename = at.filename
    --------------------------------------------------------------------
    SQL
}, desc => 'show defitions of metadata tables';

1;
