# Git intro

--

## Repository a integrace se serverem

### jak vytvořit prázdnou lokální repository

--

```
mkdir c:\bi_domain\gitTest
cd c:\bi_domain\gitTest
git init
```

--


### jak vytvořit vzdálenou repository a naklonovat jí

--

[git CE v O2](http://gitce.cz.o2/)

- new project
- `git clone http://gitce.cz.o2/BIDEV/bimain.git`

--

### jak vytvořit první commit; základy orientace v gitu

--

stage files: 

- `git add <soubor>` 
- `git add *`

--

commit staged files: 
- `git commit -m"message"` 
- `git commit`

--

**tip: cmdline editor**

```
git config --global core.editor micro
```

[micro](https://github.com/zyedidia/micro) 

--

```
git log
git log --oneline
git log --graph --abbrev-commit --decorate --format=format:"%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)" --all
```

--

**tip: git alias**

```
git config --global alias.lg "log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"
git config --global alias.ll "log --oneline"
git config --global alias.lw "log --pretty=format:'%H | %cn | %s'"
```

potom
```
git lg
```

--

### git pull a git push

--

- `pull` : dej mi změny ze serveru
- `push` : nahraj změny na server
- tracking branch vs remote branch vs soukromá branch


---

## Mírně pokročilí: branching a tagging

"References Make Commits Reachable"

--

![branch_a_tag](/images/gitbasics/branch_a_tag.png)

- sha256 hash
- tag
- branch

--

### vytvoření branche a paralelní existence kódu

--

```
git status
git branch develop
git checkout develop
```

--

## merge jako nástroj přenosu kódu

--

```
:: kde jsem?
git status
:: merge odkud?
git merge develop
```

--

## merge a řádkový konflikt

- co to je?
- jak to vypadá v notepadu?
- jak se vrátit zpátky, pokud se merge nepovedl?

--

## git mergetool

```
git config --global merge.tool winmerge
git config --replace --global mergetool.winmerge.cmd "\"d:\portable\PortableApps\WinMergePortable\WinMergePortable.exe\" -e -u -dl \"Base\" -dr \"Mine\" \"$LOCAL\" \"$REMOTE\" \"$MERGED\""
git config --global mergetool.prompt false
git config --global --replace diff.tool winmerge
git config --replace --global difftool.winmerge.cmd "\"d:\portable\PortableApps\WinMergePortable\WinMergePortable.exe\" -e -u -dl \"Base\" -dr \"Mine\" \"$REMOTE\" \"$LOCAL\""
git config --global mergetool.prompt false
git config --global --replace diff.tool winmerge
git config --global --replace merge.tool winmerge
```

---

# Informační zdroje

- <http://think-like-a-git.net/>
- <https://datasift.github.io/gitflow/IntroducingGitFlow.html>

---

# Toolset

--

## grafický klient?

**spíše ano**
- [fork](https://git-fork.com/windows) - free
- [tortoise git](https://tortoisegit.org/) - free

--

## grafický klient?

**spíše ne**
- [source tree](https://www.sourcetreeapp.com/) - chce účet na BitBucket
- [git kraken](https://www.slant.co/topics/2089/~best-git-clients-for-windows) - komerční licence

--

... a mnoho dalších...

- <https://git-scm.com/download/gui/windows>
- <https://www.slant.co/topics/2089/~best-git-clients-for-windows>

--

## mergetool, difftool?

- [winmerge](http://winmerge.org/?lang=en) - pozor, verze `2.16` je padavá, poslední funční je `2.14`
- [kdiff3](http://kdiff3.sourceforge.net/) - nemá autodetekci kódové stránky
- [p4merge](https://www.perforce.com/products/helix-core-apps/merge-diff-tool-p4merge) - zadarmo, ale "komerční", a nemá autodetekci znakové sady

---


---

# FORK intro

---

# Příště ...

- git flow
- protected branches