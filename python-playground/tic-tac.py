import os

class Point:
    def __init__(self,x,y):
        self.x = x
        self.y = y
    def __repr__(self): return f'Point(x={self.x},y={self.y})'
    def __add__(self,other): 
        return Point(x = self.x + other.x, y=self.y + other.y)

class Board:
    def __init__(self, board = [ ['.', '.', '.'], ['.', '.', '.'], ['.', '.', '.'] ]):
        self.board  = board
        self.last_move = Point(-1,-1)
        
    def __str__(self):
        ret =  "   | 0 | 1 | 2\n"
        ret += "---+---+---+---\n"
        for i, row in enumerate(self.board):
            move_row = " <===" if i == self.last_move.y else ""
            ret += f" {str(i)} | " + ' | '.join(self.board[i]) + move_row + '\n'
        
        move_col = []
        for i in range(3):
            c = '^^^ ' if i == self.last_move.x else '    '
            move_col.append(c)
        ret += '    ' + ''.join(move_col) + '\n'
        
        return ret
        
    def token(self, point):        
        if point.x<0 or point.y<0                                   : raise IndexError
        if point.x>=len(self.board[0]) or point.y>=len(self.board)  : raise IndexError        
        return self.board[point.y][point.x]
        
    def move(self, point,token):
        if not isinstance(point,Point)                  : raise ValueError(f'can not move to what is not a point: {point}')
        if not point.y < len(self.board)                : raise ValueError(f'can not move outside board (y axis): {point}')
        if not point.x < len(self.board[0])             : raise ValueError(f'can not move outside board (x axis): {point}')
        if not (token == 'x' or token == 'o')           : raise ValueError(f'players can only be "x" and "o": {token}')
        if not self.token(point) == '.'                 : raise ValueError(f'point={point}, already taken by {self.token(point)}')
        self.board[point.y][point.x] = token
        self.last_move = point
        return self
    
    def check_position(self,point):                
        token = self.token(point)
        
        # if checked position is empty, not a win
        if token == '.': return False
        
        checked_space = [
            [ Point(-1,0),  Point(1,0)  ], # above and below
            [ Point(-1,-1), Point(1,1)  ], # diagonal from top left to right
            [ Point(-1,1),  Point(1,-1) ], # diagonal from top right to left
            [ Point(0,-1),  Point(0,1)  ], # horizon
        ]
                
        for i,neighbours in enumerate(checked_space):            
            pos1, pos2 = point+neighbours[0], point+neighbours[1]
            try:
                # this can fail, method 'token' raises exception if position is out of board
                neighbour1, neighbour2 = self.token(pos1), self.token(pos2)            
            except IndexError:
                # if out od board, this is impossible position, and definitively not a win for a side
                continue
            else:
                #win if all three positions contains same token
                if token == neighbour1 == neighbour2: return True
        
        # by default, not a win
        return False
    
    def check_state(self):
        # if number of empty squares is zero at the end of the check, no more moves to play => draw
        num_empty = 0
        for y,row in enumerate(self.board):
            for x,token in enumerate(self.board[y]):
                if token == '.': num_empty += 1 
                if self.check_position(Point(x,y)): return f'side playing for [{token}] wins!'   
        if num_empty == 0: return 'draw'
        return None

if __name__=='__main__':
    side = 'x'
    b = Board()
    
    
    while True:        
        # sides are switched after each move
        side = 'o' if side == 'x' else 'x'
        move = None        
        os.system('cls')
        print (str(b))
        print ("---------------")
        
        while move==None: 

            print (f"   side to play: {side}, enter coords")
            y = input("    row   =")
            x = input("    column=")        
            
            try:                
                to = Point(int(x),int(y))   # can raise ValueError
                b.move(to, side)            # can raise ValueError or IndexError
            except ValueError as e:
                print("========> Try again: ", e)
            except IndexError as e:
                print("========> Try again: ", e)
            else:
                move = to
                
        result = b.check_state()
        if result: break
        
    print("==========================================")
    print (result)
    print ("BOARD STATE")
    print (str(b))