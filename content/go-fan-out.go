package main

import (
        "fmt"
        "sync"
        "time"
)

type Item struct {
        ID            int
        Name          string
        PackingEffort time.Duration
}

// producer, produkuje data do t� doby, dokud nen� v�echno odebran�
// k tomu pou��v� signaliza�n� kan�l done, na kter� zapisuj� konzumenti dat (????)
// vrac� kan�l a sou�asn� spou�t� rutinu kter� produkuje data
func PrepareItems(done <-chan bool) <-chan Item {
        items := make(chan Item)
        itemsToShip := []Item{
                Item{0, "Shirt", 1 * time.Second},
                Item{1, "Legos", 1 * time.Second},
                Item{2, "TV", 5 * time.Second},
                Item{3, "Bananas", 2 * time.Second},
                Item{4, "Hat", 1 * time.Second},
                Item{5, "Phone", 2 * time.Second},
                Item{6, "Plates", 3 * time.Second},
                Item{7, "Computer", 5 * time.Second},
                Item{8, "Pint Glass", 3 * time.Second},
                Item{9, "Watch", 2 * time.Second},
        }
        go func() {
                for _, item := range itemsToShip {
                        select {
                        case <-done:				// sign�l konce
                                return				
                        case items <- item:			// m�m co poslat? tak to po�lu
                        }
                }
                close(items)
        }()
        return items
}

func PackItems(done <-chan bool, items <-chan Item, workerID int) <-chan int {
        packages := make(chan int)
        go func() {
                for item := range items {
                        select {
                        case <-done:
                                return
                        case packages <- item.ID:
                                time.Sleep(item.PackingEffort)
                                fmt.Printf("Worker #%d: Shipping package no. %d, took %ds to pack\n", workerID, item.ID, item.PackingEffort / time.Second)
                        }
                }
                close(packages)
        }()
        return packages
}

func merge(done <-chan bool, channels ...<-chan int) <-chan int {
        var wg sync.WaitGroup

        
		wg.Add(len(channels))					// kolik pracant� m�me, a �ek�me na n�?        
		outgoingPackages := make(chan int) 		// v�stupn� kan�l na kter� prov�d�me fan-in
        
		//	deklarujeme funkci kterr� pro ka�d�ho workera provede JOIN
		// multiplexer, tohle je to m�sto kde se prov�d� fan-in
		multiplex := func(c <-chan int) {
                defer wg.Done()
                for i := range c {
                        select {
                        case <-done:
                                return					// konec pokud jsi na konci
                        case outgoingPackages <- i:		// fan-in
                        }
                }
        }
        
		// tady se fan-in spust�...
		for _, c := range channels {
                go multiplex(c)
        }
		// ... a sou�asn� se spust� rutina, kter� �ek� na dojezd, a uzav�e v�stupn� kan�l kdy� jsme na konci
        go func() {
                wg.Wait()
                close(outgoingPackages)
        }()
		
		// vrac�me v�stupn� kan�l
        return outgoingPackages
}

func main() {
        done := make(chan bool)
        defer close(done)

        start := time.Now()

        // konstruktor, dej mi kan�l kter� �tu
		items := PrepareItems(done)
		
		// konstuktor, ka�d� worker m� vlastn� kan�l na kter� pos�l� svoje v�stupn� data. 
		// Here, you can see that  we setup the additional �workers� to fan-out the problem. 
		// The key part is that 
		// - they all reference the same channel of Items to read from, and 
		// - they each return a channel that they write to, which in the end we will �fan-in� by merging these channels back into one single channel
        workers := make([]<-chan int, 4)
        for i := 0; i<4; i++ {
                workers[i] = PackItems(done, items, i)	// vrac� v�stupn� kan�l a spou�t� gorutinu
        }

        // fan-in - sjednocen� v�stup� zase na jeden kan�l
		// proto�e je tady range, jdeme p�es obsah kan�lu dokud nedojde k jeho uzav�en� - blocking
		numPackages := 0
        for range merge(done, workers...) {
                numPackages++
        }

        fmt.Printf("Took %fs to ship %d packages\n", time.Since(start).Seconds(), numPackages)
		// tady kon��me, a proto�e jsme m�li DEFER na clode(done), dojde k odesl�n� sign�lu, �e kon��me
		// ten si seberou v�ichni konzumenti/producenti dat, a "pokorn� se slo��"
}