---
title:  'go channels gotchas'
tags:   [go, chan channel, concurrency]   
---

# go channels gotchas

## get from channel (blocking)

```
line, more := <-iQue			// receive line		
if ! more {
    // empty channel
    return
}
```

## gotcha: N producers, 1 receiver

**producer:** `"panic: send on closed channel"`
- mějme jednoho konzumenta a N producentů
- v takové situaci **nemůže** producent uzavřít kanál, protože jiná produce instance padne na zápis na uzavřený kanál
- signalizaci konce je tedy nutné řešit jinak; například tak, že:
    - orchestrující kód spustí konzumenta a producenty; poznačí si počet producentů
    - každý producent má k dispozici dva kanály, jeden na který píše data, a druhý, na který píše informaci že skončil
    - ani jeden z těchto kanálů ale _není_ producentem uzavírám
    - místo toho orchestrující kód ví, že musí dostat signál **done** od všech producentů, a zná jejich počet
    - jakmile všichni producenti potvrdí, že jsou **done**, orchestrující rutina provede `close` na jejich kanálech
    

**consumer: prázdné tokeny na vstupním kanále**
- zatím jsem se nesnažil rozkoukat, co je příčinou; pravda ale je, že se na kanále který čte konzument mohou objevit
  "dummy" záznamy
- důvodem je fakt, že vstupní kanál je **buffered**
    
Reads on closed channels behave differently:

- If there are items yet to be popped off, the popping off happens as usual.
- When the queue is empty and closed, the read will not block.
- Reads on empty and closed channels will return the “zero-value” of the channel item type.

- je proto žádoucí umět odlišit "dummy" záznam od reálné věty, například:

```
type datLine struct {
	isNotDummy   bool           // false je default; pro věty, které pushneš na kanál, nastav true
	content      []byte
}
```

nebo:
```
item, valid := <- queue
// at this point, "valid" is:
//    true  => the "item" is valid
//    false => "queue" was closed, "item" is only a zero-value
```

nebo
```
for item := range queue {
    // process
}
// at this point, all items ever pushed into the queue has been processed,
// and the queue has been closed

```

nebo
```
var ok, valid bool
select {
    case item, valid = <- queue:
        ok = true
    default:
        ok = false
}
// at this point:
//   ok && valid  => item is good, use it
//   !ok          => channel open, but empty, try later
//   ok && !valid => channel closed, quit polling
```

**errors; návratová hodnota**
- z paralelně spuštěných gorutin nelze odchytit chybovou hodnotu
- je ale možné použít signalizační kanál k odlití chybové hodnoty, a s tou pak pracovat

```
func producer(id int, inChan chan datLine, outChan chan datLine, doneChan chan error){
	var err error
	for {
		line, more := <-inChan			// receive line
        /* work on the line which could result in err */
		outChan <- line                 // send the line further
		if ! more {			            // no more inputs, channel closed
			if err != nil {             // check if we got error
				err = fmt.Errorf("producer [%d]: %s", id, err)
			}
			doneChan <- err			
			return
		}
	}
}
```

## get from ONE OF channels 

blocking operace, která čeká na hodnotu z alespoň jednoho kanálu

```
select {
case strChan <- "value":
case otherChan <- "other value":
}
```

## blocking get with timeout

```
select {
case strChan <- "value":
case <-time.After(5 * time.Second):
}
```

## nonblocking get

```
var ok bool
select {
    case item = <- queue:
        ok = true
    default:
        ok = false
}
// at this point, "ok" is:
//   true  => item was popped off the queue (or queue was closed, see below)
//   false => not popped, would have blocked because of queue empty
```