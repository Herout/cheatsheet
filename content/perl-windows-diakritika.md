---
title: "Perl a diakritika v konzili"
tags: [perl, encoding, diakritika]
---

# Jak na české znaky na konzoli

```perl
#! /usr/bin/perl
use v5.24;                        # implicit strict
use utf8;                         # source is utf-8 encoded
use warnings;                     # simple sanity
use warnings qw( FATAL utf8 );    # fatalize all unicode warnings

=pod

Tento návod ukazuje, jak korektně na Windows zacházet z konzole s diakritikou

=cut

# non core moduly
use Encode::Locale;
use Encode qw/decode encode/;
use Text::Undiacritic qw/undiacritic/;
use Win32;

# zjisti kódové stránkly, použité konzolí
my $encoding_in  = 'CP'. Win32::GetConsoleCP();
my $encoding_out = 'CP'. Win32::GetConsoleOutputCP();
my $encoding_sys = 'CP'. Win32::GetACP();

# vyžehli ARGV
# https://www.perl.com/pub/2012/04/perlunicookbook-decode-argv-as-local-encoding.html/
# zde se používá modul Encode::Locale
# z důvodů, kterým nerozumím, je tento recept odlišný od postupu dekódování STDIN (data jsou zakódovaná jinak)
@ARGV = map { decode(locale => $_, 1) } @ARGV;
foreach my $arg (@ARGV) {    
    say undiacritic($arg);    
}

# zapiš české znaky na konzoli, nakódované správným způsobem
# na encode nelze použít literál, padá na chybě Modification of a read-only value
# takže bohužel proměnná...
my $msg = "něco mi zadej: ";
say encode($encoding_out, $msg, 1);

# načti češtinu zpátky (a odstraň její diakritiku, pokud chceš)
my $comment_txt;
while (! $comment_txt){
    $comment_txt = <STDIN>;
    chomp ($comment_txt);            
    $comment_txt = undiacritic (decode($encoding_in, $comment_txt, Encode::FB_CROAK));
    say "Comment: $comment_txt";
}
```