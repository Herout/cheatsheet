execute "x0548834".GetQueryPerformance ('TGT_ACCS_METH_PARAM_900_PHONE_POOL_PCR');

REPLACE MACRO "x0548834".GetQueryPerformance (

/* This macro returns the list of queryband items and list of performance parameters      */
/* testing macro - DBC.dbqlogtbl and dbc.dbqlsqltbl is used instead of sys_reporting_view */
/* tables                                                                                 */
     c_procedure         VARCHAR(256) -- procedura. Povinn� parametr
     --,c_logdate           DATE         
    -- ,c_username       VARCHAR(256) -- datab�ze logovan� do QB v jeho SET (parametr USR)
    --,c_step            VARCHAR(256) 
    --,c_jid             VARCHAR(256) 
    --,c_wid             VARCHAR(256) 
)
AS
( SELECT
         a.USERNAME
       , a.collecttimestamp
       , getquerybandvalue(a.QueryBand, 0, 'OBJ')     AS QB_Object
       , getquerybandvalue(a.QueryBand, 0, 'USR')     AS QB_Username
       , getquerybandvalue(a.QueryBand, 0, 'STP')     AS QB_Step
       , getquerybandvalue(a.QueryBand, 0, 'JID')     AS QB_Jid
       , getquerybandvalue(a.QueryBand, 0, 'WID')     AS QB_Wid
       , a.starttime
       ,( CAST((CAST(a.firstresptime AS DATE) - DATE '1970-01-01')*86400 
              +(EXTRACT(HOUR FROM a.firstresptime)*3600) 
              +(EXTRACT(MINUTE FROM a.firstresptime)*60) AS DECIMAL(18)) 
              +(EXTRACT(SECOND FROM a.firstresptime)) 
        ) 
         - 
        ( CAST((CAST(a.starttime AS DATE) - DATE '1970-01-01')*86400 
              +(EXTRACT(HOUR FROM a.starttime)*3600) 
              +(EXTRACT(MINUTE FROM a.starttime)*60) AS DECIMAL(18)) 
              +(EXTRACT(SECOND FROM a.starttime)) 
        )                                             AS ResponseTime
         -- Following parameters are NOT part of acceptance
       , a.SpoolUsage/(1.00*1024*1024*1024)           AS Spool_GB
       , a.MaxAmpCPUTime                              AS "MaxAmpCPU (Sec)"
       , a.AMPCPUTime    /(HASHAMP()+1)               AS "AvgAmpCPU (Sec)"
       , a.MaxAmpCPUTime - a.AMPCPUTime/(HASHAMP()+1) AS "Amp Skew diff (Sec)"
         -- Value of the "CPU Skew Impact" is part of checks during acceptance process:
       ,( CASE
              WHEN ( a.AMPCPUTime/(HASHAMP()+1)) = 0
                  THEN                             0
              ELSE (a.MaxAmpCPUTime-(a.AMPCPUTime/(HASHAMP()+1)))/(a.AMPCPUTime/(HASHAMP()+1))+1
          END 
        )                                              AS "CPU Skew Impact"
         -- Value of the "IO Skew Impact" is part of checks during acceptance process:
       ,( CASE
              WHEN (a.TotalIOCount/(HASHAMP()+1)) = 0
                  THEN 0
              ELSE (a.MaxAmpIO-(a.TotalIOCount/(HASHAMP()+1)))/(a.TotalIOCount/(HASHAMP()+1))+1
          END 
        )                                           AS "IO Skew Impact"
       , CAST(x.sqltextinfo AS VARCHAR(2000)) AS SQL_Text_substr

FROM
         dbc.dbqlogtbl a
         JOIN
                  dbc.DBQLSqltbl x
                  ON
                           x.queryid = a.queryid
WHERE
         a.ERRORCODE                        IS NOT NULL
         AND a.queryband                    IS NOT NULL
         AND getquerybandvalue(a.QueryBand, 0, 'OBJ')                     = :c_procedure
         AND CAST(A.COLLECTTIMESTAMP AS DATE FORMAT 'YYYY-MM-DD')         = /* :c_logdate */ current_date
         AND SQL_Text_substr NOT LIKE '%create multiset table%'
         AND SQL_Text_substr NOT LIKE '%create set table%'
         AND SQL_Text_substr NOT LIKE '%create table%'
         AND SQL_Text_substr NOT LIKE '%drop table%'
         AND SQL_Text_substr NOT LIKE '%begin transaction%'
         AND SQL_Text_substr NOT LIKE '%end transaction%'
         AND SQL_Text_substr NOT LIKE '%call%'
         AND SQL_Text_substr NOT LIKE '%end transaction%'
         AND SQL_Text_substr NOT LIKE '%select 1%'
         AND SQL_Text_substr NOT LIKE '%UPDATE CZ_PRD_META.PROC_STATUS%'
         AND SQL_Text_substr NOT LIKE '%SET QUERY_BAND%'
         AND SQL_Text_substr NOT LIKE '%DATABASE%'
         AND SQL_Text_substr NOT LIKE '%FROM DBC.Tables WHERE ((TRIM(BOTH  FROM DatabaseName ))%'
-- If you want to use commented items in filtering records, do not forget to uncomment the list of parameters of macro and reload 
-- definition of the macro.         
--         AND getquerybandvalue(a.QueryBand, 0, 'USR') = :c_username         
--         AND getquerybandvalue(a.QueryBand, 0, 'STP') = COALESCE(:c_step, getquerybandvalue(a.QueryBand, 0, 'STP'))
--         AND getquerybandvalue(a.QueryBand, 0, 'JID') = COALESCE(:c_jid, getquerybandvalue(a.QueryBand, 0, 'JID'))
--         AND getquerybandvalue(a.QueryBand, 0, 'WID') = COALESCE(:c_wid, getquerybandvalue(a.QueryBand, 0, 'WID'))
ORDER BY
         QB_Step asc, a.CollectTimeStamp DESC ; );