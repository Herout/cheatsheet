---
title:  'Temporální dotazy: Teradata'
tags:   [teradata,temporal,query]
---

# Temporální datový set

Ahoj, tak pro informaci, s VALIDTIME sloupcem lze normálně pracovat, jen se musí použít uvozovky
Tzn. BEGIN("VALIDTIME") místo BEGIN(VALIDTIME)

# UNTIL_CHANGED a UNTIL_CLOSED

| konstrukt       | kde použít        |
| --------------- | ----------------- |
| `UNTIL_CLOSED`  | transaction time  |
| `UNTIL_CHANGED` | valid time        |


```
LOCKING ROW FOR ACCESS 
NONSEQUENCED TRANSACTIONTIME 
SELECT 
    CASE WHEN tt_end_dttm IS NOT Until_Closed THEN 'historicky' ELSE 'live' END AS stav, 
    Count(*) 
FROM ep_iptv.DAILY_SESSIONS 
GROUP BY 1
```