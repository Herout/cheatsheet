---
title: "Python & Slack integration"
tags: [python, slack]
---

# Slack

## Tvorba aplikace na Slack API

- na adrese [https://api.slack.com/apps](https://api.slack.com/apps) zalo�it novou appku
- appka se svazuje s konkr�tn�m development workspace

## P�id�len� pr�v appce

- ka�d� appka m� definovan� "scope", kter� ��k� jak� akce m��e prov�d�t na workspace
- scopes se p�id�luj� na str�nce `OAuth & Permissions` v sekci `Scopes`
- pokud m� b�t appka schopn� pos�lat notifikace na kan�l, m�la by m�t p�inejmen��m scope `chat:write`

## Instalace do workspace

- na str�nce `OAuth & Permissions` se appka mus� zaregistrovat pro dan� workspace (Install App, Reinstall App)
- appka dostane p�id�len� token (`Bot User OAuth Access Token`) - ten si schovej

## P�izv�n� na channel

- n�sledn� je pot�eba appku p�izvat na channel, do kter�ho m� m�t mo�nost ps�t
- to ud�l� na Slack frontendu kde Appku do kan�lu p�id� (vlevo dole je seznam apps, a je tam tebou vytvo�en� appka)

# Python

- dependencies: `pip install slackclient`

## Send message

[rtfm](https://github.com/slackapi/python-slackclient#basic-usage-of-the-web-client)

```
from slack import WebClient
from slack.errors import SlackApiError


slackClient = WebClient(token="<tv�j token>")
slackChannel = '#n�jak�_kan�l'

def slackme(message):        
    try:
        slack.chat_postMessage(channel=slackChannel, text=message)
    except SlackApiError as e:
        pass
```

## 