---
title:  'pd access view'
tags:   [ pd, gtl, view ]
---

```
replace view %Code% as
locking row for access
select 
.set_value(MySeparator, "") 
.foreach_item(Columns,"//columns\n")
%-10.3:MySeparator%%Code%

.set_value(MySeparator, ",") 
.next
from %Model.Code%.%Code%
;
```