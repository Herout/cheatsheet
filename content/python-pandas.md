```
```

# Pandas

```
import numpy as np
import pandas as pd
from pandas import Series, DataFrame
```


```
# DataFrame z dict-of-arrays
dict = {'col1': [1, 2], 'col2': [3, 4]}
frame = DataFrame.from_dict(data = dict)

df1 = DataFrame(data=np.arange(9).reshape((3,3)), index = ['SF','LA','NY'],columns=['pop','size','year'])
```

```
# access row
LArow = df1.loc['LA']

# drop row
dfWithoutSF = df1.drop('SF')

# drop col
dfWithoutPop = df1.drop('pop', axis=1)

# filtrace hodnot; vrac� SLICE dan�ho df, je jeho kopii!
filtered_df = df1[df1['pop'] > 3]

# true/false matice ... v�ude kde je bu�ka > 10 dej true, a vra� matici
df1 > 10
```

## data alignment

```
# sou�et dvou s�ri� vrac� NaN tam, kde nejsou hodnoty
# to plat� jak na �rovni ��dek, tak na �rovni sloupc�
df1 = DataFrame( np.arange(4).reshape(2,2), columns = list('AB'), index=['NYC','LA'] )
df2 = DataFrame( np.arange(9).reshape(3,3), columns = list('ABC'), index=['NYC','LA','PRA'] )

da1+df2

# sloupec C nen� v df1, a ��dek PRA nen� v df1 => NaN
#         A       B       C
# LA      5.0     7.0     NaN
# NYC     0.0     2.0     NaN
# PRA     NaN     NaN     NaN

df1.add(df2, fill_value=0)

# 
#           A     B     C
# LA      5.0     7.0     5.0
# NYC     0.0     2.0     2.0
# PRA     6.0     7.0     8.0

```

## sorting & ranking

```
ser1 = Series(range(3), index=list('CBA'))
ser1.sort_index()        # vrac� s�rii v po�ad� ABC
ser1.sort_values()       # vrac� s�rii v po�ad� podle hodnot, tj CAB
ser1.sort_index().rank() # vrac� ranking pou�it� pro sort
```

ranking lze pou��t i na DF jako maticovou operaci

```
df1 = DataFrame(data = randn(9).reshape(3,3), columns=list('ABC'), index = np.arange(3))
df1.rank()
df1['B'].sort_value().head()
```

## summary stats

- `.describe` - souhrnn� statistika pro data frame

- min, max, std, mean, pct_change()

```
arr =arr = np.array([[np.nan, 2, 1], [1, np.nan, 6]])
df1 = DataFrame(arr, index=list('AB'),columns=['one','two','three'])

df1.loc['A'].sum()      # sou�et v ��dku
df['one'].sum()         # sou�et v sloupci
```

- `.idxmin()` - vrac� s�rii indexu (��dek) s nejmen�� hodnotou pro ka�d� sloupec
- `.cumsum()` - vrac� cumulative sum pro ka�d� sloupec

## korelace a kovariance

```
from IPython.display import YouTubeVideo
```

## datab�ze

```
SQL_Query = pd.read_sql_query(
    '''select
    product_name,
    product_price_per_unit,
    units_ordered,
    ((units_ordered) * (product_price_per_unit)) AS revenue
    from tracking_sales''', conn)

df = pd.DataFrame(SQL_Query, columns=['product_name','product_price_per_unit','units_ordered','revenue'])
```

## konverze datov�ch typ� 

```
import pandas as pd
df['cnt'] = df['cnt'].apply(pd.to_numeric)
```

```
# konverze stringu na timestamp; utc=True tam je proto�e m�me rozd�ln� tomez�ny na vstupu
df['datetime'] = pd.to_datetime(df['authored_date'], utc=True)
df['date']= df['datetime'].dt.date
df.head()
```

## join

```python
# inner join ... dohledej si st�vaj�c� runplan
df_inner = pd.merge(left = q, right = calendars, how='inner', left_on='LD_STREAM', right_on='STREAM_NAME', suffixes='XL')
df_inner = pd.merge(left = df_inner, right = calendars, how='inner', left_on='TRF_STREAM', right_on='STREAM_NAME', suffixes='XT')
df_inner.head()
```