---
title:  'Git Behind A Proxy Server - Guide'
tags:   [git, proxy]   
---
[Source](https://www.freecodecamp.org/forum/t/git-behind-a-proxy-server/13187 "Permalink to Git Behind A Proxy Server - Guide")

# Git Behind A Proxy Server - Guide

**Use-cases**

You might need to modify `git` commands that access (to update and read from) remote repositories if your internet access is through a [proxy server][1].

Proxy servers are common in college and business type environments.

You can [locate your proxy settings][2] from your browser's settings panel.

## Using Proxy with Git

Once you have obtained the proxy settings (server URL, port, username and password); you need to configure your git as follows:
      
    git config --global http.proxy http://internet-proxy-s1.cz.o2:8080/
    
For local repositories (repos on local network) set something like:

    no_proxy=.my.company,localhost,127.0.0.1,::1
    
Alternatively, set system variable `no_proxy`.

You would need to replace ``, ``, ``, `` with the values specific to your proxy server credentials. These fields are optional. For instance, your proxy server might not even require `` and ``, or that it might be running on port 80 (in which case `` is not required).

Once you have set these, your `git pull`, `git push` or even `git fetch` would work properly.

## When Not to Use

You should not have to use `git` commands with proxy settings, if either of the following happens

* Your system administrator or corporate policy does not allow you to access remote `git` repositories from GitHub, BitBucket etc.
* The remote repository in question is not in your machine, but it's within the internal network. An instance of GitLab deployed internally at your company is a good example.

## Unset Proxy Settings

You added an entry to your git config file by mistake. You can manipulate the both the global and per-repository config files using git config.

To find out if you added the proxy entry to the global or local config files run this from the console:

    git config -l --global | grep http  # this will print the line if it is in the global file
    git config -l | grep http # this will print the line if it is in the repo config file

Then to remove all the http.proxy entries from either the global or the local file run this:

    git config --global --unset-all http.proxy # to remove it from the global config
    git config --unset-all http.proxy  # to remove it from the local repo config file

I hope this helps.


## Resources

You may use the following for further reading on this:

    [1]: https://en.wikipedia.org/wiki/Proxy_server
    [2]: http://www.wikihow.com/Change-Proxy-Settings
    [3]: http://stackoverflow.com/questions/11499805/git-http-proxy-setting

  