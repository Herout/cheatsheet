---
title:  'Git Behind A Proxy Server - Guide'
tags:   [vmware, linux, proxy]   
---

# apt-get
* Nastavit síť jako NAT
* Nastavit na firefoxu proxy autodetect : http://wpad.cz.o2/wpad.dat
* Konfigurovat apt_get:

```
sudo su
sudo echo 'Acquire::http::Proxy "http://internet-proxy-s1.cz.o2:8080/";' > /etc/apt/apt.conf.d/proxy.conf
sudo apt-get update
```


# docker


`vim ~/.docker/config.json`

add
``
{
 "proxies":
 {
   "default":
   {
     "httpProxy": "http://internet-proxy-s1.cz.o2:8080/",
     "httpsProxy": "http://internet-proxy-s1.cz.o2:8080/",
     "noProxy": "*.cz.o2"
   }
 }
}
```
