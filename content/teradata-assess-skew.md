---
title: 'assess skew'
tags:  [ teradata, space, skew, permspace ]
---

# Odhad šikmosti při změně primárního indexu

```sql
LOCKING ROW FOR access
SELECT
        (100 - ( AVG(CAST(CNT AS DECIMAL(28,0))) / MAX (CAST(CNT AS DECIMAL(28,0)))*100) ) AS SkewFactor 
FROM   ( SELECT 
                HASHAMP(HASHBUCKET(HASHROW(
                -- start - sem je potřeba uvést výčet sloupců které budou na PI; oddělit čárkou
                BILL_STM_ID
                -- end   - seznam sloupců
                )))       AS AMPs 
              , COUNT (*) AS CNT
       FROM     <databaze>.<tabulka>
       GROUP BY 
                1 ) 
       AS a;

```

# Šikmost tabulky

```
/*
----------------------------------------------------------------------
    ROUGH GUIDELINE
----------------------------------------------------------------------
Table size	                                Acceptable skew
----------------------------------------------------------------------
< 1000 records	                            skew is not checked
between  1000 and 100.000 records	        skew < 40
between 100.001 and 2.000.000 records	    skew < 20
between 2.000.001 and 100.000.000 records	skew < 5
above 100.000.001 records	                skew < 3
----------------------------------------------------------------------
*/

SELECT
         TSIZE.DatabaseName
       , TSIZE.TableName
       , TDEF.CreateTimeStamp    AS Created
       , TDEF.LastAlterTimeStamp AS LastAltered
       , TDEF.AccessCount
       , TDEF.LastAccessTimeStamp                                    AS LastAccess
       , SUM(TSIZE.CurrentPerm)                                      AS CurrentPerm
       , SUM(TSIZE.PeakPerm)                                         AS PeakPerm
       , (100 - (AVG(TSIZE.CurrentPerm)/MAX(TSIZE.CurrentPerm)*100)) AS SkewFactor
FROM
         DBC.TableSize TSIZE
         INNER JOIN
                  DBC.Tables TDEF
                  ON
                           TSIZE.DatabaseName  = TDEF.DatabaseName
                           AND TSIZE.TableName = TDEF.TableName

WHERE tsize.tablename = 'EET_VAS' and tsize.databasename = 'EP_LND'

GROUP BY
         1
       , 2
       , 3
       , 4
       , 5
       , 6

```

# Permspace přes ampy

```sql
SELECT vproc
,currentperm
FROM dbc.tablesize
WHERE databasename = 'xxx'
AND tablename = 'yyy';
```