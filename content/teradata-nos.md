# Teradata Native Object Store

- Dostupný od Vantage v17
- Podporuje S3 a Azure BLOB Storage (nejsem si jistý zda to zahrnuje i Azure DataLake v2)
- Přístup na
  - JSON - nutno používat implicitní CAST, protože nemá definované schéma
  - CSV - nutno používat implicitní CAST, protože nemá definované schéma
  - Parquet - opírá se o definici uvnitř souboru, deklaruješ datové typy jako součást DDL
- soubory musí být buď UTF-8-BOM, nebo LATIN
- loader je obecně řádkově orientovaný; v podstatě: pro JSON musí celý record být na jednom řádku (no pretty printing!)
- pro Parquet existuje jednoznačný mapping na datové typy Teradaty
- Limity na velikost payloadu
- Komprese
  - GZIP (JSON/CSV)
  - Snappy (Parquet)
- Přístup via
  - foreign tables - proti katalogu (nový typ tabulky)
  - `READ_NOS` - adhoc dotaz
- Dá se poslat normální select, dá se provést materializace přes `CREATE TABLE AS .... WITH DATA`
- Dá se použít
    - `path filtering` 
        - výraz který krájí název souboru na proměnné
        - **na úrovni DDL** pro foreign table, ale dá se použít i v `READ_NOS`
        - WHERE se pak odkazuje na proměnné (`WHERE $path.$siteno='....'`)
        - proměnné se pak dají použít vlastně kdekoliv (jako sloupec ve view, apod)
    - `payload filtering` - musí se načíst všechno, a pak se filtruje

## Prerekvizity: Teradata side

```
GRANT EXECUTE FUNCTIONon TD_SYSFNLIB.READ_NOS to user;
GRANT CREATEAUTHORIZATION on user to user;

-- Allows user to create a CSV schemafor CSV foreign tables that need a user-defined schema.
GRANT CREATE DATASET SCHEMA ON SYSUDTLIBTO user;

-- Allows user to create function mapping forhiding credentials.
GRANT CREATE FUNCTIONON user to user
```

### Authorization object

- An authorization object is used to control who can access an external object store.

```
--Authorization object used by one user
CREATE AUTHORIZATION authorization_object
AS INVOKER TRUSTED
USER 'YOUR-ACCESS-KEY-ID' PASSWORD 'YOUR-SECRET-ACCESS-KEY';

--Authorization object shared by a group of users
CREATE AUTHORIZATION authorization_object
AS DEFINER TRUSTED
USER 'YOUR-ACCESS-KEY-ID' PASSWORD 'YOUR-SECRET-ACCESS-KEY';
```

### Function mapping

- You can use function mapping to hide credentials in an authorization object.
- After you create the functionmapping, you can use the mapped authorization object
  in queries and **avoid exposing your credentials**.

```
CREATE FUNCTION MAPPING READ_NOS_FM FOR READ_NOS
EXTERNAL SECURITY DEFINER TRUSTED authorization_object
USING BUFFERSIZE,SAMPLE_PERC,ROWFORMAT,RETURNTYPE,HEADER,MANIFEST,LOCATION,STOREDAS,FULLSCAN,ANY IN TABLE;
```

**s mapování**

```
SELECT TOP 2 LOCATION FROM READ_NOS_FM (
    ON ( SELECT cast (NULL as JSON) )
    USING LOCATION ('YOUR-STORAGE-ACCOUNT')
    RETURNTYPE ('NOSREAD_RECORD')
) AS D;
```

**bez mapování**

```
SELECT TOP 2 LOCATION FROM READ_NOS_FM (
    ON ( SELECT cast (NULL as JSON) )
    USING
        LOCATION   ('YOUR-STORAGE-ACCOUNT')
        ACCESS_ID  ('USER')
        ACCESS_KEY ('KEY')
) AS D;
```

## CSV přes foreign table

- externí tabulka má dva sloupce - `payload` a `location`
  - `payload` - CSV data jako datový typ CSV (string??)
  - `location` - kde to leží, varchar

| co      | jak                                                                                        |
| ------- | ------------------------------------------------------------------------------------------ |
| záhlaví | `SELECT DISTINCT * FROM DATASET_KEYS (ON (SELECT payload FROM foreign_table)) AS csvKeys;` |
| data    | `SELECT max(cast(payload..sloupec) as INTEGER) FROM foreign_table;`                        |

## JSON přes foreign table

```
CREATE FOREIGN TABLE riverflow_json, 
EXTERNAL SECURITY DEFINER TRUSTED authorization_object (
    Location VARCHAR(2048) CHARACTER SET UNICODE CASESPECIFIC, 
    Payload JSON INLINE LENGTH 32000 CHARACTER SET UNICODE
) USING ( LOCATION('YOUR-STORAGE-ACCOUNT') );
```

| co      | jak                                                                                        |
| ------- | ------------------------------------------------------------------------------------------ |
| záhlaví | `SELECT DISTINCT * FROM JSON_KEYS (ON (SELECT payload FROM foreign_table)) AS _keys;`      |
| data    | `SELECT max(cast(payload.sloupec) as INTEGER) FROM foreign_table;`                         |

## Parquet přes external table

- není potřeba dekódovat payload, navenek se to tváří jako "normální tabulka"