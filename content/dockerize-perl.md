---
tags:   [docker, perl]
title:  'docker tips'
---

# Create docker image

* create empty dir
* `vim Dockerfile`

Typical Perl related Dockerfile

```
# a little on heavy side, but oh well
FROM		perl:latest
MAINTAINER	jan-herout jan.herout@gmail.com

# install Perl packages
RUN cpanm --notest https://gitlab.com/Herout/app-mdd-generator-dist/raw/master/dist/App-MDD-Generator-0.01.tar.gz


WORKDIR metadata
```

# Run docker image

Na windows:

```
docker  run -i --mount "type=bind,src=d:/temp,dst=/root/metadata" jahero/mdgen-0.1.0 uname -u
docker  run -i --mount "type=bind,src=d:/temp,dst=/root/metadata" jahero/mdgen-0.1.0 which pdm_32
docker  run -i --mount "type=bind,src=d:/temp,dst=/root/metadata" jahero/mdgen-0.1.0 mdgen.pl --log "screen,debug" --metadata /root/metadata/metadata add_models --models /root/metadata/modely/SRC_ADR.pdm 
```


# Spuštění ubuntu terminálu na windows

```
docker run -it ubuntu
```