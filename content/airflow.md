# Úvod

## Airflow: Základní vlastnosti

### Co je to Airflow

[Official web](https://airflow.apache.org/)

> Airflow is a platform created by community to programmatically author, schedule and monitor workflows


```mermaid
graph TB

    subgraph "Extrakt A"
        SniffA[SniffA] --> ValidA[ValidA] -->LoadA[LoadA]-->ConsoA[ConsoA]-->EnrichA[EnrichA]
    end
    subgraph "Extrakt B"
        SniffB[SniffB] --> ValidB[ValidB] -->LoadB[LoadB]-->ConsoB[ConsoB]-->EnrichB[EnrichB]
    end

end
```


### Tech stack

- Python !!!
- MySQL/Postgress
- AirFlow + Celery
- Message queue: RabbitMQ / Redis (ten ale neperzistuje => risky business)

### Pipeline

- Pipeline authoring is done in Python
- “Configuration as code” is a principle we stand by for this purpose. 

> For example, a pipeline could consist of tasks like reading archived logs from S3, 
> creating a Spark job to extract relevant features, indexing the features using Solr 
> and updating the existing index to allow search. To automate this pipeline and run it 
> weekly, you could use a time-based scheduler like Cron by defining the workflows in Crontab.

### DAGs

DAG = (directed acyclic graphs) have the following properties:
- _Scheduled_: each job should run at a certain scheduled interval
- _Mission_ _critical_: if some of the jobs aren’t running, we are in trouble
- _Evolving_: as the company and the data team matures, so does the data processing
- _Heterogenous_: the stack for modern analytics is changing quickly, and most companies run multiple systems that need to be glued together

### Scheduling and triggers

> The Airflow scheduler monitors all tasks and all DAGs, and triggers the task instances whose dependencies have been met. 
> Behind the scenes, it spins up a subprocess, which monitors and stays in sync with a folder for all DAG objects it may contain, 
> and periodically (every minute or so) collects DAG parsing results and inspects active tasks to see whether they can be triggered.

### Architecture components

- The job definitions, in source control.
- A rich CLI (command line interface) to test, run, backfill, describe and clear parts of your DAGs.
- A web application, to explore your DAGs definition, their dependencies, progress, metadata and logs. 
- A metadata repository, typically a MySQL or Postgres database that Airflow uses to keep track of task job statuses and other persistent information.
- An array of workers, running the jobs task instances in a distributed fashion.
- Scheduler processes, that fire up the task instances that are ready to run.

### Operators

- Action: něco udělej
- Transfer: něco přesuň
- Sensors: sniffer (blokuje dokud se nesplní nějaké kritérium)

### Executors

- Sequential Executor: Each task is run locally (on the same machine as the scheduler) in its own python subprocess. They are run sequentially which means that only one task can be executed at a time. It is the default executor.
- Local Executor: It is the same as the sequential executor except that multiple tasks can run in parallel. It needs a metadata database (where DAGs and tasks status are stored) that supports parallelism like MySQL. Setting such a database requires some extra work since the default configuration uses SQLite.
- Celery Executor: The workload is distributed on multiple celery workers which can run on different machines. It is the executor you should use for availability and scalability.

### CLI examples

- `airflow test DAG_ID TASK_ID EXECUTION_DATE`. 
    - Allows the user to run a task in isolation, without affecting the metadata database, or being concerned about task dependencies. 
    - This command is great for testing basic behavior of custom Operator classes in isolation.
- `airflow backfill DAG_ID TASK_ID -s START_DATE -e END_DATE`
    - Performs backfills of historical data between START_DATE and END_DATE without the need to run the scheduler. 
    - This is great when you need to change some business logic of a currently-existing workflow and need to update historical data. (Note that backfills do not create DagRun entries in the database, as they are not run by the SchedulerJob class).
- [Další příklady](https://airflow.apache.org/docs/stable/tutorial.html#running-the-script)

### Folder structure

```
airflow                  # the root directory.
├── dags                 # root folder for all dags. files inside folders are not searched for dags.
│   ├── my_dag.py        # my dag (definitions of tasks/operators) including precedence.
│   └── ...
├── logs                 # logs for the various tasks that are run
│   └── my_dag           # DAG specific logs
│   │   ├── src1_s3      # folder for task specific logs (log files are created by date of run)
│   │   ├── src2_hdfs
│   │   ├── src3_s3
│   │   └── spark_task_etl
├── airflow.db           # SQLite database used by Airflow internally to track status of each DAG.
├── airflow.cfg          # global configuration for Airflow (this can be overriden by config inside the file.)
└── ...
```

### DAGs - implementace

### Import balíčků

```
# airflow related
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator# other packages
from datetime import datetime
from datetime import timedelta
```

### Set up default args

```
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2018, 9, 1),
    'email_on_failure': False,
    'email_on_retry': False,
    'schedule_interval': '@daily',
    'retries': 1,
    'retry_delay': timedelta(seconds=5),
}
```

### Definice DAG, tasků, operátorů

Tři tasky, které načítají data a ukládají je na S3 a HDFS. Jsou to python funkce, parametry dostývají přes `**args` a `**kwargs`.

```
def source1_to_s3():
 # code that writes our data from source 1 to s3
 
def source2_to_hdfs(config, ds, **kwargs):
 # code that writes our data from source 2 to hdfs
 # ds: the date of run of the given task.
 # kwargs: keyword arguments containing context parameters for the run.
 
def source3_to_s3():
 # code that writes our data from source 3 to s3
```

Instance DAG objektu, denní běh,

```
dag = DAG(
  dag_id='my_dag', 
  description='Simple tutorial DAG',
  default_args=default_args)
```

Tasky

```
config = get_hdfs_config()

src1_s3 = PythonOperator(
  task_id='source1_to_s3', 
  python_callable=source1_to_s3, 
  dag=dag)

src2_hdfs = PythonOperator(
  task_id='source2_to_hdfs', 
  python_callable=source2_to_hdfs, 
  op_kwargs = {'config' : config},
  provide_context=True,
  dag=dag
)

src3_s3 = PythonOperator(
  task_id='source3_to_s3', 
  python_callable=source3_to_s3, 
  dag=dag)
  
spark_job = BashOperator(
  task_id='spark_task_etl',
  bash_command='spark-submit --master spark://localhost:7077 spark_job.py',
  dag = dag)
  
# setting dependencies
src1_s3 >> spark_job
src2_hdfs >> spark_job
src3_s3 >> spark_job
```

### Aktivace

- spuštění webu: `airflow webserver`
- spuštění scheduleru : `airflow scheduler`

When setting up Apache Airflow with the **celery executor** to use a distributed architecture, you have to launch a bunch of these processes and other services:

- A metadata database (MySQL): it contains the status of the DAG runs and task instances.
- Airflow web server: a web interface to query the metadata to monitor and execute DAGs.
- Airflow scheduler: checks the status of the DAGs and tasks in the metadata database, create new ones if necessary and sends the tasks to the queues.
- A message broker (RabbitMQ): it stores the task commands to be run in queues.
- Airflow Celery workers: they retrieve the commands from the queues, execute them and update the metadata.

[setting-apache-airflow-cluster](http://site.clairvoyantsoft.com/setting-apache-airflow-cluster/)


### GOTCHAs a best practices

**Development**
- *nepoužívat* mnoho různých typů operátorů; maximální zjednodušení, použití toho co opravdu funguje (bash + skripty)
    - Developers must spend time researching, understanding, using, and debugging the Operator they want to use. 
    - This also means that each time a developer wants to perform a new type of task, they must repeat all of these steps with a new Operator. 
    - And, as we found at Bluecore, Operators are often buggy. 
    - Developer after developer moved a previously-working workflow over to Airflow only to have it brought down by an issue with an Airflow Operator itself.
- *distribuovaná povaha* + závislosti
    - This means that all Python package dependencies from each workflow will need to be installed on each Airflow Worker for Operators to be executed successfully. 
- doporučuje se používat `DockerOperator` jako "jediný operátor - protože s sebou nese všechny závislosti; pro naše účely to možná nehraje roli...

> In lieu of a growing list of functionality-specific Operators, we believe that there should be 
> a single, bug-free Operator that would be able to execute any arbitrary task. 

**Operations**
- Use Refresh on the web interface: 
    - Any changes that you make to the DAGs or tasks don’t get automatically updated in Airflow. 
    - You’ll need to use the Refresh button on the web server to load the latest configurations.
- Be careful when changing start_date and schedule_interval: 
    - As already mentioned, Airflow maintains a database of information about the various runs. 
    - Once you run a DAG, it creates entries in the database with those details. 
    - Any changes to the start_date and schedule_interval can cause the workflow to be in a weird state that may not recover properly.
- Start scheduler separately: 
    - It seems obvious to start the scheduler when the web server is started. 
    - Airflow doesn’t work that way for an important reason — to provide separation of concerns when deployed in a cluster environment where the Scheduler and web server aren’t necessarily onthe same machine.
- [crontab like scheduling](https://en.wikipedia.org/wiki/Cron)

### Závislosti napříč DAGs

> Airflow does not allow to set up dependencies between DAGs explicitly, 
> but we can use Sensors to postpone the start of the second DAG 
> until the first one successfully finishes.

> To configure the sensor, we need the identifier of another DAG (we will wait until that DAG finishes). 
> Additionally, we can also specify the identifier of a task within the DAG (if we want to wait for a single task). 
> If we want to wait for the whole DAG we must set external_task_id = None.


```python
from datetime import datetime
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.sensors.external_task_sensor import ExternalTaskSensor

dag = DAG('dependency_dag', description='DAG with sensor', schedule_interval='* * * * *',
          start_date=datetime(2019, 7, 10))

#-------------------
# aka SNIFFER
#-------------------
sensor = ExternalTaskSensor(task_id='dag_sensor', external_dag_id = 'another_dag_id', external_task_id = None, dag=dag, mode = 'reschedule')

#-------------------
# task a jeho závislost
#-------------------
task = DummyOperator(task_id='some_task', retries=1, dag=dag)
task.set_upstream(sensor)
```

> In the default configuration, the sensor checks the dependency status every minute.

### Idea

- L0: 
    - každý objekt (spíš asi SYSTÉM!!!) má vlastní DAG, protože má vlastní schedule; tady nám nevadí minutový delay na scheduleru
    - **tohle se musí dobře promyslet**! koncepčně jeden extrakt = jeden DAG, ale pokud se každý DAG musí **ručně nasadit na frontendu**, je to docela problém.
- L1/L2: každý "mart" má vlastní DAG, a je udržován jako celek; tady by nám VADIL minutový delay mezi DAGy !

*Q:* 
- jak efektivně dostat do konfigurace závislosti (generátor) ??? Přece to nebudeme udržovat ručně? 
    - nějaký vlastní "parser" který tam chybějící kousky závislosti doplní?
    - nějaký "systém maker" který z jednotlivých kousků (které se budou udržovat zvlášť) slepí celý DAG? (nejspíš tudy!)
    - deployment a jeho časovost.... výše jmenovaný bod by asi tenhle problém vyřešil. 
        




### Další zdroje

- [summary: apache-airflow-celery-workers](https://www.sicara.ai/blog/2019-04-08-apache-airflow-celery-workers)
- [blog post](https://hackersandslackers.com/data-pipelines-apache-airflow/)
- [air flow operators](https://airflow.apache.org/docs/stable/_api/airflow/operators/index.html)
    - [MsSQL](https://airflow.apache.org/docs/stable/_api/airflow/operators/mssql_operator/index.html)
    - [JDBC](https://airflow.apache.org/docs/stable/_api/airflow/operators/jdbc_operator/index.html)
    - [bash](https://airflow.apache.org/docs/stable/_api/airflow/operators/bash_operator/index.html)
    - [branching](https://airflow.apache.org/docs/stable/_api/airflow/operators/branch_operator/index.html)

**Resources:**  dole je seznam z [blogu](https://towardsdatascience.com/a-definitive-compilation-of-apache-airflow-resources-82bc4980c154)
- Official doc
    - [doc](https://airflow.apache.org/)
    - [git](https://github.com/apache/airflow)
    - [wiki](https://cwiki.apache.org/confluence/display/AIRFLOW/Airflow+Home)
    - [integration](https://airflow.apache.org/docs/stable/integration.html)
- Intro
    - [airbnb, desc](https://medium.com/airbnb-engineering/airflow-a-workflow-management-platform-46318b977fd8)
    - [paypal](https://medium.com/@r39132/apache-airflow-grows-up-c820ee8a8324)
    - [concepts](https://medium.com/@dustinstansbury/understanding-apache-airflows-key-concepts-a96efed52b1a)
    - [airflow 101](https://blog.insightdatascience.com/airflow-101-start-automating-your-batch-workflows-with-ease-8e7d35387f94)
    - [DAGs and dependencies](https://www.mikulskibartosz.name/using-sensors-in-airflow/); [docs!!](https://airflow.apache.org/docs/stable/howto/operator/external.html)
    - [Managing dependencies](https://www.astronomer.io/guides/managing-dependencies/)
    
    - [video, best practices](https://www.youtube.com/watch?v=dgaoqOZlvEA)
    - [video, ETL](https://www.youtube.com/watch?v=tcJhSaowzUI)
    - [video, a practical intro](https://www.youtube.com/watch?v=cHATHSB_450)
    - [video, development of workflows](https://www.youtube.com/watch?v=XJf-f56JbFM)
    - [video, pipelining and scheduling](https://www.youtube.com/watch?v=60FUHEkcPyY)
    
- Industry
    - [Uber](https://eng.uber.com/managing-data-workflows-at-scale/)
    - [Twitter](https://eng.uber.com/managing-data-workflows-at-scale/)
    - [RobinHood](https://robinhood.engineering/why-robinhood-uses-airflow-aed13a9a90c8)
- Distributed deployment
    - [celery&airflow](https://blog.sicara.com/using-airflow-with-celery-workers-54cb5212d405)
    - [clusters](https://stlong0521.github.io/20161023%20-%20Airflow.html)
- testing
    - [Data’s Inferno: 7 Circles of Data Testing Hell with Airflow](https://medium.com/wbaa/datas-inferno-7-circles-of-data-testing-hell-with-airflow-cef4adff58d8)
    - [Testing in Airflow Part 1 — DAG Validation Tests, DAG Definition Tests and Unit Tests](https://blog.usejournal.com/testing-in-airflow-part-1-dag-validation-tests-dag-definition-tests-and-unit-tests-2aa94970570c)
- best practices
    - [We’re All Using Airflow Wrong and How to Fix It](https://medium.com/bluecore-engineering/were-all-using-airflow-wrong-and-how-to-fix-it-a56f14cb0753)



## Celery

- worker, message consumer
- jako frontu používá buď redis, nebo rabbitmq


## RabbitMQ

[RabbitMQ](https://www.rabbitmq.com/#features)

- RabbitMQ is a message broker: it accepts and forwards messages.

- Scale: based on configuration and resources, the ballpark here is around 50K msg per second.
- Persistency: both persistent and transient messages are supported.
- One-to-one vs one-to-many consumers: both.
- There are some managed services that allow you to use it as a SaaS but it’s not part of the native major cloud provider stack. 
- RabbitMQ supports all major languages, including Python, Java, .NET, PHP, Ruby, JavaScript, Go, Swift, and more.

**Complex routing**


- RabbitMQ uses Advanced Message Queuing Protocol (AMQP) which can be configured to use SSL, additional layer of security.
- RabbitMQ takes approximately 75% of the time Redis takes in accepting messages.
- RabbitMQ supports priorities for messages, which can be used by workers to consume high priority messages first.
- There is no chance of loosing the message if any worker crashes after consuming the message, which is not the case with Redis.
- RabbitMQ has a good routing system to direct messages to different queues.
- RabbitMQ might be a little hard to maintain, hard to debug crashes.
- node-name or node-ip fluctuations can cause data loss, but if managed well, durable messages can solve the problem.


## Kafka

- Scale: can send up to a millions messages per second.
- Persistency: yes.
- One-to-one vs one-to-many consumers: only one-to-many (seems strange at first glance, right?!).

Kafka was created by Linkedin in 2011 to handle high throughput, low latency processing. As a distributed streaming platform, Kafka replicates a publish-subscribe service. It provides data persistency and stores streams of records that render it capable of exchanging quality messages.
Kafka has managed SaaS on Azure, AWS, and Confluent. They are all the creators and main contributors of the Kafka project. Kafka supports all major languages, including Python, Java, C/C++, Clojure, .NET, PHP, Ruby, JavaScript, Go, Swift and more.

**Large data amounts**

## Redis

[redis](https://redis.io/topics/introduction)

- Scale: can send up to a million messages per second.
- Persistency: basically, no – it’s an in-memory datastore.
- One-to-one vs one-to-many consumers: both.

**Short-lived Messages**