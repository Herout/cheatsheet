---
title:  'CPAN behind proxy'
tags:   [perl, proxy, cpan]   
---

# CPAN pod VMWare image v O2 lokální síti

**Firefox**
* Nastavit síť jako NAT
* Nastavit na firefoxu proxy autodetect : http://wpad.cz.o2/wpad.dat
* Konfigurovat apt_get:

```
sudo su
sudo echo 'Acquire::http::Proxy "http://internet-proxy-s1.cz.o2:8080/";' > /etc/apt/apt.conf.d/proxy.conf
sudo apt-get update
```


**cpanm, carton**

```
Based on your comments do you have http_proxy ENV variable set in your shell?

$ env | grep http_proxy

If not then set it with your proxy settings and re-try perlbrew install:

 $ export http_proxy="http://internet-proxy-s1.cz.o2:8080/"
 $ export HTTP_PROXY="http://internet-proxy-s1.cz.o2:8080/"
 $ perlbrew install perl-5.12.1
```

* shared folders jsou na adrese `/mnt/hgfs`

