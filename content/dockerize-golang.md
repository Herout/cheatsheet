---
tags:   [docker, golang]
title:  'docker tips'
---

[source](https://www.cloudreach.com/blog/containerize-this-golang-dockerfiles/)

# Multistage build

```
FROM golang:alpine as builder
RUN mkdir /build 
ADD . /build/
WORKDIR /build 
RUN go build -o main .
FROM alpine
RUN adduser -S -D -H -h /app appuser
USER appuser
COPY --from=builder /build/main /app/
WORKDIR /app
CMD ["./main"]
```

* Notice two FROM directives in this Dockerfile. 
* The first one we label as “builder”, using it to build the application. 
* We then use a second FROM, this time pulling from base “alpine” (very lightweight!) and copy our built executable from that environment to this new environment. 

This results in an image size MUCH smaller than the previous! 

# Build from scratch, static linking

```
##### build static executable
FROM golang:alpine as builder
RUN mkdir /build 
ADD . /build/
WORKDIR /build 
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

##### final image
FROM scratch
COPY --from=builder /build/main /app/
WORKDIR /app
CMD ["./main"]
```

* The final Docker image will only contain this one single executable in this example, **without the baggage of a container operating system.**