---
title:  Jazyk Go a aplikace s vlastním příkazovým řádkem
tags:   [go, cli, repl]
---

[Source](https://www.root.cz/clanky/jazyk-go-a-aplikace-s-vlastnim-prikazovym-radkem/ "Permalink to Jazyk Go a aplikace s vlastním příkazovým řádkem")

# Jazyk Go a aplikace s vlastním příkazovým řádkem

## 7. Instalace balíčku **go-prompt**

[go-prompt](https://github.com/c-bata/go-prompt)

## 8. Základní klávesové zkratky používané knihovnou **go-prompt**

#### Příkazy pro přesuny kurzoru

| ----- |
| Klávesa |  Význam |  
| Ctrl+B |  přesun kurzoru na předchozí znak |  
| šipka doleva |  přesun kurzoru na předchozí znak |  
|   |    |  
| Ctrl+F |  přesun kurzoru na další znak |  
| šipka doprava |  přesun kurzoru na další znak |  
|   |    |  
| Ctrl+A |  přesun kurzoru na začátek řádku |  
| Home |  přesun kurzoru na začátek řádku |  
|   |    |  
| Ctrl+E |  přesun kurzoru na konec řádku |  
| End |  přesun kurzoru na konec řádku | 

#### Mazání textu, práce s kill ringem

Pro přesun části textu v rámci editovaného řádku se například v BASHi používá takzvaný _kill ring_, do něhož se smazaný text uloží. Pro vložení takto smazaného textu do jiné oblasti se používá operace nazvaná _yank_ (odpovídá _paste_). V případě knihovny **go-prompt** jsou v současné verzi implementovány pouze příkazy pro smazání části textu, nikoli však příkaz _yank_ pro jeho vložení na jiné místo (tato operace je však plánována do dalších verzí, popravdě ji není příliš složité doimplementovat):

| ----- |
| Klávesa |  Význam |  
| Ctrl+D |  smaže jeden znak na pozici kurzoru (pokud je ovšem na řádku nějaký obsah, jinak typicky ukončí vstup či celou aplikaci) |  
| Delete |  smaže jeden znak na pozici kurzoru |  
|   |    |  
| Ctrl+H |  smaže jeden znak před kurzorem |  
| Backspace |  smaže jeden znak před kurzorem |  
|   |    |  
| Ctrl+W |  smaže předchozí slovo |  
|   |    |  
| Ctrl+K |  smaže text od pozice kurzoru do konce řádku |  
| Ctrl+U |  smaže text od začátku řádku do pozice kurzoru | 

#### Práce s historií dříve zadaných příkazů

Uživatelé mají k dispozici i historii zapsaných příkazů. K jejich vyvolání slouží několik klávesových zkratek vypsaných v následující tabulce. Prozatím zde však nenajdete zkratky pro vyhledávání v historii příkazů; tuto funkcionalitu je však možné v případě potřeby doprogramovat a pravděpodobně se s ní setkáme v další verzi knihovny **go-prompt**:

| ----- |
| Klávesa |  Význam |  
| Ctrl+P |  průchod historií – předchozí text/příkaz |  
| šipka nahoru |  průchod historií – předchozí text/příkaz |  
|   |    |  
| Ctrl+N |  průchod historií – následující text/příkaz |  
| šipka dolů |  průchod historií – předchozí text/příkaz | 

Poznámka: k historii příkazů mají vývojáři přístup přes rozhraní poskytované knihovnou **go-prompt**. Proto je například možné z historie odstranit neplatné příkazy, získat celou historii zapsaných příkazů a uložit ji do externího souboru atd. 

## 9. Přečtení řádku s příkazy funkcí **Input**

Nyní si ukážeme základní funkcionalitu, kterou nám knihovna **go-prompt** nabízí. Bude se jednat o nejjednodušší možnou aplikaci, která pouze přečte dva textové údaje z příkazového řádku. Aplikace je napsána takovým způsobem, že jsou podporovány všechny výše zmíněné editační příkazy, ovšem nikoli historie příkazového řádku (nemáme totiž žádný objekt, který by historii držel). Základem je použití funkce **Input**:
    
    
    login := prompt.**Input**("Login: ", completer)
    password := prompt.**Input**("Password: ", completer)

Této funkci je nutné předat referenci na takzvaný _completer_, s jehož významem se setkáme v navazujících kapitolách.

[Zdrojový kód][26] příkladu vypadá následovně:
    
    
    package **main**
     
    import "github.com/c-bata/go-prompt"
     
    func **completer**(in prompt.Document) []prompt.Suggest {
            return []prompt.Suggest{}
    }
     
    func **main**() {
            login := prompt.Input("Login: ", completer)
            password := prompt.Input("Password: ", completer)
            println(login)
            println(password)
    }


## 10. Plnohodnotný příkazový řádek s vlastní historií příkazů

Aby bylo možné použít příkazový řádek s vlastní historií příkazů, je možné aplikaci poněkud upravit, a to takovým způsobem, že se reakce na příkazy zadané uživatelem přesune do samostatné funkce. Tato funkce se většinou nazývá **executor**, i když nám samozřejmě nic nebrání použít odlišné jméno:
      
    func **executor**(t string) {
            println(t)
    }

Doplníme i funkci se jménem **completer**, která bude vracet prázdnou tabulku s návrhy na doplnění celých slov s příkazy:
      
    func **completer**(in prompt.Document) []prompt.Suggest {
            return []prompt.Suggest{}
    }

Nyní již můžeme zkonstruovat objekt (datovou strukturu) typu **Prompt** a spustit interní smyčku, která bude jednotlivé příkazy načítat a pro každý příkaz zavolá výše zmíněnou callback funkci **executor**:
        
    p := prompt.New(executor, completer)
    p.Run()

Úplný kód příkladu, který implementuje plnohodnotný příkazový řádek i s historií, je dostupný na adrese [https://github.com/tisnik/go-root/blob/master/article20/04_prom­pt.go][29]:
    
    
    package **main**
     
    import "github.com/c-bata/go-prompt"
     
    func **executor**(t string) {
            println(t)
    }
     
    func **completer**(in prompt.Document) []prompt.Suggest {
            return []prompt.Suggest{}
    }
     
    func **main**() {
            p := prompt.New(executor, completer)
            p.Run()
    }

## 11. Zajištění automatického doplňování příkazů na základě statické tabulky

Jednou z velmi užitečných funkcí nabízených knihovnou **go-prompt** je automatické doplňování příkazů, a to bez toho, aby uživatel musel použít nějakou specializovanou klávesovou zkratku – příkazy se nabízí automaticky přímo při psaní. Taktéž se přímo při psaní zobrazuje tabulka s dostupnými příkazy. Interně je tato vlastnost zajištěna neustálým voláním callback funkce **completer**, které se předá hodnota typu **Document** obsahující informace jak o zapsaném textu, tak i o pozici kurzoru v rámci tohoto textu. V tom nejjednodušším případě může callback funkce **completer** vracet vždy stejnou tabulku s příkazy, nezávisle na tom, co uživatel zapisuje. Informace o dostupných příkazech jsou reprezentovány hodnotami typu **Suggest**:
    
```go
func completer(in prompt.Document) []prompt.Suggest {
        return []prompt.Suggest{
                {Text: "help"},
                {Text: "exit"},
                {Text: "quit"},
        }
}
```

Aby byl příklad úplný, doplníme i kód funkce **executor**, který bude jednoduchý – na každý podporovaný příkaz nějakým způsobem zareagujeme. Aplikace se buď ukončí po příkazech „exit" a „quit", nebo se zobrazí nápověda popř. informace o tom, že se jedná o neznámý příkaz:
    
```go    
func **executor**(t string) {
        switch t {
        case "exit":
                fallthrough
        case "quit":
                os.Exit(0)
        case "help":
                println("HELP:nexitnquit")
        default:
                println("Nothing happens")
        }
}
```

Úplný zdrojový kód tohoto demonstračního příkladu naleznete na adrese [https://github.com/tisnik/go-root/blob/master/article20/05_ba­sic_completer.go][34]:
    
    
    package **main**
     
    import (
            "github.com/c-bata/go-prompt"
            "os"
    )
     
    func **executor**(t string) {
            switch t {
            case "exit":
                    fallthrough
            case "quit":
                    os.Exit(0)
            case "help":
                    println("HELP:nexitnquit")
            default:
                    println("Nothing happens")
            }
    }
     
    func **completer**(in prompt.Document) []prompt.Suggest {
            return []prompt.Suggest{
                    {Text: "help"},
                    {Text: "exit"},
                    {Text: "quit"},
            }
    }
     
    func **main**() {
            p := prompt.New(executor, completer)
            p.Run()
    }

## 12. Kontextová nápověda na základě části zapsaného příkazu

Předchozí příklad pochopitelně nebyl dokonalý, protože nezávisle na vstupu zapsaném uživatelem nabízel stále stejné příkazy. Ovšem díky funkci **FilterHasPrefix** je možné z tabulky se všemi dostupnými příkazy vyfiltrovat pouze ty, které začínají zadaným řetězcem. Povšimněte si, jakým způsobem se získá text umístěný před kurzorem (to je jeden z důvodů, proč se funkci **completer** nepředává pouhý řetězec, ale objekt typu **Document**:
    
    
    func **completer**(in prompt.Document) []prompt.Suggest {
            s := []prompt.Suggest{
                    {Text: "help"},
                    {Text: "exit"},
                    {Text: "quit"},
            }
            return prompt.**FilterHasPrefix**(s, in.**GetWordBeforeCursor()**, true)
    }

Poznámka: posledním parametrem funkce **FilterHasPrefix** se určuje, zda se mají ignorovat velikosti znaků či nikoli. 

Po této nepatrné úpravě se chování aplikace radikálně změní, protože bude možné použít plnohodnotné doplňování, kontextovou nápovědu atd.:

![][35]

_Obrázek 12: Stiskem **q** a klávesy **Tab** se automaticky doplní příkaz „quit"._

![][36]

_Obrázek 13: Tabulka je nyní již kontextová, protože nabízí jen relevantní příkazy._

Opět se podívejme na [úplný zdrojový kód tohoto příkladu][37]:
    
    
    package **main**
     
    import (
            "github.com/c-bata/go-prompt"
            "os"
    )
     
    func **executor**(t string) {
            switch t {
            case "exit":
                    fallthrough
            case "quit":
                    println("Quitting")
                    os.Exit(0)
            case "help":
                    println("HELP:nexitnquit")
            default:
                    println("Nothing happens")
            }
    }
     
    func **completer**(in prompt.Document) []prompt.Suggest {
            s := []prompt.Suggest{
                    {Text: "help"},
                    {Text: "exit"},
                    {Text: "quit"},
            }
            return prompt.FilterHasPrefix(s, in.GetWordBeforeCursor(), true)
    }
     
    func **main**() {
            p := prompt.New(executor, completer)
            p.Run()
    }

## 13. Přidání nápovědy k jednotlivým příkazům

Rozšířením datové struktury **Suggest** o položku **Description** je možné zajistit zobrazení nápovědy k jednotlivým příkazům, které jsou nabízeny v kontextovém okně. Úprava zdrojového kódu je snadná:
    
    
    s := []prompt.Suggest{
            {Text: "help", Description: "show help with all commands"},
            {Text: "exit", Description: "quit the application"},
            {Text: "quit", Description: "quit the application"},
    }

Výsledek by měl vypadat takto:

![][38]

_Obrázek 14: Zobrazení všech příkazů i s nápovědou._

Zdrojový kód takto upraveného demonstračního příkladu naleznete na adrese [https://github.com/tisnik/go-root/blob/master/article20/07_com­pletion_description.go][39]:
    
    
    package **main**
     
    import (
            "github.com/c-bata/go-prompt"
            "os"
    )
     
    func **executor**(t string) {
            switch t {
            case "exit":
                    fallthrough
            case "quit":
                    os.Exit(0)
            case "help":
                    println("HELP:nexitnquit")
            default:
                    println("Nothing happens")
            }
            return
    }
     
    func **completer**(in prompt.Document) []prompt.Suggest {
            s := []prompt.Suggest{
                    {Text: "help", Description: "show help with all commands"},
                    {Text: "exit", Description: "quit the application"},
                    {Text: "quit", Description: "quit the application"},
            }
            return prompt.FilterHasPrefix(s, in.GetWordBeforeCursor(), true)
    }
     
    func **main**() {
            p := prompt.New(executor, completer)
            p.Run()
    }

## 14. Rozsáhlejší příklad se všemi klíčovými slovy BASICu

Nyní již máme k dispozici všechny informace nutné pro vytvoření rozsáhlejšího příkladu, který uživatelům bude na příkazovém řádku nabízet všechny příkazy, funkce a klíčová slova Atari BASICu. Jak seznam příkazů, tak i jejich popis byl získán [z Wikipedie][40]. Tento demonstrační příklad by se měl chovat následovně:

![][41]

_Obrázek 15: Nabídka některých příkazů Atari BASICu._

V ostatních ohledech se tento příklad neliší od demonstračního kódu, s nímž jsme se seznámili [v předchozí kapitole][3], takže se [podívejme na jeho výpis][42]:
    
    
    package **main**
     
    import (
            "github.com/c-bata/go-prompt"
            "os"
    )
     
    func **executor**(t string) {
            switch t {
            case "exit":
                    fallthrough
            case "quit":
                    os.Exit(0)
            default:
                    println("READY")
            }
            return
    }
     
    func **completer**(in prompt.Document) []prompt.Suggest {
            s := []prompt.Suggest{
                    {Text: "ABS", Description: "Returns the absolute value of a number"},
                    {Text: "ADR", Description: "Returns the address in memory of a variable (mostly used for machine code routines stored in variables)"},
                    {Text: "AND", Description: "Logical conjunction"},
                    {Text: "ASC", Description: "Returns the ATASCII value of a character"},
                    {Text: "ATN", Description: "Returns the arctangent of a number"},
                    {Text: "BYE", Description: "Transfers control to the internal Self Test program"},
                    {Text: "CHR", Description: "Returns a character given an ATASCII value"},
                    {Text: "CLOAD", Description: "Loads from cassette tape a tokenized program that was saved with CSAVE"},
                    {Text: "CLOG", Description: "Returns the common logarithm of a number"},
                    {Text: "CLOSE", Description: "Terminates pending transfers (flush) and closes an I/O channel"},
                    {Text: "CLR", Description: "Clears variables' memory and program stack"},
                    {Text: "COLOR", Description: "Chooses which logical color to draw in"},
                    {Text: "COM", Description: "Implementation of MS Basic's COMMON was cancelled. Recognized but the code for DIM is executed instead"},
                    {Text: "CONT", Description: "Resumes execution of a program after a STOP at the next line number (see STOP)"},
                    {Text: "COS", Description: "Returns the cosine of a number"},
                    {Text: "CSAVE", Description: "Saves to cassette tape a program in tokenized form (see CLOAD)"},
                    {Text: "DATA", Description: "Stores data in lists of numeric or string values"},
                    {Text: "DEG", Description: "Switches trigonometric functions to compute in degrees (radians is the default mode) (see RAD)"},
                    {Text: "DIM", Description: "Defines the size of a string or array (see COM)"},
                    {Text: "DOS", Description: "Transfers control to the Disk Operating System (DOS); if DOS was not loaded, same as BYE"},
                    {Text: "DRAWTO", Description: "Draws a line to given coordinates"},
                    {Text: "END", Description: "Finishes execution of the program, closes open I/O channels and stops any sound"},
                    {Text: "ENTER", Description: "Loads and merges into memory a plain text program from an external device, usually from cassette tape or disk (see LIST)"},
                    {Text: "EXP", Description: "Exponential function"},
                    {Text: "FOR", Description: "Starts a for loop"},
                    {Text: "FRE", Description: "Returns the amount of free memory in bytes"},
                    {Text: "GET", Description: "Reads one byte from an I/O channel (see PUT)"},
                    {Text: "GOSUB", Description: "Jumps to a subroutine at a given line in the program, placing the return address on the stack (see POP and RETURN)"},
                    {Text: "GOTO", Description: "and GO TO  Jumps to a given line in the program. GOTO can be omitted in IF ... THEN GOTO ..."},
                    {Text: "GRAPHICS", Description: "Sets the graphics mode"},
                    {Text: "IF", Description: "Executes code depending on whether a condition is true or not"},
                    {Text: "INPUT", Description: "Retrieves a stream of text from an I/O channel; usually data from keyboard (default), cassette tape or disk"},
                    {Text: "INT", Description: "Returns the floor of a number"},
                    {Text: "LEN", Description: "Returns the length of a string"},
                    {Text: "LET", Description: "Assigns a value to a variable. LET can be omitted"},
                    {Text: "LIST", Description: "Lists (all or part of) the program to screen (default), printer, disk, cassette tape, or any other external device (see ENTER)"},
                    {Text: "LOAD", Description: "Loads a tokenized program from an external device; usually a cassette tape or disk (see SAVE)"},
                    {Text: "LOCATE", Description: "Stores the logical color or ATASCII character at given coordinates"},
                    {Text: "LOG", Description: "Returns the natural logarithm of a number"},
                    {Text: "LPRINT", Description: "Prints text to a printer device (same result can be achieved with OPEN, PRINT and CLOSE statements)"},
                    {Text: "NEW", Description: "Erases the program and all the variables from memory; automatically executed before a LOAD or CLOAD"},
                    {Text: "NEXT", Description: "Continues the next iteration of a FOR loop"},
                    {Text: "NOT", Description: "Logical negation"},
                    {Text: "NOTE", Description: "Returns the current position on an I/O channel"},
                    {Text: "ON", Description: "A computed goto - performs a jump based on the value of an expression"},
                    {Text: "OPEN", Description: "Initialises an I/O channel"},
                    {Text: "OR", Description: "Logical disjunction"},
                    {Text: "PADDLE", Description: "Returns the position of a paddle controller"},
                    {Text: "PEEK", Description: "Returns the value at an address in memory"},
                    {Text: "PLOT", Description: "Draws a point at given coordinates"},
                    {Text: "POINT", Description: "Sets the current position on an I/O channel"},
                    {Text: "POKE", Description: "Sets a value at an address in memory"},
                    {Text: "POP", Description: "Removes a subroutine return address from the stack (see GOSUB and RETURN)"},
                    {Text: "POSITION", Description: "Sets the position of the graphics cursor"},
                    {Text: "PRINT", Description: "and ?     Writes text to an I/O channel; usually to screen (default), printer, cassette tape or disk (see LPRINT and INPUT)"},
                    {Text: "PTRIG", Description: "Indicates whether a paddle trigger is pressed or not"},
                    {Text: "PUT", Description: "Writes one byte to an I/O channel (see GET)"},
                    {Text: "RAD", Description: "Switches trigonometric functions to compute in radians (see DEG)"},
                    {Text: "READ", Description: "Reads data from a DATA statement"},
                    {Text: "REM", Description: "Marks a comment in a program"},
                    {Text: "RESTORE", Description: "Sets the position of where to read data from a DATA statement"},
                    {Text: "RETURN", Description: "Ends a subroutine, effectively branching to the line immediately following the calling GOSUB (see GOSUB and POP)"},
                    {Text: "RND", Description: "Returns a pseudorandom number"},
                    {Text: "RUN", Description: "Starts execution of a program, optionally loading it from an external device (see LOAD)"},
                    {Text: "SAVE", Description: "Writes a tokenized program to an external device; usually a cassette tape or disk (see LOAD)"},
                    {Text: "SETCOLOR", Description: "Maps a logical color to a physical color"},
                    {Text: "SGN", Description: "Returns the signum of a number"},
                    {Text: "SIN", Description: "Returns the sine of a number"},
                    {Text: "SOUND", Description: "Starts or stops playing a tone on a sound channel (see END)"},
                    {Text: "SQR", Description: "Returns the square root of a number"},
                    {Text: "STATUS", Description: "Returns the status of an I/O channel"},
                    {Text: "STEP", Description: "Indicates the increment used in a FOR loop"},
                    {Text: "STICK", Description: "Returns a joystick position"},
                    {Text: "STOP", Description: "Stops the program, allowing later resumption (see CONT)"},
                    {Text: "STRIG", Description: "Indicates whether a joystick trigger is pressed or not"},
                    {Text: "STR", Description: "Converts a number to string form"},
                    {Text: "THEN", Description: "Indicates the statements to execute if the condition is true in an IF statement"},
                    {Text: "TO", Description: "Indicates the limiting condition in a FOR statement"},
                    {Text: "TRAP", Description: "Sets to jump to a given program line if an error occurs (TRAP 40000 cancels this order)"},
                    {Text: "USR", Description: "Calls a machine code routine, optionally with parameters"},
                    {Text: "VAL", Description: "Returns the numeric value of a string"},
                    {Text: "XIO", Description: "General-purpose I/O routine (from Fill screen to Rename file to Format disk instructions) "},
                    {Text: "exit", Description: "Quit the application"},
                    {Text: "quit", Description: "Quit the application"},
            }
            if in.GetWordBeforeCursor() == "" {
                    return nil
            } else {
                    return prompt.FilterHasPrefix(s, in.GetWordBeforeCursor(), true)
            }
    }
     
    func **main**() {
            p := prompt.New(executor, completer)
            p.Run()
    }

## 15. Pokročilejší vyhledání vhodných příkazů na základě fuzzy filtru

V mnoha případech je výhodnější změnit způsob výběru nabízených příkazů. Namísto seznamu příkazů, které _začínají_ určitým řetězcem lze použít takzvaný _fuzzy filtr_, který vrátí všechny příkazy obsahující zadanou sekvenci znaků (v daném pořadí). Například pro řetězec „PN" se mohou vrátit příkazy **PRINT**, **LPRINT**, **POINT** a **POSITION**, ale i **OPEN** apod. Úprava funkce **completer** je snadná – pouze zavoláme funkci **FilterFuzzy** namísto funkce **FilterHasPrefix**:
    
    
    func **completer**(in prompt.Document) []prompt.Suggest {
            if in.GetWordBeforeCursor() == "" {
                    return nil
            } else {
                    return prompt.**FilterFuzzy**(suggestions, in.GetWordBeforeCursor(), true)
            }
    }

![][43]

_Obrázek 16: Všech šest příkazů, které jsou zobrazeny v kontextové nápovědě, skutečně obsahuje znaky „C" a „L" v uvedeném pořadí._

Následuje výpis [zdrojového kódu tohoto příkladu][44]:
    
    
    package **main**
     
    import (
            "github.com/c-bata/go-prompt"
            "os"
    )
     
    var suggestions = []prompt.Suggest{
            {Text: "ABS", Description: "Returns the absolute value of a number"},
            {Text: "ADR", Description: "Returns the address in memory of a variable (mostly used for machine code routines stored in variables)"},
            {Text: "AND", Description: "Logical conjunction"},
            {Text: "ASC", Description: "Returns the ATASCII value of a character"},
            {Text: "ATN", Description: "Returns the arctangent of a number"},
            {Text: "BYE", Description: "Transfers control to the internal Self Test program"},
            {Text: "CHR", Description: "Returns a character given an ATASCII value"},
            {Text: "CLOAD", Description: "Loads from cassette tape a tokenized program that was saved with CSAVE"},
            {Text: "CLOG", Description: "Returns the common logarithm of a number"},
            {Text: "CLOSE", Description: "Terminates pending transfers (flush) and closes an I/O channel"},
            {Text: "CLR", Description: "Clears variables' memory and program stack"},
            {Text: "COLOR", Description: "Chooses which logical color to draw in"},
            {Text: "COM", Description: "Implementation of MS Basic's COMMON was cancelled. Recognized but the code for DIM is executed instead"},
            {Text: "CONT", Description: "Resumes execution of a program after a STOP at the next line number (see STOP)"},
            {Text: "COS", Description: "Returns the cosine of a number"},
            {Text: "CSAVE", Description: "Saves to cassette tape a program in tokenized form (see CLOAD)"},
            {Text: "DATA", Description: "Stores data in lists of numeric or string values"},
            {Text: "DEG", Description: "Switches trigonometric functions to compute in degrees (radians is the default mode) (see RAD)"},
            {Text: "DIM", Description: "Defines the size of a string or array (see COM)"},
            {Text: "DOS", Description: "Transfers control to the Disk Operating System (DOS); if DOS was not loaded, same as BYE"},
            {Text: "DRAWTO", Description: "Draws a line to given coordinates"},
            {Text: "END", Description: "Finishes execution of the program, closes open I/O channels and stops any sound"},
            {Text: "ENTER", Description: "Loads and merges into memory a plain text program from an external device, usually from cassette tape or disk (see LIST)"},
            {Text: "EXP", Description: "Exponential function"},
            {Text: "FOR", Description: "Starts a for loop"},
            {Text: "FRE", Description: "Returns the amount of free memory in bytes"},
            {Text: "GET", Description: "Reads one byte from an I/O channel (see PUT)"},
            {Text: "GOSUB", Description: "Jumps to a subroutine at a given line in the program, placing the return address on the stack (see POP and RETURN)"},
            {Text: "GOTO", Description: "and GO TO  Jumps to a given line in the program. GOTO can be omitted in IF ... THEN GOTO ..."},
            {Text: "GRAPHICS", Description: "Sets the graphics mode"},
            {Text: "IF", Description: "Executes code depending on whether a condition is true or not"},
            {Text: "INPUT", Description: "Retrieves a stream of text from an I/O channel; usually data from keyboard (default), cassette tape or disk"},
            {Text: "INT", Description: "Returns the floor of a number"},
            {Text: "LEN", Description: "Returns the length of a string"},
            {Text: "LET", Description: "Assigns a value to a variable. LET can be omitted"},
            {Text: "LIST", Description: "Lists (all or part of) the program to screen (default), printer, disk, cassette tape, or any other external device (see ENTER)"},
            {Text: "LOAD", Description: "Loads a tokenized program from an external device; usually a cassette tape or disk (see SAVE)"},
            {Text: "LOCATE", Description: "Stores the logical color or ATASCII character at given coordinates"},
            {Text: "LOG", Description: "Returns the natural logarithm of a number"},
            {Text: "LPRINT", Description: "Prints text to a printer device (same result can be achieved with OPEN, PRINT and CLOSE statements)"},
            {Text: "NEW", Description: "Erases the program and all the variables from memory; automatically executed before a LOAD or CLOAD"},
            {Text: "NEXT", Description: "Continues the next iteration of a FOR loop"},
            {Text: "NOT", Description: "Logical negation"},
            {Text: "NOTE", Description: "Returns the current position on an I/O channel"},
            {Text: "ON", Description: "A computed goto - performs a jump based on the value of an expression"},
            {Text: "OPEN", Description: "Initialises an I/O channel"},
            {Text: "OR", Description: "Logical disjunction"},
            {Text: "PADDLE", Description: "Returns the position of a paddle controller"},
            {Text: "PEEK", Description: "Returns the value at an address in memory"},
            {Text: "PLOT", Description: "Draws a point at given coordinates"},
            {Text: "POINT", Description: "Sets the current position on an I/O channel"},
            {Text: "POKE", Description: "Sets a value at an address in memory"},
            {Text: "POP", Description: "Removes a subroutine return address from the stack (see GOSUB and RETURN)"},
            {Text: "POSITION", Description: "Sets the position of the graphics cursor"},
            {Text: "PRINT", Description: "and ?     Writes text to an I/O channel; usually to screen (default), printer, cassette tape or disk (see LPRINT and INPUT)"},
            {Text: "PTRIG", Description: "Indicates whether a paddle trigger is pressed or not"},
            {Text: "PUT", Description: "Writes one byte to an I/O channel (see GET)"},
            {Text: "RAD", Description: "Switches trigonometric functions to compute in radians (see DEG)"},
            {Text: "READ", Description: "Reads data from a DATA statement"},
            {Text: "REM", Description: "Marks a comment in a program"},
            {Text: "RESTORE", Description: "Sets the position of where to read data from a DATA statement"},
            {Text: "RETURN", Description: "Ends a subroutine, effectively branching to the line immediately following the calling GOSUB (see GOSUB and POP)"},
            {Text: "RND", Description: "Returns a pseudorandom number"},
            {Text: "RUN", Description: "Starts execution of a program, optionally loading it from an external device (see LOAD)"},
            {Text: "SAVE", Description: "Writes a tokenized program to an external device; usually a cassette tape or disk (see LOAD)"},
            {Text: "SETCOLOR", Description: "Maps a logical color to a physical color"},
            {Text: "SGN", Description: "Returns the signum of a number"},
            {Text: "SIN", Description: "Returns the sine of a number"},
            {Text: "SOUND", Description: "Starts or stops playing a tone on a sound channel (see END)"},
            {Text: "SQR", Description: "Returns the square root of a number"},
            {Text: "STATUS", Description: "Returns the status of an I/O channel"},
            {Text: "STEP", Description: "Indicates the increment used in a FOR loop"},
            {Text: "STICK", Description: "Returns a joystick position"},
            {Text: "STOP", Description: "Stops the program, allowing later resumption (see CONT)"},
            {Text: "STRIG", Description: "Indicates whether a joystick trigger is pressed or not"},
            {Text: "STR", Description: "Converts a number to string form"},
            {Text: "THEN", Description: "Indicates the statements to execute if the condition is true in an IF statement"},
            {Text: "TO", Description: "Indicates the limiting condition in a FOR statement"},
            {Text: "TRAP", Description: "Sets to jump to a given program line if an error occurs (TRAP 40000 cancels this order)"},
            {Text: "USR", Description: "Calls a machine code routine, optionally with parameters"},
            {Text: "VAL", Description: "Returns the numeric value of a string"},
            {Text: "XIO", Description: "General-purpose I/O routine (from Fill screen to Rename file to Format disk instructions) "},
            {Text: "exit", Description: "Quit the application"},
            {Text: "quit", Description: "Quit the application"},
    }
     
    func **executor**(t string) {
            switch t {
            case "exit":
                    fallthrough
            case "quit":
                    os.Exit(0)
            default:
                    println("Nothing happens")
            }
            return
    }
     
    func **completer**(in prompt.Document) []prompt.Suggest {
            if in.GetWordBeforeCursor() == "" {
                    return nil
            } else {
                    return prompt.FilterFuzzy(suggestions, in.GetWordBeforeCursor(), true)
            }
    }
     
    func **main**() {
            p := prompt.New(executor, completer)
            p.Run()
    }

## 16. Kontextová nápověda u příkazů, které se skládají ze dvou slov

Často se setkáme s požadavkem, aby program umožňoval zadání příkazů, které se skládají ze dvou či dokonce z většího množství slov. I v takovém případě samozřejmě můžeme použít funkce z knihovny **go-prompt**, ovšem budeme muset provést několik úprav ve funkci **completer**. Nejprve si však ukažme, jak by mělo vypadat očekávané chování aplikace:

![][45]

_Obrázek 17: Nápověda prvních slov příkazu._

![][46]

_Obrázek 18: Nápověda dalšího slova dvouslovního příkazu._

![][47]

_Obrázek 19: Nápověda dalšího slova odlišného dvouslovního příkazu._

Vidíme, že kontextová nápověda ke druhému slovu závisí na tom, jakým slovem příkaz začíná.

## 17. Implementace kontextové nápovědy víceslovních příkazů

Jedna z možných implementací „inteligentní" kontextové nápovědy pro víceslovní příkazy spočívá v tom, že ve funkci **completer** nejprve rozložíme řetězec zadaný uživatelem na jednotlivá slova:
    
    
    blocks := strings.Split(in.TextBeforeCursor(), " ")

Dále si vytvoříme několik tabulek příkazů. První tabulka bude obsahovat pouze první slova příkazu:
    
    
    s := []prompt.Suggest{
            {Text: "help", Description: "show help with all commands"},
            {Text: "user", Description: "add/delete user"},
            {Text: "ls", Description: "list users/storages/computers"},
            {Text: "list", Description: "list users/storages/computers"},
            {Text: "exit", Description: "quit the application"},
            {Text: "quit", Description: "quit the application"},
    }

Další tabulky pak pouze druhá slova příkazů:
    
    
    user_s := []prompt.Suggest{
            {Text: "add", Description: "add new user"},
            {Text: "assign", Description: "assign a role to user"},
            {Text: "del", Description: "delete user"},
    }
     
    list_s := []prompt.Suggest{...
     
    empty_s := []prompt.Suggest{}

Následně implementujeme primitivní logiku pro rozhodnutí, kterou tabulku s nabídkou příkazů vrátit a použít – zda seznam prvních slov či nějaký ze seznamů slov druhých. Ve větvi **default** se vrací prázdná tabulka pro jednoslovní či pro neznámé příkazy:
    
    
    if len(blocks) == 2 {
            switch blocks[0] {
            case "user":
                    return prompt.FilterHasPrefix(user_s, blocks[1], true)
            case "ls":
                    fallthrough
            case "list":
                    return prompt.FilterHasPrefix(list_s, blocks[1], true)
            default:
                    return empty_s
            }
    }
    return prompt.FilterHasPrefix(s, blocks[0], true)

## 18. Úplný zdrojový kód příkladu s víceslovními příkazy

Úplný zdrojový kód příkladu s víceslovními příkazy nalezneme na adrese [https://github.com/tisnik/go-root/blob/master/article20/10_two_wor­d_commands.go][48]:
    
    
    package **main**
     
    import (
            "github.com/c-bata/go-prompt"
            "os"
            "strings"
    )
     
    func **executor**(t string) {
            switch t {
            case "exit":
                    fallthrough
            case "quit":
                    os.Exit(0)
            case "help":
                    println("HELP:nexitnquit")
            case "user add":
                    println("Adding user")
            case "user del":
                    println("Deleting user")
            default:
                    println("Nothing happens")
            }
            return
    }
     
    func **completer**(in prompt.Document) []prompt.Suggest {
            blocks := strings.Split(in.TextBeforeCursor(), " ")
     
            s := []prompt.Suggest{
                    {Text: "help", Description: "show help with all commands"},
                    {Text: "user", Description: "add/delete user"},
                    {Text: "ls", Description: "list users/storages/computers"},
                    {Text: "list", Description: "list users/storages/computers"},
                    {Text: "exit", Description: "quit the application"},
                    {Text: "quit", Description: "quit the application"},
            }
     
            user_s := []prompt.Suggest{
                    {Text: "add", Description: "add new user"},
                    {Text: "assign", Description: "assign a role to user"},
                    {Text: "del", Description: "delete user"},
            }
     
            list_s := []prompt.Suggest{
                    {Text: "users", Description: "show list of all users"},
                    {Text: "logs", Description: "show list of all logs"},
                    {Text: "storages", Description: "show list of all storages"},
                    {Text: "computers", Description: "show list of all computers"},
            }
     
            empty_s := []prompt.Suggest{}
     
            if len(blocks) == 2 {
                    switch blocks[0] {
                    case "user":
                            return prompt.FilterHasPrefix(user_s, blocks[1], true)
                    case "ls":
                            fallthrough
                    case "list":
                            return prompt.FilterHasPrefix(list_s, blocks[1], true)
                    default:
                            return empty_s
                    }
            }
            return prompt.FilterHasPrefix(s, blocks[0], true)
    }
     
    func **main**() {
            p := prompt.New(executor, completer)
            p.Run()
    }

