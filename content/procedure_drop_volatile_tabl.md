---
title:  'Drop a volatile table in procedure, Teradata'
tags:   [procedure,volatile,teradata]
---

```sql
/*
   Drop a table ignoring 3807 error (Table doesn't exist)
   All other errors still return an error
   Database & table name will be double quoted to allow non-standard names like 'my table?'
   If database is not specified it defaults to current DATABASE
   Drops Volatile Tables when database is set to USER 
*/
REPLACE PROCEDURE drop_table_if_exists
(
  IN db_name VARCHAR(128) CHARACTER SET Unicode,
  IN tbl_name VARCHAR(128) CHARACTER SET Unicode,
  OUT msg VARCHAR(400) CHARACTER SET Unicode
) SQL SECURITY INVOKER
BEGIN
   DECLARE full_name VARCHAR(361)  CHARACTER SET Unicode;

   DECLARE sql_stmt VARCHAR(500)  CHARACTER SET Unicode;
   
   DECLARE exit HANDLER FOR SqlException
   BEGIN
      IF SqlCode = 3807 THEN SET msg = full_name || ' doesn''t exist.';
      ELSE
        RESIGNAL;
      END IF;
   END;

   SET full_name = '"' || Coalesce(db_name,DATABASE) || '"."' || COALESCE(tbl_name,'') || '"';

   SET sql_stmt = 'DROP TABLE ' || full_name || ';';

   EXECUTE IMMEDIATE sql_stmt;

   SET msg = full_name || ' dropped.';
END;

CALL drop_table_if_exists('mydb', 'mytbl', outmsg);
CALL drop_table_if_exists(NULL, 'mytbl', outmsg); -- defaults to DATABASE
CALL drop_table_if_exists(USER, 'mytbl', outmsg); -- to drop Volaltile Table
```