---
title:  'devops draft'
tags:   [git, devops]
---

# Devops

Základní principy které jsme diskutovali se Zdeňkem a Honzou.

**SPRINT**
* jednotka vývoje je sprint
* sprint je zahájen vytvořením branche z Masteru, a žije vlastním životem.
* pokud se má sprint nasazovat na EDWTEST, realizuje se to nasazením z dané SPRINT branche přes tagovaný commit

**MASTER**
* master branch je chtěný obraz produkce. 
* sprint se dokončuje zamergováním na MASTER branch, daný commit se musí otagovat.

**PR**
* PR je změna provedená přímo na MASTER větvi
* změna se musí otagovat jako PR, a nasazuje se stejnými mechanismy, jako sprint


Branch na DEV GITu bude mít omezenou živostnost; (nějakou dovu po) po provedení merge na master se celá branch zlikviduje, a to i na remote repository.



# Adresářová struktura - hrubý nástřel


### Dev master

* odtud se nasazují TPT skripty (a param soubory)

| adresář                                       | co tam je                                                            |
|-----------------------------------------------|----------------------------------------------------------------------|
| `/M/SRC/VIRT_NODE/EDW` (nebo `CDC`,`IFRS`,...)  | kompletní obraz filesystému, z něj se nasazuje (ne system_info apod) |
| `/M/db/Teradata`                                | kompletní obraz DDL z teradaty (tak jak má vypadat); **odsud se nenasazuje** |
| `/M/db/PDC`                                     | vygenerované PDC skripty; **odtud se nenasazuje**                    |
| `/W/...`                                        | freestyle adresář daného sprintu (striktně vzato tohle nechceme mergovat do masteru; jak to zajistit?) |


### Package

* Odtud se nasazují změny na Teradatu a na PDC
* Na replikaci se nasazuje podle jiných pravidel (po půlnoci), a proto je žádoucí tyhle skripty adresářově oddělit 
* Základní jednotka nasazení je `GIT TAG`, 
    * pod daným tagem je schovaný (celý) adresář daného sprintu
    * pod identickým tagem jsou na `dev master` části gitu uložené TPT skripty pro produkci

| adresář                                       | co tam je                                                            |
|-----------------------------------------------|----------------------------------------------------------------------|
| `/P/main_sprint_xyz`                          | změnové skripty pro PDC a Teradatu (mimo CDC) pro daný sprint, co adresář to "balíček"      |
| `/P/cdc_sprintxyz`                            | změnové skripty pro CDC a PDC pro replikaci (odděleno kvůli odlišným pravidlům pro nasazení |


### Dokumentace

* adresář `/doc` - návody, postupy, kuchařky
* adresář `/model` - datové modely

...

# Nasazení

* nasazuje se ze dvou míst
* nasazuje se vždy daný TAG
* na produkci vždy z MASTER branche, na TEST může jít o nasazení z odlišné branche
