# Performance monitoring

Viz [docs](https://docs.teradata.com/reader/B7Lgdw6r3719WUyiCSJcgw/zYorB2DKSeBHXVorQogWuQ)

## DBQLogTbl

- Popis pro [DBQLogTbl](https://docs.teradata.com/reader/B7Lgdw6r3719WUyiCSJcgw/YIKoBz~QQgv2Aw5dF339kA)

### Kde jí najdeš:

- v databázi `DBC`, tam ale je k dispozici jen provoz od půlnoci dál - vhodné pro denní sběr "zajímavých" dat
- v databázi `PDCRINFO`, ale tam nemáš práva, tak co?
- v databázi `PDCRDATA`, kde je historie

### Co tam najdeš

- User ID and user name under which the session being logged was initiated
- Unique ID for the process, session, and host (client) connection
- Account string, expanded as appropriate, that was current when the query completed
- First 200 characters of the query SQL statement
- CPU and I/O statistics

Vybraná dostupná pole:

- NUPI
    - `CollectTimeStamp` - vztahuje se k buffer cache (???)
    - `ProcID` - processor ID of the dispatcher
    
- Obecné a identifikační údaje
    - `QueryID` - cizí klíč do ostatních DBQL tabulek; **unikátní identifikátor dotazu** i bez `ProcID`
    - `SessionID`
    - `StartTime` - kdy byl dotaz odeslán
    - `AbortFlag` - označuje dotaz který nedoběhl, a nemusí mít kompletní data
    - `AppId` - identifikuje aplikaci (BTEQ)    
    - `ErrorCode` - pokud je větší než 0, došlo ksyntaktické chybě; viz také `ErrorText`
    - `UserName` - pod jakým uživatelem byla session iniciována
    - `QueryText` - jen 200 znaků
    - `QueryBand`
        - =T> transaction query band pairs
        - =S> session query band pairs
        - =P> profile query band pairs 
    
- Performance related údaje    
    - `AMPCPUTime` - celkový počet CPU vteřin
    - `SpoolUsage` - maximum amount of spool used while processing the query. 
    - `DelayTime` - ve vteřinách - jak dlouho držel TASM dotaz "on hold"
    - `MaxAMPCPUTime`, `MaxAMPCPUTimeNorm` - ve vteřinách (nejdelší čas strávený na AMPu)
    - `MaxAmpIO` - IO count
    - `MinAmpCPUTime`, `MinAmpCPUTimeNorm`, `MinAmpIO`
    - `TotalIOCount`  - Total logical I/O used by the query.
    - `NumOfActiveAmps` - lze použít za účelem výpočtu průměrného CPU času nebo IO
    - `NumResultRows` - počet záznamů které dotaz vrátil
    - `NumSteps` - komplexita exekučního plánu
    
- Další performance related údaje
    - `ReqIOKB` - total logical I/O usage in kilobytes.
    - `ReqPhysIO` - The number of physical I/Os.
    - `ReqPhysIOKB` - Total physical I/O usage in kilobytes.
    
- Estimations
    - `EstResultRows`, `EstProcTime` - počet záznamů a čas (ve vteřinách)

## DBQLObjTbl

- Co záznam, to použitý objekt
- Popis pro [DBQLObjTbl](https://docs.teradata.com/reader/B7Lgdw6r3719WUyiCSJcgw/eOMXq~u5PwRV5GrooD6_9A)

## DBQLSQLTbl + QryLogSQLDoc

- Plný text SQL dotazu

```
select     SQLTextInfo
from       dbc.DBQLSQLTbl
where      QueryId = xxxxxxx
order by   SQLRowNo asc                 // fragmentace po 32.000 bajtech

--- tady je view které se to pokouší sloučit

select     SQLTextDoc
from       dbc.QryLogSQLDoc
where      QueryId = xxxxxxx
```


## DBQLStepTbl

- Performance statistiky po jednotlivých stepech dotazu
- Pod zátěží bohužel může dojít k tomu, že se metadata nepodaří sebrat a/nebo uložit; a bohužel zrovna pod zátěží jsou metadata zajímavá.
- Viz [docs](https://docs.teradata.com/reader/B7Lgdw6r3719WUyiCSJcgw/BC8owdBbJGdQXIlV_aWrSA)


- Obecné a identifikační údaje
    - `QueryID`
    - `StatementNum`
    - `CollectTimeStamp`, `StepStartTime`, `StepStopTime`
    
    
- Performance
    - `CpuTime`, `CpuTimeNorm`
    - `IOCount`, `IOKB`, `MaxAmpIO`, `MinAmpIO`
    - `PhysIO`, `PhysIOKB`, 
    - `MaxAmpCPUTime`, `MaxAmpCPUTimeNorm`
    - `MinAmpCPUTime`, `MinAmpCPUTimeNorm`
    - `MaxAMPSpool`, `MinAMPSpool`, `SpoolUsage`
    - `NumofActiveAMPs`
    - `RowCount`

# Dotazy

## Product Join Indicator (PJI)

If PJI is relatively high for a query, then the query takes many CPUs for the given number of I/Os.

- Maybe there is a Product Join in the Explain.   
- If the value is higher than 3, then the select HAS to be tuned
- If the value is higher than 6, then it is completely unacceptable

## Unnecessary Io Indicator (UII)

The value should be around 1. 
If UII is relatively high for a query, then it could mean that many I/O blocks are read, but relatively low number of rows actually processed.
If it is a full table scan with only a few rows qualifying, then an index could reduce the I/O consumption in this case. 

- If the value is higher than 3, then the select HAS to be tuned
- If the value is higher than 6, then it is completely unacceptable

## "Všechno zajímavé" v posledním loadu


```
SELECT
                a.username
              , a.queryId -- tohle je must have pokud chci zjistit o tom dotazu víc
              , a.sessionId
              , a.collectTimeStamp -- tohle je na NUPI v query logu, pokud budu potřebovat dohledat detail, bude se to hodit
              , a.ProcId           -- tohle je na NUPI v query logu, pokud budu potřebovat dohledat detail, bude se to hodit
              , GetQuerybandValue(a.QueryBand, 0, 'STP') AS step
              , GetQuerybandValue(a.QueryBand, 0, 'OBJ') AS mapping_name
              , GetQuerybandValue(a.QueryBand, 0, 'MID') AS map_id
              , a.starttime
              ,((a.FirstRespTime - a.StartTime) HOUR(4) TO SECOND)                                          AS ElapsedTimeInDB
              , a.SpoolUsage/(1.00*1024*1024)                                                               AS SpoolUsage_MB
              , COALESCE(100 -(NULLIFZERO(a.Ampcputime/(HASHAMP()+1))/(NULLIFZERO(a.Maxampcputime))*100),0) AS CPUSkew
              , a.MaxAMPCPUTime*a.NumOfActiveAMPs                                                              ImpactCPU
              ,( 
                CASE
                                WHEN a.AMPCPUTime = 0
                                THEN 0
                                ELSE (HASHAMP() + 1 ) * a.MaxAmpCPUTime / a.AMPCPUTime
                END ) AS CPUSkewImpact
              ,(
                CASE
                                WHEN a.TotalIOCount = 0
                                THEN 0
                                ELSE (HASHAMP()+1)*a.MaxAmpIO/a.TotalIOCount
                END )                            AS IOSkewImpact
              , a.TotalIOCount/( HASHAMP() + 1 ) AS AvgCountIO
              , (
                CASE
                                WHEN a.MaxAmpIO = 0
                                THEN 0
                                ELSE 100 * (1 - (AvgCountIO / MaxAmpIO))
                END )                                                         AS IOSkew
              ,(a.AMPCPUTime            *1000)/NULLIFZERO(a.TotalIOCount)     AS PJI
              , COALESCE(a.TotalIOCount /(NULLIFZERO(a.AMPCPUTime) * 1000),0) AS UII
              , CASE 
                                WHEN a.queryText LIKE 'USING%' 
                                THEN b.callqueryText 
                                                                || ' @ ' 
                                                                || SUBSTRING(a.queryText FROM 0 FOR 100) 
                                ELSE SUBSTRING(a.queryText FROM 0 FOR 100) 
                END           AS NormQueryText
FROM            dbc.DBQLogTbl    a
LEFT OUTER JOIN 
                ( SELECT 
                         sessionId 
                       , MIN(SUBSTRING(queryText FROM 0 FOR 50)) AS callqueryText
                FROM     dbc.DBQLogTbl
                WHERE 
                         queryText LIKE 'CALL %'
                GROUP BY 
                         1 ) 
                b 
ON              a.sessionId = b.sessionId
WHERE
                a.username LIKE 'UEP%'
                --AND    NOT queryBand IS NULL
                --AND     NOT queryBand = '=S> ClientLoadSessions=0;'
                -----------------------------------------------------------
                -- nezajímavé procesy
                -----------------------------------------------------------
AND             NOT ( queryText           LIKE '%CAST(MIN(EXTRACT_DTTM) AS VARCHAR(19)) AS MIN_EXTRACT_DTTM%EP_OPR.LOADED_DATA_STAT%' -- stage sniffery
                OR              queryText LIKE 'CALL EP_OPR.proc_stats_exec%'                                                         -- stats procka
                OR              queryText LIKE 'SET QUERY_BAND%'                                                                      -- common
                OR              queryText LIKE 'HELP SESSION%'                                                                        -- common
                OR              querytext LIKE 'COLLECT%' )
                -----------------------------------------------------------
                -- performance filter start
                -----------------------------------------------------------
AND             ( SpoolUsage_MB > 100 * 1024 
                                /* spool větší než 100 GB */
                OR              impactcpu > ( HASHAMP() + 1 ) * 250 
                                /* víc jak 250 sekund na ampech */
                OR              ( spoolusage_mb         > 1000
                                AND             IOSkew  > 10 )
                OR              ( spoolusage_mb         > 1000
                                AND             CPUSkew > 10 ) )
```

## pdcrdata.DBQLogTbl_hst

Jako `DBQLObjTbl`, ale s historií.