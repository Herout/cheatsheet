---
title:  'Conemu tips'
tags:   [conemu, windows]
---
[Source](https://superuser.com/questions/532037/how-can-i-have-clickable-urls-in-conemu)


**Q**: Can I have clickable HTTP links in conemu?

**A**: Hold LeftCtrl and click them. Look for "Highlight & goto" option on "Controls" page ("Mark & Paste" in old stable release).

