---
title: "From Perl to Python"
tags: [perl, python]
---

# Naprostý základ

- lokalizace scope a shadowing: local - encapsulated - global

## Strings

- by default unicode, ale existují i byte strings
- byte string se konvertuje takhle: `string = b"byte string".decode("utf8")`
- ale pozor....

```
In [7]: string = b"ř".decode("utf8")
  File "<ipython-input-7-c407479a198d>", line 1
    string = b"ř".decode("utf8")
             ^
SyntaxError: bytes can only contain ASCII literal characters.
```

| koncept            | jak                                                    |
| ------------------ | ------------------------------------------------------ |
| raw strings        | : `r'tohle je raw \t'                                  |
| byte string        | `b"bytes"`                                             |
| multiline strings  | `"""prvni radek, pak druhy"""`                         |
| interpolace        | `f"{promenna} = {hodnota}"`                            |
| interpolace po 3.8 | `f"{promenna=}"` => výstup: `promenna='hodnota'`       |
| formatted strings  | `'param1 = %s, param2 = %d' % (paramString, paramInt)` |
| concat             | `string1 + string2 + string3`                          |

## Tuple, Array, Slice

- `tuple` je read only pole (immutable)
- `slice` neobsahuje poslední prvek indexu

```python
myTuple = ( 1,2,3,4 )                       # tuple: read only list
myArr = [ 1,2,3,4 ]                         # array
print (myArr[0], myArr[-1], myArr[0:2])     # index, index od konce, slice; pozor, konec slice NENÍ součástí výstupu!

# list of lists, list of hashes, hash of lists
myList = [ [1,2], [3,4] ]
myList = [ { 'key1':'val1', 'key2':'val2'} ]
HoL = { 'flintstones' : ['fred', 'barney'],
        'jetsons' : ['george', 'jane', 'elroy'],
        'simpsons': ['homer', 'marge', 'bart'], }


# apped a delete
myArr.append(100)
del myArr[0]
print myArr

myTuple = ( 1,2,3,4 )                       # tuple: read only list
myArr = [ 1,2,3,4 ]                         # array
print (myArr[0], myArr[-1], myArr[0:2])     # index, index od konce, slice; pozor, konec slice NENÍ součástí výstupu!

# apped a delete
myArr.append(100)
del myArr[0]
print (myArr)
```

**iterace**

```
for itm in array:
    ...
for itm in reversed(array):
    ...
for itm in sorted(array, key=comparioson_function):
    ...

for i,v in enumerate(array):
    ...

mydict = {1: 'a', 2: 'b'}
for i, (k, v) in enumerate(mydict.items()):
    print("index: {}, key: {}, value: {}".format(i, k, v))

# loop dokud nenarazíš na sentinel value, což je prázdný string
for block in iter(partial(f.read, 32), ''):
    ...
```

## Dictionary (hash)

### Základ

```
myDict = { "key":"val" }
if "key" in myDict: print ("klíč existuje")
key myDict["key"]
```

### Autovivifikace

```
dict.setdefault(key,default)

# will look up dict[key] and ...
#    ... if the key already exists, return its current value without modifying it, or ...
#    ... assign the default value (my_dict[key] = default) and then return that.
```

příklad autovivifikace "postaru", nebo pro složitější úlohy (hash of hashes, dict of dicts)

```
for r in pdc:
    db,table,histType = r['databaseName'], r['tableName'], r['histType']
    # autovivifikace
    translations = databases.setdefault(db,dict()).setdefault(table,dict())
    translations[database][table]
```

### modul: collections

**collections**

Autovivifikace v Pythonu je "vopruz". Budeš narázet na exceptions na neexistenci prvků.
Pro primitivní use cases se dá použít `collecstions.defaultdict`, ale zapomeň na složitější
datové struktury ve srovnání s Perlem.

```
from collections import defaultdict
dict = defaultdict(int) # defaultdict(array); defaultdict(dict)
print (dict['neexistujici_klíč'])   # vrátí 0
dict['neexistujici_klíč']+=1        # nahodí 1
```

**chainmap** - linking; dictionary, které vrací zpátky hodnotu z prvního výskytu který najde

```
from collections import ChainMap
config = ChainMap( command_line_args_dict, os.environ, defaults_dict )
```

## funkce, args, kwargs

- dvě klíčová slova, `def` a `return`
- podpora default hodnot v parametrech přes `param=default`
- podpora předem neznámého počtu argumentů přes `*args`,
  kde hvězdička kompilátor žádá o "rozbalení" seznamu parametrů
- podpora pojmenovaných argumentů přes `**kwargs`, kde dvě hvězdičky
  kompilátor žádají o rozbalení parametrů a překlad na dictionary/hash
- `**kwargs` je přibližně totéž jako `my (%args) = @_`

```
def acceptNames(**kwargs):
    for key in kwargs:
        print ("key= %s" % (key))
        print ("val=", kwargs[key])

def sumList(*cisla):
    sum = 0
    for i in cisla:
        sum+=i
    return sum

# tady kupodivu nemusí být názvy v apostrofech, protože to vlastně jsou NÁZVY PROMĚNNÝCH
acceptNames( name = 'John Doe', age = 21 , mission = ['kick ass', 'chew bubblegum'])

print (sumList(1,2,3))
```

## context manager

**temporary context for decimals**

```
# OLD
from decimal import *

getcontext().prec = 6

Decimal(1) / Decimal(7)
Decimal('0.142857')

getcontext().prec = 28

Decimal(1) / Decimal(7)
Decimal('0.1428571428571428571428571429')

# NEW
with localcontext(decimal.Context(prec=50)):
    print decimal.Decimal(355)/decimal.Decimal(100)

```

**files**

```
with open('data.txt') as f:
    data = f.read()
```

**locks** - threading.locks

```
with lock:
    print('critical section')
```

**ignore errors**

```
with ignored(OSError):
    os.remove('file') # never fail
```

**přesměrování**

```
with open('help.txt','w') as f:
    with redirect_stdout(f):
        print('tohle se zapíše do souboru, protože jsem do něj přesměroval stdout')
        help(pow)
```

## Control flow

### if

```python
if condition:
    do something
elif other_condition:
    do something else
else:
    default case
```

### while

- `while`, `break`, `continue` (jako next v Perlu)

```python
while True:
    if found_what_was_wanted:
        break
    if skip_rest:
        continue
    print('sem se nemusíme dostat')
```

### přeskok přes dvě úrovně

```
class BreakLevelError(Exception):
    pass

for i in range(10):
    try:
        for y in range(10):
            print ("y=",y)
            if y == 1:
                raise BreakLevelError
    except BreakLevelError:
        break
else:
    # else ve for bloku je vlastně něco jako "if not found"....
    print("tohle se nikdy nevytiskne protože jsem dal break")

```

## Moduly

### Základy

- adresář, který obsahuje `__init__.py` se považuje za "regular" package
- `__init__.py` se vykoná při importu; postupuje se přitom rekurzivně k nadřazeným adresářům v hierarchii
- importní cesta je definována v listu `sys.modules`

### Import

```
import os               # takže potom: os.rename('file1','new-file1')
from os import rename   # takže potom: rename('file1','new-file1')

# konvence je taková, že knihovna je soubor s příponou .py
# ten se hledá v adresářích v listu sys.path
# bacha sys není defaultně importovaná!

import sys

libPath = 'c:\gitlab\cheatsheet\content\perl2pytnon-samples\lib'
if libPath not in sys.path:
    sys.path.append(libPath)

import myModule
myModule.myPrint
```

### Import Path : sys.path, aka PYTHONPATH

- `sys.path`:
  - A list of strings that specifies the search path for modules.
  - Initialized from the environment variable PYTHONPATH, plus an installation-dependent default.
- `PYTHONPATH`:
  - one or more directory pathnames separated by `os.pathsep`

### Tvorba

```
# tree

<CWD>
└───mapping
    └───__init__.py
    └───mapping.py

# usage
from mapping import mapping

```

- `docstring` - každá funkce a každý modul (a třída) může obsahovat docstring. Ten se pak vrací pokud dáš `help(class)`
- `private vars` - pokud proměnná začíná na podtržítko, `help` jí neukazuje; ale ne dostupná (!)
- `properties` - pokud deklaruješ accessor přes `@property` dekorátor, help tu property potom ukazuje

#### **init**.py

- The **init**.py files are required to make Python treat directories containing the file as packages.
- In the simplest case, `__init__.py` can just be an empty file.
- if a package's `__init__.py` code defines a list named `__all__`, it is taken to be the list of module names
  that should be imported when from package import \* is encountered.

# that should be imported when from package import \* is encountered.

## Práce se soubory

- defaultně se neřeší kódování (tj system default); open ho umí vzít jako parametr na vstupu
- iterace přes filehandle jde na jednotlivé řádky; defaultně se neprovádí `chomp`
- ekvivalent `chomp` z Perlu je metoda `rstrip` na stringu - pokud nedostane parametr, uřízne zprava všechy "bílé znaky" (jinak jen kombinaci ze vstupu)

| open mode     | parametr |
| ------------- | -------- |
| read          | r        |
| write         | w        |
| append        | a        |
| read binary   | rb       |
| write binary  | wb       |
| append binary | ab       |

- `os`
  - `rename`
  - `remove`

### Základní testy, remove, rename

- `os`
  - `path.exists(cesta)`
  - `rename(old, new)`
  - `remove(what)`

```
import os

if os.path.exists('c:/temp/neexistuje'):
    os.remove('c:/temp/neexistuje')
else:
    print ('!neexistuje!')
```

### Řádek po řádku ...

```python
fh = open('c:\gitlab\cheatsheet\content\perl2python.md','r', encoding="utf-8")

for line in fh:
    print('>>' + line.rstrip(), end='<<\n')

fh.close
```

```python
for line in open(filename):
    # code to process each line
```

### stdin

```python
import sys
for line in open(sys.stdin):
    ...                 # code to process each line
```

### Zápis do souboru

```python
fh = open('c:/temp/test.md','w', encoding="utf-8")

for line in range(4):
    fh.write("Hodnota je %d\n" % (line))

fh.close

```

# Moduly

## collections

**deque**

```
names = collections.deque( [ 1,2,3,4] )
names.popleft()
names.appendleft()
```

## pathlib

Jako `Path::Tiny`, ale není dotažená ztoho pohledu, že vrací OS specific objekty, a jejich metody prostě nejsou identické.

```python
from pathlib import Path

root = Path(r'c:/temp')
subdir = root / 'db' / 'teradata'
subdir.mkdir(parents=True,exist_ok=True)

# zápis do souboru s jeho automatickým uzavřením
myFile = subdir / ( '%s.sql' % ('tabulka'))
with myFile.open(mode='w'):
    myFile.write_text('nějaký UTF8 text')   # existuje .read_text, která dělá pravý opak

with myFile.open(mode='r'):
    comments = [ line.strip() for line in myFile if line.startswith('#') ]

# části názvu
file = Path('/home/gahjelle/realpython/test.md')
fileName = file.name            # test.md
baseName = file.stem            # test
fileExt = file.suffix           # .md
fileParent = file.parent        # nadřazená složka
fileRoot = file.anchor          # '/'



# další metody
my homeDir = pathlib.Path.home()
my cwdDir = pathlib.Path.cwd()



# najdi všechny MD soubory pod kořenovým adresářem, pokud mají v názvu PARTY
rootDir = Path(r'c:\BI_Domain\bimain\meta\mappings')
mdFiles = [ f for f in rootDir.rglob('*.md') if "party" in str(f).lower() ]

# kódová stránka a nepřeložitelné znaky : https://www.python.org/dev/peps/pep-0383/
# https://docs.python.org/3/howto/unicode.html
for f in mdFiles:
    # tady předpokládám že jde o soubor v UTF8, ale pokud se neačtou data OK, nepadni, a proveď replacement
    # http://python-notes.curiousefficiency.org/en/latest/python3/text_file_processing.html
    # https://docs.python.org/3/library/codecs.html#error-handlers
    with open(f, errors="surrogateescape") as h:
        for line in h.readlines():
            ... process line

```

## OPEN a invalidní Unicode

```
text = Path('soubor').read_text(errors='replace')
```

That means that with replace, any offending byte will be replaced with the same `U+FFFD` REPLACEMENT CHARACTER,
while with surrogateescape each byte will be replaced with a different value.
For example a `\xe9` would be replaced with a `\udce9` and `\xe8` with `\udce8`.

So with `replace`, **you get valid unicode characters, but lose the original content of the file**, while with `surrogateescape`,
you can know the original bytes (and can even rebuild it exactly with `.encode(errors='surrogateescape')` ),
but your unicode string is incorrect because it contains raw surrogate codes.

Long story short: if the original offending bytes do no matter and you just want to get rid of the error, replace is a good choice,
and if you need to keep them for later processing, surrogateescape is the way to go.

## Teradata přes nativní driver - je hrozně ukecaný, nelíbí se mi

[https://downloads.teradata.com/tools/reference/teradata-python-module](https://downloads.teradata.com/tools/reference/teradata-python-module)

```python
from os import environ
import teradata
import re

def td_connect(system, user, password, authentication='LDAP', driver="Teradata Database ODBC Driver 16.20"):
    dbh = teradata.UdaExec(appName='python', version='1.0', logConsole=False)
    session = dbh.connect(method="odbc", system=system, driver=driver, username=user, password=password,  authentication=authentication);
    return session

def show_table( session, db, tab ):
    ddl = ''
    for row in session.execute('show table %s.%s' % (db, tab)):
        ddl += re.sub(r'(\r|\n)+',row[0],"\n")
    return ddl

session = td_connect(system='edwprod.cz.o2',user = environ['TO2_DOMAIN_USER'], password = environ['TO2_DOMAIN_PASSWORD'])
sql = show_table(session, 'ep_lnd', 'mds_man_lookup')
```

## Teradata přes pyodbc

Pozor, změny do databáze je nutné explicitně commitovat, viz druhý snippet. Nebo nahodit autocommit=True na connectu.

```
# cnxn = pyodbc.connect('Provider=Teradata;DBCName=EDWPROD_64BIT_TECH;Database=edwprod;UID=%s;PWD=%s;AUTHENTICATION=LDAP' % (environ['TO2_DOMAIN_USER'], environ['TO2_DOMAIN_PASSWORD']))

from os import environ
dbh = pyodbc.connect('DSN=EDWPROD_64BIT_TECH;UID=%s;PWD=%s;AUTHENTICATION=LDAP' % (environ['TO2_DOMAIN_USER'], environ['TO2_DOMAIN_PASSWORD']))
#tohle vypadá jako že to vlastně škodí!
#dbh.setdecoding(pyodbc.SQL_CHAR, encoding='utf-8')
#dbh.setdecoding(pyodbc.SQL_WCHAR, encoding='utf-8')
#dbh.setdecoding(pyodbc.SQL_WMETADATA, encoding='utf-8')
#dbh.setencoding(encoding='utf-8')

cursor = dbh.execute('select * from .....')
for wor in cursor:
    print(row)
dbh.close()
```

```
        ddl = (
            "create table " + tables[tab]["devdb"] + "." + tables[tab]["tab"] +
            " as " + tables[tab]["db"] + "." + tables[tab]["tab"] +
            " with no data and stats;" )
         print( ddl )
         dbh = pyodbc.connect('DSN=EDWTEST_64_LDAP;UID=%s;PWD=%s;AUTHENTICATION=LDAP' % (environ['TO2_DOMAIN_USER'], environ['TO2_DOMAIN_PASSWORD']))
         ddl_cursor = dbh.execute(ddl)
         dbh.commit()
```

## Oracle : cx_Oracle

```
# sosni si PDC metadata
import cx_Oracle
con = cx_Oracle.connect('BIDEV/osstp153@(DESCRIPTION=(ADDRESS=(COMMUNITY=tcp.world)(PROTOCOL=TCP)(HOST=hxpdc2dbpkg.ux.to2cz.cz)(PORT=1533))(CONNECT_DATA=(SID=PDC2)))')
cur = con.cursor()
cur.execute("select job_name from pdc.ctrl_job where engine_id = 0 and job_name not like '%IL_TRANSFORMATION.LOAD_%' and (job_name like 'EP_TGT%IL_TRANSFORMATION%' or job_name like 'EP_TGT%IL_TRANSFORMATION%') order by job_name")
res = [ r[0] for r in cur.fetchall() ] # pouze první sloupec
con.close()

```

## YAML

**serializace** : pyaml

```
import yaml
from pathlib import Path
with Path('git-commits.yaml') as f:
    f.write_text( yaml.dump(commits) )
```

**deserializace**: pyaml

```
    import yaml
    from pathlib import Path
    cache = Path('datafix-pdc.yml')
    if cache.exists():
        with open(cache) as f:
            return yaml.load(f, Loader=yaml.FullLoader)
```

# Dependency management

## Poetry

Nefunguje na windows ???

[poetry](https://python-poetry.org/docs/basic-usage/)

```
poetry new <projekt>
cd <projekt>
poetry add <knihovna>
```

# Templating

```
from jinja2 import Template

t = """
[ model = {{model}} ]

{% for key,val in properties.items() %}
{% if "\n" in val %}{{key}} = ->
    {{val}}
<-
{%else%}{{key}} = {{val}}{%endif %}
{% endfor %}

print r.render(model='ep_tgt', properties={'ordered':'dictionary'}
"""
```

# Error handling

[merry](https://github.com/miguelgrinberg/merry/)

```
from merry import Merry

merry = Merry()

@merry._try
def crashes():
    raise Exception('dolu')

@merry._except(IOError)
def ioerror():
    print("Error: can't write to file")

@merry._except(Exception)
def catch_all(e):
    print('Unexpected error: ' + str(e))


crashes()               # tohle spadne na chybu...
print('aha')            # ... ale merry ji odchytí a program jede dál
```

# Testing

- `unittest` - součást core
- `pytest` - asi nejčastěji používaná knihovna

## PyTest

- hledá soubory s názvem `test_*` a v nich funkce s názvem `test_`
- jednotlivé testy používají pouze `assert` a za tím logickou podmínku
- exceptions:
  - odchycení přes context manager: `with pytest.raises(FileNotFoundError):`, pokud nehodí, je to failed
  - pokud neodchytíš, je to failed test

## Golden Files

```python
import a_module
import os

moduleDir = os.path.dirname(a_module.__file__)      # najdi adresář, kde leží daný modul
goldenFile = moduleDir + '/_tests/golden.md'        # najdi goldenFile

```

# Virtualizace na windows

**freeze**

```
python -m venv venv
venv\Scripts\activate
pip install ....
pip freeze > requirements.txt
```

**precommit workflows**

```
pip install black flake8
pre-commit install
```

obsah <gitrepo>/.pre-commit-config.yaml

```
repos:
-   repo: https://github.com/ambv/black
    rev: stable
    hooks:
    - id: black
      language_version: python3.6
-   repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v1.2.3
    hooks:
    - id: flake8
```

# Make BAT file with python code

- the `'''` respectively starts and ends python multi line comments.
- `0<0# : ^` is more interesting
  - due to redirection priority in batch it will be interpreted like :0<0# ^ by the batch script
    which is a label which execution will be not displayed on the screen.
  - The caret at the end will escape the new line and second line will be attached to the first line.
  - For python it will be 0<0 statement and a start of inline comment.

```
0<0# : ^
'''
@echo off
echo batch code
python "%~f0" %*
exit /b 0
'''

print("python code")
```

# Regular expressions

```python
import re

matcher = re.compile(r"(?P<skupina>m)", re.I)

if m:= matcher.fullmatch("nechytne se m"):                          # fullmatch: od začátku až do konce
    print ("fullmatch: " + m.group("skupina"))

elif m:= matcher.match("ani tohle se nechytne m"):                  # match od začátku (ale ne do konce)
    print ("match: " + m.group("skupina"))

elif m:= matcher.search("tohle ale ano: m"):                        # search - kdekoliv v textu
    print ("search: " + m.group("skupina"))

for m in matcher.findall("najde všechna m a M v tomto textu"):      # findall / finditer - všechny match objekty
    print (m)                                       # match objekt
    m.groups()                                      # všechny grupy v match objektu jako tupple
    m.group("skupina")                              # access
    m.groupdict()                                   # key/val dictionary obsahující všechny match skupiny
    m.string[m.start("skupina") : m.end("skupina")] # co se matchlo - trochu jiný zápis


```

# Subprocess

## run and capture

- https://docs.python.org/3/library/subprocess.html#using-the-subprocess-module
- https://docs.python.org/3/library/subprocess.html#subprocess.CompletedProcess
- https://docs.python.org/3/library/io.html#io.TextIOWrapper

tips:

- `capture_output` nastav na True, jinak nedostaneš zpátky stdout/stderr
- `cwd` nahoď na nějaký adresář, pokud potřebuješ vědět "kde to jede"
- `shell=True` pokud jde o shell built in (jako dir, apod)
- pokud nahodíš `encoding` na nějakou hodnotu, dostaneš zpátky string, jinak byte string (a s tím se debilně zachází)
- pokud nahodíš check=True, a návratová hodnota je nenulová, `CalledProcessError` exception will be raised.
- pokud nahodíš `encoding`, měl bys nahodit i `errors` (`strict`, `ignore`, `replace`)

```python
git = subprocess.run([ "git", "fh", str(file) ], cwd=cwd, capture_output=True, encoding="UTF-8", errors="replace")
if git.returncode != 0:
    raise Exception(git.stderr)
first_stdout_line = git.stdout.split("\n")[0]
```

## write na stdin

```python
from subprocess import Popen, PIPE
session = Popen(["command", "param1", "param2"], stdin=PIPE, stdout=PIPE, stderr=PIPE, cwd="c:/BI_Domain/bimain")

# Write to stdin for process to read
session.stdin.write(bytes("some_utf8_encoded_input",'utf-8'))

# Interact with process: Send data to stdin. Read data from stdout and stderr, until end-of-file is reached.
# communicate() returns a tuple (stdout_data, stderr_data).
# The data will be strings if streams were opened in text mode; otherwise, bytes.
stdout_data, stderr_data = session.communicate()
```

### code pages

Each process that attaches to a console shares the console’s active input and output codepages –
i.e. the console doesn’t use a separate set of active codepages for each attached process.

In Python:

| in/out                             | how to detect           |
| ---------------------------------- | ----------------------- |
| console’s active input codepage is | `os.device_encoding(0)` |
| active output codepage is          | `os.device_encoding(1)` |

They both default to the same value, which defaults to the system OEM codepage unless modified in the registry.

However, they can be changed independently of each other via WINAPI SetConsoleCP and SetConsoleOutputCP. Python has no builtin function to change the device encoding, but it’s easy enough to implement via ctypes.

For example:

```
import ctypes
kernel32 = ctypes.WinDLL('kernel32', use_last_error=True)
if not kernel32.SetConsoleCP(1251):
    raise ctypes.WinError(ctypes.get_last_error())
 # Set the active output codepage to UTF-8
if not kernel32.SetConsoleOutputCP(65001):
    raise ctypes.WinError(ctypes.get_last_error())

>>> os.device_encoding(0)
'cp1252'
>>> os.device_encoding(1)
'cp65001'
```

`cmd`, for example, uses the active console output codepage when translating to and from its internal `UCS-2` text encoding.

That said, not many applications follow this pattern. Using a console codepage as a preferred encoding isn’t
documented as a suggested practice, plus it’s limited to Windows and just plain weird. To compare with Unix,
it’s as if there were a way to query the active encoding used by a terminal emulator and a program chose to use that
instead of the LC_CTYPE locale category.
