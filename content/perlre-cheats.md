---
title:  'Perl - regexp cheatsheet'
tags:   [perl, re, perlre, regexp]   
---

viz také [Regex-mastery.pdf](../books/Regex-mastery.pdf)

```
---------------------------------------------------------------------------------------------
Char classes and escape sequences
---------------+-----------------------------------------------------------------------------
   \x{}, \x00  |     character whose ordinal is the given hexadecimal number
   \w          |     ASCI: [a-zA-Z0-9_].                 UNICODE: any unicode word char
   \s          |     ASCII: [\f\t\n\r ]                  UNICODE: any unicode white space
   \R          |     Any line break (CR, LF, CRLF)
   \W \D \S \N |     [^\w] [^\d] [^\s] [^\n]  
---------------+-----------------------------+-----------------------------------------------
Greedy quantifiers                           |      Not greedy variant
---------------------------------------------+-----------------------------------------------
   a?          0-1         'a' characters    |         a??
   a+          1-infinite  'a' characters    |         a+?
   a*          0-infinite  'a' characters    |         a*?     
   a{n,m}      n-m         'a' characters    |         a{n,m}?
   a{n,}       n-infinite  'a' characters    |         a{n,}?
   a{n}        n           'a' characters    |              REDUNDANT
---------------------------------------------+-----------------------------------------------
Grouping and capturing
---------------------------------------------------------------------------------------------
   \g1, \g{name}  Backreference to a specific or previous group
   \g{-1}         RELATIVE backreference to a previous group
---------------------------------------------------------------------------------------------
Anchors
---------------------------------------------------------------------------------------------
   ^           Beginning of string (or beginning of line if /m enabled)
   $           End of string (or end of line if /m enabled)
   \A          Beginning of string  
   \Z          End of string (or before new-line)
   \z          End of string
   \b          Word boundary (start-of-word or end-of-word)
---------------------------------------------------------------------------------------------
Modifiers
---------------------------------------------------------------------------------------------
  /m           Change ^ and $ to match beginning and end of line respectively
  /s           Change . to match new-line as well
  /a           ASCII: mach  \d , \s, \w to ANSI range only. (switch off /u Unicode)
  /aa          ASCII: forbid ASCII/non-ASCII matches (like "k" with \N{KELVIN SIGN} ),
---------------------------------------------------------------------------------------------
Extended (/x)
---------------------------------------------------------------------------------------------
  (?:pattern)               Non-capturing group.
  (?<NAME>pattern)          A named capture group: $+{NAME}
  (?<NAME>(?&NAME_PAT))     Match using a pattern NAME_PAT and store it as NAME
  (?(DEFINE)
      (?<NAME_PAT>....   )  Define NAME_PAT to be used by (?&NAME_PAT)
  )  
  (?=pattern)       positive look-ahead   :: /\w+(?=\t)/   :: word followed by a tab
  (?!pattern)       negative look-ahead   :: /foo(?!bar)/  :: "foo" not followed by "bar"
  (?<=pattern)      positive look-behind  :: /(?<=\t)\w+/  :: word that follows a tab
  (?<!pattern)      negative look-behind  :: /(?<!bar)foo/ :: "foo" not following "bar"
---------------------------------------------------------------------------------------------
                       Perlrun
---------------------------------------------------------------------------------------------
-e  execute one liner                       |  -i.bak      inplace edit with backup
-E  execute one liner, with say and stuff   | 
-n  while(<>) on -e/-E... each line in file |  -p          -n and print $_
---------------------------------------------------------------------------------------------
#check: perl -MEncode -n -e "Encode::decode($_, 'UTF-8', Encode::FB_CROAK)" _file_ > nul
#head:  perl -p -e "exit(0) if $.>10" _soubor_  
#sed:   perl -n "s/foo/bar/"_soubor
---------------------------------------------------------------------------------------------
FlipFlop
---------------------------------------------------------------------------------------------    
foreach (@lines) { 
    if ((my $start = /START_MARK/) .. (my $end = /END_MARK/)) { 
        # vypíše všechny řádky mezi START_MARK a END_MARK, kromě řádků kde je marker
        print unless ($start or $end);  
    }
}
```