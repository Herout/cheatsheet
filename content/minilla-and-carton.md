---
title:  'Minilla, Carton'
tags:   [perl, carton, minilla, CPAN, dependencies]   
---
# Synopsis

## Setup

```
cpanm Carton
cpanm Minilla
```

File: ~/.pause 
```
user <YOUR-PAUSE-ID>
password <your-password>
```

## First release

* [carton](https://metacpan.org/pod/distribution/Carton/script/carton)
* [scan_prereqs_cpanfile](https://metacpan.org/release/App-scan_prereqs_cpanfile)
* [minilla](https://metacpan.org/pod/Minilla)


```
# create a new project with Minilla
minil new Space::Cowboy
cd Space-Cowboy

# write your code
vi lib/Space/Cowboy.pm t/space-cowboy.t

# manage your dependencies with Carton
cpanm App::scan_prereqs_cpanfile
scan-prereqs-cpanfile --ignore=bin > cpanfile
vi cpanfile                       

# deploy dependencies
carton install

# release
git commit -a
carton exec minil release
```


# Jak na závislosti: instalace na remote počítači

```
git clone https://gitlab.com/Herout/perlGen.git
cd perlGen
carton install --deployment
```

Následně je žádoucí doplnit do proměnné PATH cestu k `perlgen\bin`.


---

# Longer version

## How to publish to CPAN

__Workflow__
* Create a PAUSE.perl.org account* 
* Use Minilla to create a distribution (.tar.gz)* 
* Use Carton to manage dependencies for your code* 
* Use Minilla to upload your distribution (.tar.gz) to PAUSE* 



### PAUSE

* PAUSE is the Perl Authors Upload SErver* 
* You need a PAUSE account to upload/manage CPAN modules
* MetaCPAN.org uses PAUSE accounts for authentication
* When you upload a module, you will upload it to PAUSE
* After you upload a module to PAUSE, its indexed, and then distributed to CPAN mirrors

**Create a PAUSE account**
* (http://pause.perl.org) - Click 'Create account'

**Save your credentials in ~/.pause**
```
user <YOUR-PAUSE-ID>
password <your-password>
```

Minilla uses this to upload to CPAN.


## Minilla

**What is minilla**
```
cpanm Minilla
```

* Minilla is a kiaju -- the first of several young Godzillas
* A simple alternative to Dist::Zilla
* Not extensible
* Created by Tokuhirom


**Create a new project with Minilla**
```
minil new Space::Cowboy
```

This generated 9 boiler plate files
```
$ tree Space-Cowboy
.
├── Build.PL            # script to install Space::Cowboy
├── Changes             # changes for each release
├── cpanfile            # dependencies
├── lib
│   └── Space
│       └── Cowboy.pm
├── LICENSE
├── META.json           # meta data about the module for metacpan
├── minil.toml          # config file for minilla
├── README.md           # used by Github
└── t
    └── 00_compile.t   

3 directories, 9 files
```



## Carton
Carton installs and tracks dependencies
Installs dependencies to the './local' directory
you create a [cpanfile](http://metacpan.org/cpanfile) to list your dependencies
carton generates a cpanfile.snapshot which enables other developers to use the exact same deps

```
cpanm Carton
```


**cpanfile example**
```
requires Moo,     '0.2000';         # PINNED VERSION
requires Plack,   '== 0.9980';      # MINIMUM VERSION
requires Starman, '>= 0.2000';

on test => sub {
    requires 'Test::More';
};

on develop => sub {
    recommends 'Dist::Zilla'; 
};
```

**using carton**
```
carton                       # installs dependencies to ./local
carton exec bin/space-cowboy # adds ./local to $PERL5LIB
```

**Bash env shortcuts**
```
export PERL5LIB=./local/lib/perl5:./lib:$PERL5LIB
export PATH=./local/bin:$PATH
```

**release**
```
carton exec minil release
```

Minilla:
* checks if you forgot to check in any files
* runs your tests
* runs extra tests
* generates a README.md from your pod
* generates a META.json file for MetaCPAN.org
* generates a BUILD.pl file for MetaCPAN.org
* bumps the version number
* builds a distribution (tar.gz)
* uploads your distribution (tar.gz) to CPAN using credentials from ~/.pause
* tags your release in git
* pushes your code and tags to github


