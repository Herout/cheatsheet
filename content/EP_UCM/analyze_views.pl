# skript pro rozbor a řešení závislostí mezi VIEWS 
# specializace na EP_UCM
# pozor, move na konci je zakomentová

use v5.24;    # implicit strict
use utf8;
use open qw/:std :utf8/;   # UTF-8 default on all handles (+STDIN/STDOUT/STDERR)
use Carp;                  # carp=warn, croak=die, confess=die
use Path::Tiny;
use File::Find::Rule; 
use Data::Dumper;

my $root = path($0)->parent;

#
# zdrojový a cílový adresář. Zdrojový adresář má obsahovat VŠECHNA views z EP_UCM databáze
# do cílového adresáře budou přesunuta závislá views
#
my $src = $root->child('21_views');
my $tgt = $root->child('22_dependent_views');
my @files = map { path($_)->absolute } File::Find::Rule->file()->name( qr/[.]sql$/i )->in( "$src" );
my $all_views;
my $dependent_views;

# získej seznam views nad ep_ucm
foreach my $f (@files) {
    my $b = $f->basename(qr/[.]sql$/i);
    $all_views->{$b}++;
}

# projdi jednotlivá views
foreach my $f (@files) {
    my $b = uc $f->basename(qr/[.]sql$/i);
    my $c = $f->slurp_raw;
    
    my $needs;
    while ($c =~ /EP_UCM[.]([a-z0-9_]+)/gi) {
        my $hit = uc $1;
        
        if ($hit ne $b and $all_views->{$hit}) {        
            $needs->{$hit}++;                
            $dependent_views->{$b}->{file} = $f;
            $dependent_views->{$b}->{dependency} -> {$hit}++;
        }
        #say $b;
        #say Dumper $needs;
    }
    #if ($b eq 'CP_NBO_A1_2') {
    #    say Dumper $needs;
    #    say Dumper $dependent_views->{$b};
    #    say Dumper $all_views->{CP_HSPND_3M};
    #    #die;
    #}
    next unless $dependent_views->{$b};    
}


say 'All       : ' . scalar keys $all_views->%*;
say 'Dependent : ' . scalar keys $dependent_views->%*;

foreach my $dep_view (sort keys $dependent_views->%*) {
    my $src_file = $dependent_views->{$dep_view}->{file};
    my $tgt_file = $tgt->child( $src_file->basename );
    say "$src_file => $tgt_file";
    #$src_file->move($tgt_file);
}