use v5.24;    # implicit strict
use utf8;
use open qw/:std :utf8/;   # UTF-8 default on all handles (+STDIN/STDOUT/STDERR)
use Carp;                  # carp=warn, croak=die, confess=die
use Path::Tiny;
use File::Find::Rule; 
use DBI;
use Text::Trim;


my $root = path($0)->parent;
my $dbh = DBI->connect('DBI:ODBC:EDWPROD_64BIT_TECH', $ENV{TO2_DOMAIN_USER}, $ENV{TO2_DOMAIN_PASSWORD}, {RaiseError => 1, LongReadLen => 1024*1024});

my @list = map { chomp; my @a = split /\./, $_; \@a } <DATA>;
my @create_columns;

foreach my $rec (@list) {
    my ($db, $tab) = $rec->@*;
    next unless $tab;
    say $tab;
    my $sql = qq/
        SELECT columnName, CommentString
        FROM dbc.columns
        WHERE databaseName = '$db' AND tableName = '$tab'
        order by columnId
    /;
    my $cols = $dbh->selectall_arrayref($sql);
    foreach my $c ($cols->@*) {
        my ($cn, $cc) = $c->@*;        
        trim $cn;
        trim $cc;
        $cc =~ s/"/""/g;
        my $dt = 'DUMMY';
        if ($cn =~ /_id$/i) {
            $dt = 'INTEGER';
        }
        if ($dt eq 'DUMY' and $cn =~ /dttm/i) {
            $dt = 'TIMESTAMP(0)';
        }
        if ($dt eq 'DUMY' and $cn =~ /dt|date/i) {
            $dt = 'DATE';
        }
        if ($dt eq 'DUMY' and $cn =~ /amt/i) {
            $dt = 'DECIMAL';
        }
        if ($dt eq 'DUMY' and $cn =~ /amt/i) {
            $dt = 'DECIMAL';
        }
        if ($dt eq 'DUMY' and $cn =~ /hist_type/i) {
            $dt = 'INTEGER';
        }
        
        push @create_columns, qq/create_column "$tab", "$cn", "$dt",false,false,"$cc"/;
    }    
}

my $header = q{
'************************************************************************************************************************************************************
' Author:           Jan Herout
' Purpose:          ____________
' Date:             ____________
'************************************************************************************************************************************************************
option explicit
const TO2_GLOBAL = "c:\BI_Domain\Development\_SETTINGS\Scripts\"
executeGlobal CreateObject("Scripting.FileSystemObject").openTextFile( TO2_GLOBAL & "globalScript.txt" ).readAll() 
'************************************************************************************************************************************************************

dim l : set l = new class_locator
dim tab

sub create_column (byval tab, byval col, byval datatype, byval primary, byval mandatory, byval comment)

    dim oTab
    if l.locate_table(tab) then 
        set oTab = l.found 
    else 
        msgbox "Tabulka neexistuje? " & tab
        output 1/0
    end if
    
    dim oCol
    set oCol = oTab.columns.createNew()
    oCol.setnameandcode col, "", true
    oCol.datatype = datatype
    oCol.primary = primary
    if not primary then oCol.mandatory = mandatory
    oCol.comment = comment
end sub


};

$root->child('list.vbs')->spew(join "\n", $header, '','', @create_columns);




__DATA__
EP_UCM.CP_HSPND_3M
EP_UCM.CP_NBO_A1_0
EP_UCM.CP_NBO_A1_1
EP_UCM.CP_NBO_A1_2
EP_UCM.CP_NBO_A1_3
EP_UCM.CP_NBO_A2_0
EP_UCM.CP_NBO_A2_1
EP_UCM.CP_NBO_A3_0
EP_UCM.CP_NBO_A3_1
EP_UCM.CP_NBO_A4_0
EP_UCM.CP_NBO_A4_1
EP_UCM.CP_NBO_A4_2
EP_UCM.CP_NBO_A6_0
EP_UCM.CP_NBO_A6_1
EP_UCM.CP_NBO_A7_0
EP_UCM.CP_NBO_A7_1
EP_UCM.CP_NBO_B1_0
EP_UCM.CP_NBO_B1_1
EP_UCM.CP_NBO_B1_2
EP_UCM.CP_NBO_B1_3
EP_UCM.CP_NBO_B2_0
EP_UCM.CP_NBO_B2_1
EP_UCM.CP_NBO_B2_2
EP_UCM.CP_NBO_B3_0
EP_UCM.CP_NBO_B3_1
EP_UCM.CP_NBO_B4_0
EP_UCM.CP_NBO_B4_1
EP_UCM.CP_NBO_B4_2
EP_UCM.CP_NBO_B6_0
EP_UCM.CP_NBO_B6_1
EP_UCM.CP_NBO_B7_0
EP_UCM.CP_NBO_B7_1
EP_UCM.CP_NBO_C1_0
EP_UCM.CP_NBO_C1_1
EP_UCM.CP_NBO_C2_0
EP_UCM.CP_NBO_C2_1
EP_UCM.CP_NBO_C3_0
EP_UCM.CP_NBO_C3_1
EP_UCM.CP_NBO_C4_0
EP_UCM.CP_NBO_C4_1
EP_UCM.CP_NBO_C4_2
EP_UCM.CP_NBO_C6_0
EP_UCM.CP_NBO_C6_1
EP_UCM.CP_NBO_C7_0
EP_UCM.CP_NBO_C7_1
EP_UCM.CP_NBO_D1_0
EP_UCM.CP_NBO_D1_1
EP_UCM.CP_NBO_D2_0
EP_UCM.CP_NBO_D3_0
EP_UCM.CP_NBO_D3_1
EP_UCM.CP_NBO_D4_0
EP_UCM.CP_NBO_D4_1
EP_UCM.CP_NBO_D4_2
EP_UCM.CP_NBO_D6_0
EP_UCM.CP_NBO_D6_1
EP_UCM.CP_NBO_D7_0
EP_UCM.CP_NBO_D7_1
EP_UCM.CP_NBO_INIT_DSL
EP_UCM.CP_NBO_INIT_FV
EP_UCM.CP_NBO_INIT_M
EP_UCM.CP_NBO_INIT_O2TV
EP_UCM.OIC_SLS_TXN_EVAL
EP_UCM.TS_NBO_C7_0
EP_UCM.TS_NBO_C7_1
EP_UCM.TS_NBO_D7_0
EP_UCM.TS_NBO_D7_1