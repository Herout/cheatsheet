---
title: 'Perl a znakové sady'
tags: [perl, unicode, utf-8]
---


# Zdroje

- [https://stackoverflow.com/questions/6162484/why-does-modern-perl-avoid-utf-8-by-default/6163129#6163129](https://stackoverflow.com/questions/6162484/why-does-modern-perl-avoid-utf-8-by-default/6163129#6163129)
- [https://perldoc.perl.org/perluniintro.html#Legacy-Encodings](https://perldoc.perl.org/perluniintro.html#Legacy-Encodings)
- [https://perldoc.perl.org/perlunicode.html](https://perldoc.perl.org/perlunicode.html)

## Jak dostat češtinu na standardní výstup (windows)

```perl
use v5.24;    # implicit strict
use utf8;
use Encode qw/encode decode/;

# sekce DATA je nakódovaná UTF-8, takže i stringy v listu jsouy UTF-8 encoded
my @lines = <DATA>;

# na výstupu ale musíme explicitně data zkonvertovat
print encode('cp852', $_) foreach @lines;

__DATA__
příšerně žluťoučký kůň
úpěl ĎÁBELSKÉ Řecké ódy
do žabího rybníčku
```

## Co na to ROOT.CZ?

[Zdroj](https://www.root.cz/clanky/perlicky-kodovani-znaku-a-unicode/)

### Kódování, Unicode a Perl

Experimentální podpora Unicode byla zavedena již ve verzi 5.6, ve verzi 5.8 byl odstraněn status „experimentální“. Podpora se dělí na práci s Unicode textem a podpora skriptů psaných v Unicode.

Pro práci s Unicode textem Perl používá interní reprezentaci textu, která je platformově závislá a můžeme si ji představit jako černou skříňku. Perlu potřebujeme pouze dát najevo, jakým kódováním jsou vstupní/výstupní data zakódována a/nebo konverzi udělat ručně. Na většině systémů je Perl schopen také převádět mezi starými znakovými sadami (kódovými stránkami) a interní Unicode reprezentací.

Nastavit kódování na souboru lze pomocí systému vrstev PerlIO. Tento systém umožňuje nastavovat na souborech různé vlastnosti kódování, ne nutně spjaté s Unicode (např. režim ukončování řádků). Jednou z vrstev je utf8 pro UTF-8, druhou je encoding(…) pro jakékoliv jiné kódování. Nastavení vrstvy lze provést při otevírání souboru pomocí open, nebo na již otevřeném souboru operátorem binmode.

```perl
use strict;
use warnings;

binmode(STDIN, ':encoding(iso8859-2)');
binmode(STDOUT, ':utf8');

print while (<>);
```

V případě otevírání by se použila syntax open(my $f, '<:utf8', 'soubor.txt'). Ruční konverzi provedeme pomocí modulu Encode:

```perl
use strict;
use warnings;

use Encode qw/encode decode/;

while (my $line = <>) {
    $line = decode('iso8859-2', $line);

    # v $line je nyní text v interní reprezentaci

    print encode('utf8', $line);
}
```


Opačným směrem se pochopitelně ne vždy musí konverze podařit.

```perl
echo 'text,смотри' | perl -MEncode -ne 'print encode("iso8859-2", decode("utf8", $_))'
text,??????
```

Třetím parametrem můžeme modulu Encode říci, co v takové situaci udělat.

```
echo 'text,смотри' | perl -MEncode -ne 'print encode("iso8859-2", decode("utf8", $_), Encode::FB_HTMLCREF)'
text,&#1089;&#1084;&#1086;&#1090;&#1088;&#1080;

FB_WARN nebo FB_CROAK: "\x{0441}" does not map to iso-8859-2 at...
FB_PERLQQ: text,\x{0441}\x{043c}\x{043e}\x{0442}\x{0440}\x{0438}
FB_XMLCREF: text,&#x441;&#x43c;&#x43e;&#x442;&#x440;&#x438;
FB_QUIET: text,
```