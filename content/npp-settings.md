---
title:  'Notepad++, nastavení - Perl, Go, MD'
tags:   [notepad, npp, settings, md, go]
---

# Markdown

```
//********************************************************
//              MARKDOWN PRETTIFY
//********************************************************
//  depends on : https://prettier.io/ + node.js
//  depends on : perl + Path::Tiny
//--------------------------------------------------------




npp_save $(FULL_CURRENT_PATH)

// prettify
c:\BIN.Scripts\node_modules\.bin\prettier.cmd "$(FULL_CURRENT_PATH)" > "$(FULL_CURRENT_PATH).tmp"

// move
perl -MPath::Tiny -e "$a=path('$(FULL_CURRENT_PATH).tmp'); $a->move( '$(FULL_CURRENT_PATH)' );"

npp_open $(FULL_CURRENT_PATH)
```

# Perl

Execution plugin

**perltidy a perlcritic**

```
//********************************************************
//              PERL TIDY A CRITIC
//********************************************************

npp_save $(FULL_CURRENT_PATH)
perl -x -S perltidy -b "$(FULL_CURRENT_PATH)" -ole=win -pro=c:\BI_Domain\Development\_DOCUMENTS\kucharky\PERL\perltidyrc
c:\Strawberry\perl\site\bin\perlcritic.bat --profile "c:\BI_Domain\Development\_DOCUMENTS\kucharky\PERL\.perlcritic" $(FULL_CURRENT_PATH)
npp_open $(FULL_CURRENT_PATH)
```

Krom toho nastavení pro highlighting (Shift + F6).

| highlight mask                                   |   color          |
|------------------                                | --------         |
| `[v] [* at line %LINE%, column * (Severity: 4)]` | `0xFF 0x00 0x00` |
| `[v] [* at line %LINE%, column *]`               | `0xFF 0x00 0x00` |


**perlrun**
```
npp_save $(FULL_CURRENT_PATH)
perl "$(FULL_CURRENT_PATH)"
```


# Golang

```
//********************************************************
//              GOFMT + GOIMPORTS
//********************************************************

npp_save $(FULL_CURRENT_PATH)
gofmt -s -w $(FULL_CURRENT_PATH)
goimports -w $(FULL_CURRENT_PATH)
npp_open $(FULL_CURRENT_PATH)
```
