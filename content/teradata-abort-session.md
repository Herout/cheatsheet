---
title: 'kill session  in Teradata'
tags:  [ teradata, session, abort, kill ]
---

# Abort session

## investigate and kill

```sql
SELECT 'AbortSessions (' || HostId || ', ''' || UserName || ''', ' || SessionNo || ', ''Y'', ''Y'');' AS abort_command, t1.*
FROM TABLE (MonitorSession(-1, '*', 0)) AS t1 
ORDER BY reqSpool DESC
```

## only kill

- pod účtem pod kterým session jede

```sql
SELECT SYSLIB.AbortSessions (-1, '*',  <session_id>, 'Y', 'Y');
```