---
tags:   [pdc,perl]
title:  'Formátování system_info.xml'
---

# formátování system_info

```perl
    use v5.24;                  # implicit strict
    use utf8;
    use open qw/:std :utf8/;   # UTF-8 default on all handles (+STDIN/STDOUT/STDERR)
    use Carp;                  # carp=warn, croak=die, confess=die
    use Path::Tiny;
    use File::Find::Rule; 

    my $root = path($0)->parent;
    my @pmrootdirs = map {$root->child($_) } qw/
        PMRootDir_E20
        PMRootDir_E21
        PMRootDir_E22
        PMRootDir_EDW
        PMRootDir_LND
        PMRootDir_LND_I
    /;
    my $virt_node = $root->basename;


    {
        my @dirs = map { $_->child(qw/Security Passwords/)} @pmrootdirs;
        my @files = map { $_->child( 'system_info.xml') } @dirs;
        foreach my $file (@files) {
            next unless $file->is_file;
            my $c = $file->slurp_raw;
            #$c =~ s/\\EDW_NEWPDC/\\$virt_node/gi;
            #$c =~ s/PDC_X0548834/PDC__zadej_uzivatele/gi;
            $c =~ s/\t/    /gi;
            
            my @lines = split /\R/, $c;
            my @out_lines;
            foreach my $line (@lines) {
            
                if ($line =~ /^\s*([a-z0-9_]+)\s*=\s*("[^"]+")\s*$/i) {
                    my $k = $1 . (' ' x 80);
                    $k = substr $k, 0, 40;
                    my $v = $2;
                    $line = "    $k = $v";
                    push @out_lines, $line;
                } else {
                    $line = '' if $line =~ /^\s+$/;
                    push @out_lines, $line;
                }
            }
            
            $c = join "\r\n", @out_lines;
            $file->spew_raw($c);
        }
    }
```