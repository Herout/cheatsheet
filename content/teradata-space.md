---
title: 'give space in Teradata'
tags:  [ teradata, space, permspace ]
---



## PERM: pr�zkum

### PERM: na datab�zi

```sql
    SELECT databasename, max_perm_gb - current_perm_gb AS free_perm_gb, max_perm_gb, current_perm_gb 
    FROM (
        SELECT  DATABASENAME, Cast(Sum(MAXPERM)/(1024*1024*1024) AS DECIMAL(7,2)) AS MAX_PERM_GB, Cast(Sum(CURRENTPERM)/(1024*1024*1024) AS DECIMAL(7,2)) AS CURRENT_PERM_GB
        FROM DBC.DISKSPACE 
        WHERE DATABASENAME LIKE 'ED0%' 
        GROUP BY DATABASENAME 
    ) a
    ORDER BY free_perm_gb DESC
```

### PERM: Na tabulce

```sql
    SELECT trim(databasename) as databasename, trim(tablename) as tablename, SUM(currentperm)/(1024*1024*1024) as perm_gb, SUM(currentperm) as perm_b   
    FROM DBC.tablesizeV
    WHERE databasename in ('EP_LND' ) AND tablename in (
      'S_CCR_OCM_CRM_PARTY_TS'
    , 'CCR_OCM_CRM_PARTY_TS'
    , 'CCR_OCM_CRM_ASSET'           
    , 'S_CCR_OCM_CRM_ASSET'
    ) 
    group by 1,2 order by 1,2;
```

## PERM: realokace

```sql
CREATE DATABASE SPACEMOVE FROM ED1_UCM_WRK AS PERM = 1024*1024*1024 /* GB */ * 10;
GIVE SPACEMOVE TO ED1_UCM;
DROP DATABASE SPACEMOVE;
```