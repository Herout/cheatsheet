
## bytes, bufio

- konverze byte slice na scanner

    scanner := bufio.NewScanner(bytes.NewReader(byteSlice))   // import bytes
    for scanner.Scan() {
		fmt.Println(scanner.Text()) // Println will add back the final '\n'
	}
