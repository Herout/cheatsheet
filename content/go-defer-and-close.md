---
title:  'Close, defer, and errors'
tags:   [go, error, defer, close, file]
---

# Defer a exekuce

## os.Exit()

Defered calls are not executed (exits immediately).

## runtime.Goexit()

- Goexit terminates the goroutine that calls it. No other goroutine is affected. Goexit runs all deferred calls before terminating the goroutine. 
- Because Goexit is not panic, however, any recover calls in those deferred functions will return nil.
- Calling Goexit from the main goroutine terminates that goroutine without func main returning. 
- Since func main has not returned, the program continues execution of other goroutines. If all other goroutines exit, the program crashes.

So if you call it from the main goroutine, at the top of main you need to add

defer os.Exit(0)

Below that you might want to add some other defer statements that inform the other goroutines to stop and clean up.


## panic

We can take advantage of panic and recover. It turns out that panic, by nature, will honor defer calls but will also always exit with non 0 status code and dump a stack trace. 
The trick is that we can override last aspect of panic behavior with:

```go
package main

import "fmt"
import "os"

type Exit struct{ Code int }

// exit code handler
func handleExit() {
    if e := recover(); e != nil {
        if exit, ok := e.(Exit); ok == true {
            os.Exit(exit.Code)
        }
        panic(e) // not an Exit, bubble up
    }
}

// Now, to exit a program at any point and still preserve any declared defer instruction we just need to emit an Exit type:
// It doesn�t require any refactoring apart from plugging a line inside func main:
func main() {
    defer handleExit() // plug the exit handler
    defer fmt.Println("cleaning...")
    panic(Exit{3}) // 3 is the exit code
}

```

# defer a IO

## Close, defer, and errors

package main

    import (
        "fmt"
        "os"
    )

    // deferErr is a simple wrapper which can be used in defered calls returning errors (of functions returning error as named parameter).
    // deferedCall is 'name' of the defered call, errEarlier is an error from outer scope, errDefered is an error from defered call.
    func deferErr(deferedCall string, errEarlier, errDefered error) error {
        if errEarlier == nil && errDefered != nil {
            return fmt.Errorf("%s: defered err = %w", errDefered)
        }
        if errEarlier != nil && errDefered != nil {
            return fmt.Errorf("%s: defered err = %w, err = %w", deferedCall, errDefered, errEarlier)
        }
        if errEarlier != nil && errDefered == nil {
            return errEarlier
        }
        return nil
    }

    // example use of deferErr
    func doSomethingOnFile(fileName string) (err error) {
        f, err := os.Create(fileName)
        if err != nil {
            return
        }
        fakeClose := func() error {
            f.Close()
            return fmt.Errorf("faking file close, which close failed too")
        }
        defer func() { err = deferErr("file close", err /* f.Close() */, fakeClose()) }()

        // do something on the file, which fails, and sets an err
        err = fmt.Errorf("something failed")
        return
    }

    func main() {
        err := doSomethingOnFile("c:\\temp\\test-go-err")
        if err != nil {
            panic(err)
        }
    }
    
    
fingertext shortcut: `cf`