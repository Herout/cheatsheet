---
title:  'Go build tips and tricks'
tags:   [go, build, mage, commit, version]   
---

# go build tips

## Bake GIT commit hash into go binaries

### Makefile

```
GOOS=linux
GOARCH=386

.PHONY: build

GIT_COMMIT := $(shell git rev-list -1 HEAD)

build:
	GOOS=$(GOOS) GOARCH=$(GOARCH) go build -ldflags "-X main.gitCommit=$(GIT_COMMIT)" .
```

### main.go

```go
package main

var gitCommit string

func printVersion() {
	log.Printf("Current build version: %s", gitCommit)
}

func main() {
	versionFlag := flag.Bool("v", false, "Print the current version and exit")
	flag.Parse()

	switch {
	case *versionFlag:
		printVersion()
		return
	}
	// continue with other stuff
}
```


- GIT_COMMIT will store of the value of the current commit hash that our code base is at.
- `-ldflags "-X main.gitCommit=$(GIT_COMMIT)"` - instrukce pro linker: propi� obsah syst�mov� promenn� GIT_COMMIT do promenn� `main.gitCommit`
