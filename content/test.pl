#! /usr/bin/perl
use v5.24;                        # implicit strict
use utf8;                         # source is utf-8 encoded
use warnings;                     # simple sanity
use warnings qw( FATAL utf8 );    # fatalize all unicode warnings

use EDW::Console::Encoding;
use Path::Tiny;

foreach (@ARGV) {
    win_say($_);
}

my $hash = { nejaka => "zkurvená česká hash" };

win_say ("Koza bláznivá", "kráva chudořivá a čímanská", path("c:/"), $hash);
my $in = win_read();
win_say($in);