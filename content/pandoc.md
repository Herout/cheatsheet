## Pandoc

```
docker run --volume /c/BI_Domain/biRfp/doc/:/data pandoc/latex:2.6 to-be.md -o to-be.pdf
docker run --volume /c/BI_Domain/biRfp/doc/:/data gibibit/pandoc to-be.md -t html --css buttondown.css -o to-be.pdf --metadata pagetitle="to-be stav" --env http_proxy=http://internet-proxy-s1.cz.o2:8080/
```

gaze -c "docker run --volume /c/BI_Domain/biRfp/doc/:/data pandoc/latex:2.6 to-be.md -o to-be.pdf" to-be.md

## Plantuml

```
docker run -d -p 8080:8080 plantuml/plantuml-server:jetty
```
[use it](localhost:8080)


