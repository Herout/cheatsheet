.SET FORMAT OFF
.SET HEADING ''
.SET TITLEDASHES OFF
.SET PAGELENGTH 65531
.SET FOLDLINE ON ALL
.SET WIDTH 65531                        -- toto je v 13.10 maxim�ln� povolen� ���ka obrazovky
.SET SEPARATOR 0


/* 
    POZOR. pokud pot�ebuje� vyrobit DV� r�zn� views, ned�lej to z jednoho BTEQ skriptu, ale zalo� druh� BTEQ.
    D�vod - po prvn�m RUNu se ��zen� z d�vodu kter� jsem nezkoumal do hlavn�ho nasazovac�ho BTEQu, tak�e to "druh�" view se nikdy nezalo��.
*/
    


.EXPORT FILE = '10-inline-view.sql4sql', CLOSE
SELECT 
         cmd (TITLE '') 
FROM ( 
    select 'replace view _next_load as select cast(''' || cast(max(cast(load_dttm as date) + 1) as varchar(40)) || ' 00:00:00'' as timestamp(0) FORMAT ''YYYY-MM-DDBHH:MI:SS'') as load_dttm' as cmd
    from EP_OPR.table_loaded
    where db_cd = 'EP_STG' and tbl_cd = '<LOAD_END>'
)  _gen
;
.EXPORT RESET


.RUN FILE = '10-inline-view.sql4sql'