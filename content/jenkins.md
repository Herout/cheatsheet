---
title: "From Perl to Python"
tags: [perl, python]
---

# Jenkins tips and tricks

## Lockable resources

- Mo�nost p�idat nov� je pod [configure system](http://ntbidev403:8080/configure) v sekci Lockable Resources Manager.
- Link na titulce s labelem [lockable resources](http://ntbidev403:8080/lockable-resources) vede pouze na read only seznam existuj�c�ch zdroj�.

## Jenkinsfile

```
node {
                    
    stage('Commit_hash') {
                
        if (fileExists('mdl/do_not_build')) {
            echo 'Current dir: ' + pwd()
            echo 'Contents: ' + readFile('mdl/do_not_build')
            echo 'Do not run if mdl/do_not_build exists.'
            currentBuild.result = 'ABORTED'
            error('Stopping early, because file mdl/do_not_build exists.')
        }  else {
           echo 'execute build'
        }
               
        if (env.BRANCH_NAME == 'master' || env.BRANCH_NAME == 'develop') {
                echo 'Do not run on MASTER and DEVELOP (only on feature/hotfix branches).'
        } else {       
            echo 'Building commit %GIT_COMMIT%'        
            checkout scm
        }
    }
    
    stage('Create_output_dir') {
        if (env.BRANCH_NAME == 'master' || env.BRANCH_NAME == 'develop') {
            echo 'Do not run on MASTER and DEVELOP (only on feature/hotfix branches).'
        } else {
            //--------------------------------------------------------------------------------
            // drop and recreate output dir
            //--------------------------------------------------------------------------------
            bat 'perl -MPath::Tiny -e "path(q!e:/BUILD_OUTPUT/%BUILD_TAG%!)->remove_tree"'
            bat 'mkdir "e:/BUILD_OUTPUT/%BUILD_TAG%"'
            bat 'mkdir "e:/BUILD_OUTPUT/%BUILD_TAG%/metadata"'
            bat 'mkdir "e:/BUILD_OUTPUT/%BUILD_TAG%/output"'
            bat 'mkdir "e:/BUILD_OUTPUT/%BUILD_TAG%/log"'
        }
    }
    
    stage('Prepare_build_config') {
        if (env.BRANCH_NAME == 'master' || env.BRANCH_NAME == 'develop') {
            echo 'Do not run on MASTER and DEVELOP (only on feature/hotfix branches).'
        } else {
            echo 'PREPARING ENVIRONMENT'
                                    
            //--------------------------------------------------------------------------------
            // clone repo containing UDI settings + templates
            //--------------------------------------------------------------------------------
            echo 'GIT CLONE'
            bat 'git clone git@gitce.cz.o2:BIDEV/bimeta.git "e:/BUILD_OUTPUT/%BUILD_TAG%/metadata"'
            
            // tohle tady je protoze soubory potrebuju pod adresarem s UDI projektem
            echo 'COPY METADATA'
            bat 'copy "e:\\BUILD_OUTPUT\\%BUILD_TAG%\\metadata\\ModelMetadata\\*.txt" "e:\\BUILD_OUTPUT\\%BUILD_TAG%\\metadata\\UDI\\CodeGen"'
            //--------------------------------------------------------------------------------
            
            //--------------------------------------------------------------------------------
            // 2. sestaven� konfigurace pro build
            //    na rootu vznikne YML soubor obsahujici instrukce pro build
            //    tento soubor je n�sledne spousten z %WORKSPACE%
            //--------------------------------------------------------------------------------
            echo 'SCAN MODELS AND PREPARE YAML SETTINGS'
            bat 'perl "e:/BUILD_OUTPUT/%BUILD_TAG%/metadata/UDI/BuildBot/KONFIGURACE/udiconfig.pl" --outdir "e:/BUILD_OUTPUT/%BUILD_TAG%"'            
            echo 'Copy aditional files needed by UDI'
            bat 'copy "e:\\BUILD_OUTPUT\\%BUILD_TAG%\\metadata\\ModelMetadata\\*.txt" "e:\\BUILD_OUTPUT\\%BUILD_TAG%\\metadata\\UDI\\CodeGen\\"'  // TXT soubory z powerdesigneru se ctou na �ablon�ch            
        }
    }
    
    // dopadovka a mirrorring mohou be�et pouze pokud je dostupn� zdroj JHE_MRP na EDWTESTu
    lock('JHE_MRP') {
        stage('ImpactAnalysis') {
            if (env.BRANCH_NAME == 'master' || env.BRANCH_NAME == 'develop') {
                echo 'Do not run on MASTER and DEVELOP (only on feature/hotfix branches).'
            } else {
                echo 'IMPACT ANALYSIS IS RUNNING'
                bat 'java -jar "C:/Program Files/UDI Studio/udirunner/udirunner.jar" --iaTask "e:/BUILD_OUTPUT/%BUILD_TAG%/metadata/UDI/ImpactAnalyzer/Workflows/Project_BUILDBOT.xml" --udidir C:/Users/jenkins/AppData/Roaming/.udistudio/dev --newVersion MAIN --project EDW --repositoryConnection "JHE_GRP@Teradata TEST" --repositorySchema JHE_MRP >"e:/BUILD_OUTPUT/%BUILD_TAG%/log/impacts.log" 2>&1'
                bat 'perl e:\\BUILD_OUTPUT\\%BUILD_TAG%\\metadata\\UDI\\buildbot\\konfigurace\\parse_impact_analysis_log.pl "e:\\BUILD_OUTPUT\\%BUILD_TAG%\\log"'
            }
        }
        stage('Mirrorring') {
            if (env.BRANCH_NAME == 'master' || env.BRANCH_NAME == 'develop') {
                echo 'Do not run on MASTER and DEVELOP (only on feature/hotfix branches).'
            } else {
                echo 'MIRRORRING IS RUNNING IS RUNNING'
                bat 'java -jar "C:/Program Files/UDI Studio/udirunner/udirunner.jar" --udidir C:/Users/jenkins/AppData/Roaming/.udistudio/dev --mirrorFile  "e:/BUILD_OUTPUT/%BUILD_TAG%/metadata/UDI/CodeGen/BUILDBOT/mirrorring.mirror" >"e:/BUILD_OUTPUT/%BUILD_TAG%/log/mirror.log" 2>&1'
            }
        }
        
        stage('BUILD') {
            if (env.BRANCH_NAME == 'master' || env.BRANCH_NAME == 'develop') {
                echo 'Do not run on MASTER and DEVELOP (only on feature/hotfix branches).'
            } else {       
                echo 'BUILDING'
                bat 'perl e:\\BUILD_OUTPUT\\%BUILD_TAG%\\metadata\\UDI\\Buildbot\\KONFIGURACE\\pack.pl run e:\\BUILD_OUTPUT\\%BUILD_TAG%\\build.yml build'
            }
        }
        
        stage('CHECK LOGS') {        
            if (env.BRANCH_NAME == 'master' || env.BRANCH_NAME == 'develop') {
                echo 'Do not run on MASTER and DEVELOP (only on feature/hotfix branches).'
            } else {       
                bat 'echo "Vystupni adresar je: \\\\NTBIDEV403\\BUILD_OUTPUT\\%BUILD_TAG%"'
                echo 'Probiha kontrola log souboru z buildu, pad znamena ze vnich byly chyby, coz nemusi nutne znamenat ze vygenerovany kod neni OK.'
                bat 'perl e:\\BUILD_OUTPUT\\%BUILD_TAG%\\metadata\\UDI\\buildbot\\konfigurace\\parse_build_logs.pl "e:\\BUILD_OUTPUT\\%BUILD_TAG%\\log"'
                bat 'perl e:\\BUILD_OUTPUT\\%BUILD_TAG%\\metadata\\UDI\\buildbot\\konfigurace\\parse_impact_analysis_log.pl "e:\\BUILD_OUTPUT\\%BUILD_TAG%\\log"'
            }
        }
        
    }
}
```
