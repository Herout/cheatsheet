---
title:	"vim on windows"
tags:	[vim,windows]
---

# vim on windows

* `.vimrc` se nazývá `_vimrc`
* problémy sčeštinou je možné odstranit nastavením znakové sady na `utf-8`;
  k tomu je potřeba dát si do `_vimrc` zhruba následující

```
set enc=utf-8
set fileencoding=utf-8
set fileencodings=ucs-bom,utf8,prc
set guifont=Monaco:h11
set guifontwide=NSimsun:h12
```

* kromě toho je rozumné nastavit si znakovou sadu terminálu na `utf-8`
* viz [utf-8-windows-console.md](./utf-8-windows-console.md)

# Pohyb v dokumentu

```
             ^
             k              Hint:  The h key is at the left and moves left.
       < h       l >               The l key is at the right and moves right.
             j                     The j key looks like a down arrow.
             v
% - matching brace
500G - jdi na řádek 500
Ctrl + g - ukaž mi pozici v sougoru
gg - jdi na začátek
G - jdi na konec
Ctrl + O - jdi na pozici kde jsi byl před tím, než jsi začal hledat pattern (/?)
```

**modes**
```
    i I o O a A
    R - replace more then one character
    r - replace one character
    vy - select and yank
    p - paste here
```

**motions**
```
  A short list of motions:
    w - until the start of the next word, EXCLUDING its first character.
    e - to the end of the current word, INCLUDING the last character.
    $ - to the end of the line, INCLUDING the last character.
```

**edits**
```
    c - change (ce - until end of word, cw - entire word)
```

**undo a redo**
```
    u      - undo
    Ctrl+R - redo
```

**put**
```
    p - put a deleted line (dd) below current position
```
