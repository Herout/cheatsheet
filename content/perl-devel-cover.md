---
title: 'Devel::Cover'
tags: [perl,coverage,devel,cover,'devel::cover']
---
 
# Synopsis (Windows)


    cover -delete
    set HARNESS_PERL_SWITCHES=-MDevel::Cover
    prove t xt
    cover


* výstup je pod `cover_db/coverage.html`
