---
title:  'UDI a TD 16.20'
tags:   [udi, jdbc, socket error]   
---

# Problém

- pád dopadovky, v logu je chybová hláška která pochází z JDBC driveru; Teradata 16.20 ale JDBC ve verzi pro UDI 3.6.7, což je 15.00

# Instalace nové verze JDBC driveru do UDI

- V services změnit definici teradata driveru, aby ukazoval na správné jar soubory 
  - `terajdbc4.jar`
  - `tdgssconfig.jar` (od verze 16.20 prý není nutný)
- Odstranit následující soubory z podadresáře udirunner
  - `udistudio/udirunner/lib/tdgssconfig-1.0.jar`
  - `udistudio/udirunner/lib/TeradataDriver-1.0-SNAPSHOT.jar`
  - `udistudio/udirunner/lib/terajdbc4-1.0.jar`
- Externí proces (udirunner) si pak převezme definice driverů ze Services
- Správný "Driver class" je jen a pouze: `com.teradata.jdbc.TeraDriver`
- Nesmí být nadefinovány dva drivery se stejnou "Driver class", externí proces bude hlásit chybu classloaderu, že nemůže načíst stejnou třídu dvakrát. Bohužel komponenta Services toto umožňuje.
- Od verze UDIS 4.0.0 je TD driver ve verzi 16.20
- Bez problémů jsem se připojil s driverem verze 14.10 k vmwaru s TD 16.20

# Socket error

- UDI se zjevně připojí a chyba nastane až v průběhu procesu (při nahrávání do MR)
- Bohužel mám jen malou část logu, takže jen odhaduji:
  - Na začátku se proces úspěšně připojí k TD
  - Potom chvíli není s TD žádná komunikace
  - myslím, že trvá dlouho extrakce metadat (dlouho v tomto kontextu může být i 30s)
  - mezitím se spojení do databáze přeruší 

- Něco k chybě jsem nalezl zde:

[https://developer.teradata.com/doc/connectivity/jdbc/reference/current/jdbcug_chapter_5.html]

Toto jsem reprodukovat nedokázal, ale připadá mi to docela pravděpodobné, že connection dropne nějaký podpůrný proces na straně databáze. Nic lepšího zatím nemám..

Dle [https://developer.teradata.com/doc/connectivity/jdbc/reference/current/jdbcug_chapter_2.html]

bych experimentoval s jdbc parametry keepalive a linger

`jdbc:teradata://192.168.1.56/TMODE=ANSI,CHARSET=UTF8,TCP=KEEPALIVE`

