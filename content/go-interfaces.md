---
title:  'go interfaces'
tags:   [go, golang, interface, type assertion, assert, assertion, type]   
---
[source](https://www.youtube.com/watch?v=F4wUrj6pmSI&t=2676s)

[guru](https://godoc.org/golang.org/x/tools/cmd/guru)

# Golang interfaces

## Proč?

- writing generic algorithms
- hiding implementation details
- providing interception points


> Be conservative in what you send, be liberal in what you accept.
> Return concrete types, receive interfaces as parameters.
> Use type assertions to extend behaviour.

### Generic algorithms : příklad, stack

```go
// interface
type Stack interface {
    Push(v interface{}) Stack
    Pop() Stack
    IsEmpty() bool
}

// generic algorithm
func Size(s Stack) int {
    if s.IsEmpty() {
        return 0
    }
    return Size(s.Pop()) + 1
}

```

### Chaining interfaces

```
const input = `nějaký base64 text, který obsahuje zipped bajty`

var r io.Reader = strings.NewReader(input)          // tohle by ale mohlo stejně dobře číst soubor
r = base64.NewDecoder(base64.StdEncoding, r)
r, err := gzip.NewReader(r)
if err != nil { /* do something */ }

// chaining: copy <- unzip <- bese64 decode <- from string
io.Copy(os.Stdout, r)
```

### Type assertion

Tahle technika se používá v stdlib, viz například `fmt.Stringer`

```
func do(v interface{}) {
    s := v.(fmt.Stringer)                   // might panic!
    if s, ok := v.(fmt.Stringer); ok {      // do something if it is CAPABLE to do it
        fmt.Println(s.String())
    }
}
```

### Errors are values
> Don't just check errors, handle them gracefully
> Use type assertion to classify errors

- package Context
- kontroluješ na chybě, co je schopná o sobě sdělit
- chyba může mít metody - jedna z nich musí být error.Error(), jinak to není chyba, ale můžeš tuhle logiku rozšířit

```
type Context interface {
    Done() <-chan struct {}
    Err() error                                         // fce vrací dva typy chyb, Canceled, DeadlineExceeded
    Deadline() (deadline time.Time, ok bool)
    Value(key interface{}) interface{}
}

var Canceled = errors.New("context canceled")
var DeadlineExceeded error = deadlineExceededError{}    // unexported type - proč?

// protože
type deadlineExceededError struct {}
funce (deadlineExceededError) Error() string    { return "......." }   // text chyby
funce (deadlineExceededError) Timeout() bool    { return true }         
funce (deadlineExceededError) Temporary() bool  { return true }

// a potom někde dál....
if tmp, ok := err.(interface { Temporary() bool }); ok {        // je to chyba která MŮŽE být temporary?
    if tmp.Temporary() {                                        // a JE temporary?
                                                                // --> retry
    } else {
                                                                // --> report, die
    }
}

```


### Generic algorithms : sortable

```go
// sort.Interface : https://golang.org/pkg/sort/
type Interface interface {
        // Len is the number of elements in the collection.
        Len() int
        // Less reports whether the element with
        // index i should sort before the element with index j.
        Less(i, j int) bool
        // Swap swaps the elements with indexes i and j.
        Swap(i, j int)
}

// pokud mám něco co vyhoví tomuto rozhraní, mám k dispozici věci jako:
//  func IsSorted(data Interface) bool
//  func Sort(data Interface)
```

### Interception point, middleware

```
// http.Client      ---> http.DefaultTransport
type Client struct {
    Transport RoundTripper
}

type RoundTripper interface {
    RoundTrip(*Request) (*Response, error)
}

// !!! middleware !!!
type headers struct {
    rt http.RoundTripper
    v map[string]string
}

func (h headers) RoundTrip(r *http.Request) (*Response, error) {
    for k, v := range h.v {
        r.Header.Set(k,v)
    }
    return h.rt.RoundTrip(r)
}

// a později v kódu... něco jako
// httpClient.Transport = headers{ rt: http.DefaultTransport, v: map[string]string{ "header" : "value" }}
```