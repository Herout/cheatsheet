---
title:  'Git Cheatsheet'
tags:   [git]   
---

# Nejpoužívanější

Nainstaluj si [git extras](https://github.com/tj/git-extras/blob/master/Commands.md) !

| JAK                                                               | CO                                                         |
| -------------------------------------------                       |----------------------------------------------              |
| `git fh <soubor>`                                                 | ukaž mi historii změn na souboru nebo na masce či adresáři; je to alias, viz dole |
| `git branch --contains d3f21121`                                  | odkud přišel commit?                                       |
| `git undo [N]`                                                    | odcouvej N commitů (N=1 by default); změny mi nech na FS;viz git extras   |
| `git reset-file <file> [commit-ish]`                              | odcouvej jeden soubor na daný commit nebo head             |
| `git lf <commit1> [commit2]`                                      | všechny soubory změněné mezi dvěma commity či na commitu   |
| `git diff --name-status <sha-old> <sha-new> \| perl -ne "print if /^D/"` | vypiš všechny smazané soubory mezi dvěma commity           |
| `git diff --name-status <commit> <commit>`                        | seznam změn a typů změn na dvou commitech                  |
| `git rebase -i`                                                   | squashing of commits                                       |
| `git cherry-pick <commit-hash>`                                   | cherry pick                                                |
| `git difftool <commit> <commit> -- <file>`                        | změny souboru mezi dvěma commity, ale přes winmerge        |
| `git merge-base branch2 branch3`                                  | poslední společný commit na dvou větvích                   |
| `git name-rev <sha>`                                              | který tag jako první obsahuje daný commit ? |
| `git log --all --full-history -- "**/thefile.*"` | které commity měnily daný soubor? |


## Other

| JAK                                        | CO                                                         |
| -------------------------------------------|----------------------------------------------              |
| `git reset --hard <HEAD~1>`                | plný reset o jeden commit zpátky                           |
| `git reset <path>`                         | odeber ze staging area                                     |
| `git rm --cached <file>`                   | smaž soubor & stage                                        |
| `git fsck`, `git gc`                       | maintenance. FS check + garbage collection                 |
| `git clean -f -d --dry-run`                | maže soubory a adresáře které nejsou verzované             |
| `git diff <commit> <commit> -- <file>`     | změny souboru mezi dvěma commity                           |

[On undoing, fixing, or removing commits in git](http://sethrobertson.github.io/GitFixUm/fixup.html)

git contributions za 2020

```
perl -MData::Dumper -e "use v5.24; my @lines=qx/git log --date=format:\"%Y-%m-%d\" --format=\"%ad;%aE\"/; my $data; foreach (@lines) { chomp; my ($d, $a) = split /;/; next unless $d =~ /2020/; $data->{$d}->{$a}++; foreach my $d (sort keys $data->%*) { foreach my $a (sort keys $data->{$d}->%*) { say join qq/\t/, $d, $a, $data->{$d}->{$a}; } }}" > c:\temp\git.csv    
```

## Dohledání branches  commitem (podruhé)

Find branches the commit is on

```
git branch --contains <commit>
```

This will tell you all branches which have the given commit in their history. Obviously this is less useful if the commit's already been merged.

If you are working in the repository in which the commit was made, you can search the reflogs for the line for that commit. Reflogs older than 90 days are pruned by git-gc, so if the commit's too old, you won't find it. That said, you can do this:

```
git reflog show --all | grep a871742
```

to find commit a871742. The output should be something like this:

```
a871742 refs/heads/completion@{0}: commit (amend): mpc-completion: total rewrite
```

indicating that the commit was made on the branch "completion". The default output shows abbreviated commit hashes, so be sure not to search for the full hash or you won't find anything.

git reflog show is actually just an alias for git log -g --abbrev-commit --pretty=oneline, so if you want to fiddle with the output format to make different things available to grep for, that's your starting point!

If you're not working in the repository where the commit was made, the best you can do in this case is examine the reflogs and find when the commit was first introduced to your repo; with any luck, you fetched the branch it was committed to. This is a bit more complex, because you can't walk both the commit tree and reflogs simultaneously. You'd want to parse the reflog output, examining each hash to see if it contains the desired commit or not.

Find a subsequent merge commit

This is workflow-dependent, but with good workflows, commits are made on development branches which are then merged in. You could do this:

```
git log --merges <commit>..
```

to see merge commits that have the given commit as an ancestor. (If the commit was only merged once, the first one should be the merge you're after; otherwise you'll have to examine a few, I suppose.) The merge commit message should contain the branch name that was merged.

If you want to be able to count on doing this, you may want to use the --no-ff option to git merge to force merge commit creation even in the fast-forward case. (Don't get too eager, though, that could become obfuscating if overused.) VonC's answer to a related question helpfully elaborates on this topic

## Zpracování seznamu změn podle commitu

Zjištění seznamu změněných souborů:

```bash
git lf 2e5b66d3395d99a8a3970b54d4bd1447b96cf3ab > c:\temp\ucm_deploy\2e5b66d3395d99a8a3970b54d4bd1447b96cf3ab
git lf f1ea05b7b3418aae990c495d9100cd8cebb7b889 > c:\temp\ucm_deploy\f1ea05b7b3418aae990c495d9100cd8cebb7b889
git lf 73359d6e6d927d1c0299000ce9a1486abbaef398 > c:\temp\ucm_deploy\73359d6e6d927d1c0299000ce9a1486abbaef398
git lf 62f9b20ecb6be92be357e3ffb68452d5d68e2a35 > c:\temp\ucm_deploy\62f9b20ecb6be92be357e3ffb68452d5d68e2a35
git lf 53ef073ed197d9d59140f778089da8547c3f40a9 > c:\temp\ucm_deploy\53ef073ed197d9d59140f778089da8547c3f40a9
```

Skript který zkopíruje změněné soubory v poslední verzi někam bokem:

```perl
use v5.24;    # implicit strict
use utf8;
use open qw/:std :utf8/;   # UTF-8 default on all handles (+STDIN/STDOUT/STDERR)
use Carp;                  # carp=warn, croak=die, confess=die
use Path::Tiny;
use File::Find::Rule; 


#  v cestě, kde je tento skript, se očekává adresář UCM_DEPLOY
my $root = path($0)->parent->child('ucm_deploy');       
# z adresáře se vezme seznam všech souborů
my @files = map { path($_)->absolute } File::Find::Rule->file()->name( '*' )->in( "$root" ); 


# pro jednotlicvé soubory se načte seznam změn
my $uniq_files = {};
foreach my $c (@files){
    my @lines = split /\R/, $c->slurp;
    $uniq_files->{$_}++ foreach  (@lines);
};

# kontrola
# say foreach keys $uniq_files->%*;

# zdrojový a cílový adresář
# $0 - proměnná která drží plnou cestu ke spuštěnému skriptu
my $tgt = path($0)->parent->child('ucm_deploy_pkg'); # relativní cesta ve vztahu k tomuto skriptu
my $src = path('c:\bi_domain\bimain'); # absolutní cesta

# jdi přes seznam změněných souborů
foreach my $f (sort keys $uniq_files->%*) {
    my $sf = $src->child($f);   # cesta k zdrojovému souboru (pod BIMAIN)
    my $tf = $tgt->child($f);   # cesta kam chci soubor nakopírovat
    my $td = $tf->parent;       # cílový adresář (absolutní cesta) kam chci nakopírovat
    
    $td->mkpath;                # vyrob cílový adresář
    $sf->copy($tf);             # zkopíruj source file do target file
}
```

## Git a systémové proměnné

| key           | val                                                                                                 |
|-----------    | ------                                                                                              |
| `no_proxy`    | `.cz.o2`                                                                                            |
| `http_proxy`  | `http://internet-proxy-s1.cz.o2:8080/`                                                              |
| `https_proxy` | `http://internet-proxy-s1.cz.o2:8080/`                                                              |
| `LC_ALL`      | `C.UTF-8` - log potom je vidět česky... ale Perl mi potom nefunguje, a češtinu jsem stejně obešel   |


## Git, Windows, a case sensitivita

- pozor, git je case sensitive, windows ale ne
- pokud někdo změní název složky nebo souboru v gitu z UPPERCASE na lowercase (nebo podobě), z pohledu GITu je to nový soubor
- jeho změny se ale potom přenášení u ostatních na již existující file
- vzniká siruace, kdy git úpravu souboru chce dopravit na dvě místa, což ale na Win nejde provést

**fix**
- oprava: `git rm -r -f C:\BI_Domain\bimain\mstr\SRC\e24\PMROOTDIR_E24`
- opravit FS na poslední stav
- commit změn


## Git a PDM

- na binárních souborech se nesledují konflikty (na úrovni FORKu)

# Moje aliasy a další nastavení

## Nastavení repo na windows

Tohle je rozumné provést abychom se vyhnuli problémům při změně "case" na konkrétních názvech souborů.

```
git config core.ignorecase true
```

## Aliasy

```
git config log.date format:"%Y-%m-%d %H:%M:%S" --replace-all
git config --global alias.fh "log --graph --date=format:'%Y-%m-%d %H:%M:%S' --all --format=\"%Cred%H %Cgreen%ad %Cred%aE %Cblue%f\" --"
git config --global alias.lg "log --date=format:'%Y-%m-%d %H:%M:%S' --format=\"%Cred%H %Cgreen%ad %Cred%aE %Cblue%f\" --all --graph --decorate"
git config --global alias.ll "log --date=format:'%Y-%m-%d %H:%M:%S' --format=\"%Cred%H %Cgreen%ad %Cred%aE %Cblue%f\""
git config --global alias.lf "diff-tree --no-commit-id --name-only -r"
```

## Neustálý dotaz na klíč

```
git config --global credential.helper store
```

... a už by se neměl ptát ... 

## Git commit

[micro](https://github.com/zyedidia/micro) : `git config --global core.editor micro`


### WinMerge jako MERGE a DIFF tool

- funguje dobře, ale v nové verzi háže "assertion failed"
- poslední funkční verze je (zdá se) `2.14`, nivější neinstalovat

```
git config --global merge.tool winmerge
git config --replace --global mergetool.winmerge.cmd "\"d:\portable\PortableApps\WinMergePortable\WinMergePortable.exe\" -e -u -dl \"Base\" -dr \"Mine\" \"$LOCAL\" \"$REMOTE\" \"$MERGED\""
git config --global mergetool.prompt false
git config --global --replace diff.tool winmerge
git config --replace --global difftool.winmerge.cmd "\"d:\portable\PortableApps\WinMergePortable\WinMergePortable.exe\" -e -u -dl \"Base\" -dr \"Mine\" \"$REMOTE\" \"$LOCAL\""
git config --global mergetool.prompt false
git config --global --replace diff.tool winmerge
git config --global --replace merge.tool winmerge
```

## Git log

[source](https://git-scm.com/book/en/v2/Git-Basics-Viewing-the-Commit-History)

- historie na jedné řádce: `git log --oneline`
- historie na jedné řádce: `git log --pretty=oneline`
- hitorie v custom formátu, 10 commitů: `git log --pretty=format:"%H | %cn | %s"`
- který commit smazal daný soubor? : `git log --full-history -- [file path]`

Example:

git log --full-history  -- myfile

If you want to see only the last commit, which deleted a file use -1 in addition, e.g., git log --full-history -1 -- [file path]

## Git log na konkrétním souboru

- `git log -- <filename>` - co se dělo na daném souboru?
- `git log --all --format=%H ep_ucm_out.il_transformation.transform_ucm_out_oic_sls_txn_eval_ucm.tpt`
- `git diff <mybranch nebo commit> <master nebo commit> -- <filename>` - změny souboru mezi dvěma commity
- `git difftool <mybranch nebo commit> <master nebo commit> -- <filename>` - změny souboru mezi dvěma commity, ale přes winmerge

## High level

| co                             | jak                                        |
| ---                            | ---                                        |
| poslední dva týdny             | `git log --since=2.weeks`                  |
| posledních 10                  | `git log -n 10`                            |
| na jedné řádce                 | `git log --oneline`                        |
| na jedné řádce                 | `git log --pretty=oneline`                 |
| na jedné řádce                 | `git log --pretty=format:"%H | %cn | %s"`  |
| změny funkce `bar`             | `git log -S bar`                           |
| změny funkce `bar`             | `git log -S bar`                           |
| commit log obsahuje `foobar`   | `git log --grep foobar`                    |


## Změny na souborech

| co                             | jak                                                    |
| ---                            | ---                                                    |
| změněné soubory (pouze)        | `git diff --name-only <SHA, tag start> <SHA, tag end>` |
| změněné soubory za 3 commity   | `git diff --name-only HEAD HEAD~3`                     |
| soubor a typ změny             | `git diff --name-status HEAD HEAD~3`                   |

- `A` = Added
- `D` = Deleted
- `M` = Modified
- `R` = Renamed



## Git a workflow

[source](https://git-scm.com/book/en/v2/Distributed-Git-Distributed-Workflows)

## Git - další postupy

### Track all remote changes

```sh
for branch in `git branch -a | grep remotes | grep -v HEAD | grep -v master `; do
   git branch --track ${branch#remotes/origin/} $branch
done
```

## Revert commit on protected branch

Locally on the command line, you can use the `git revert` command to revert changes. 
**This will work on both your protected branch and a downstream branch.** 
`git revert` creates a new commit ahead of the current HEAD so you don't need to force push, and if from a downstream branch, you can manually create a pull request for the reverted changes.

## Revert merge commit on protected branch

The -m option specifies the parent number. This is because a merge commit has more than one parent, and Git does not know automatically which parent was the mainline, and which parent was the branch you want to un-merge.

When you view a merge commit in the output of git log, you will see its parents listed on the line that begins with Merge:

- commit **8f937c6**
- Merge: **8989ee0** **7c6b236**

In this situation, `git revert **8f937c6** -m 1` will get you the tree as it was in 8989ee0, and `git revert -m 2` will reinstate the tree as it was in **7c6b236**.

**POZOR** - následný merge z "offending" branche s sebou zmíněný commit nepřinese, protože již byl zapracován (a revertnut)!

## squash commits

Určitě nedělat pokud už proběhl push!

- `git status` - abych si ověřil, jak moc jsem napřed
- `git rebase --interactive HEAD~[N]` - kde N je počet commitů které chci sloučit, počínaje tím který je nejnovější
- alternativně: `git rebase --interactive <commit-hash>`, kde `commit-hash` je poslední commit, který **NECHCI** sloučit
- zobrazí se editor se seznamem commitů, nejstarší je nahoře; nejstarší commit pravděpodobně chci nechat (`pick`), protože první commit musí zůstat, nejde ho zkolabovat
- ke každému commitu napsat jednu z položek: `s` jako `squash`, nebo `p` jako `pick`, tj v editoru bude něco jako

```
 p 300641e commit1
 s c1065ce commit2
 
 # Rebase f1d3156..c1065ce onto f1d3156 (2 commands)
```

- potom: `git rebase --continue`

# Git::Wrapper

```perl
use Git::Wrapper;
use Encode qw/decode encode/;
use Text::Undiacritic qw(undiacritic);
use Win32;

my $git = Git::Wrapper->new('c:/BI_Domain/bimeta');
my @logs = $git->log({ raw => 1 });

foreach my $log ( @logs ) {        
    # decode: protože git vrací zpátky UTF-8 string, což ale Perl::Wrapper neřeší
    # induacritic: protožer nechci řešit "nábodeníčky" na vstupu
    my $msg = undiacritic(decode('UTF-8', $log->message, Encode::FB_CROAK)); chomp $msg;
    my $aut = undiacritic(decode('UTF-8', $log->author, Encode::FB_CROAK));
    say "$aut: $msg";    

    #say "In commit '" . $log->id . "', the following files changed:";
    #my @mods = $log->modifications;
    #foreach my $mod ( @mods ) {
    #    say "\t" . $mod->filename;
    #}
}
```

# Velikosti commitů

This shell script displays all blob objects in the repository, sorted from smallest to largest.

```bash
git rev-list --objects --all \
| git cat-file --batch-check='%(objecttype) %(objectname) %(objectsize) %(rest)' \
| sed -n 's/^blob //p' \
| sort --numeric-sort --key=2 \
| cut -c 1-12,41- \
| $(command -v gnumfmt || echo numfmt) --field=2 --to=iec-i --suffix=B --padding=7 --round=nearest
```

# ForgetBlob

In a nutshell, this

- uses git filter-branch  to apply git rm  to each single commit
- then, it removes all possible references including remotes, tags and reflog
- next, it deletes unreferenced packs, and
- finally, it forces aggresive garbage collection with git gc –prune .

Things to keep in mind:

- This **rewrites history**, so  forced pushes,  merges, conflicts and such niceties will happen.
- For the same reasons, **tags will be lost and commit hashes will change**.

Remember to keep a checked out copy of the repo before trying this, and use with care.

```sh
#!/usr/bin/env bash
# Completely remove a file from a Git repository history
#
# Copyleft 2017-2019 by Ignacio Nunez Hernanz <nacho _a_t_ ownyourbits _d_o_t_ com>
# GPL licensed (see end of file) * Use at your own risk!
#
# Usage:
#   git-forget-blob file_to_forget
#
# Notes:
#   It rewrites history, therefore will change commit references and delete tags.
function git-forget-blob()
{
  test -d .git || { echo "Need to be at the base of a Git repository." && return 1; }
  git repack -Aq
  ls .git/objects/pack/*.idx &>/dev/null || {
    echo "There is nothing to be forgotten in this repository." && return; 
  }
  echo "Read blobs..."
  local BLOBS=( $( git verify-pack -v .git/objects/pack/*.idx | grep blob | awk '{ print $1 }' ) )
  for ref in "${BLOBS[@]}"; do
    local FILE
    FILE=$( git rev-list --objects --all | grep "$ref" | awk '{ print $2 }' )
    [[ "$FILE" == "$1" ]] && break
    unset FILE
  done
  [[ "$FILE" == "" ]] && { echo "$1 not found in the repository history." && return; }

  echo "Wipe out remotes..."
  git branch -a | grep "remotes\/" | awk '{ print $1 }' | cut -f2 -d/ | while read -r r; do git remote rm "$r" 2>/dev/null; done
  echo "Modify history..."
  git filter-branch --index-filter "git rm --cached --ignore-unmatch $FILE" --force -- --branches --tags
  echo "Wipe out refs..."
  rm -rf .git/refs/original/ .git/refs/remotes/ .git/*_HEAD .git/logs/
  (git for-each-ref --format="%(refname)" refs/original/ || echo :) | \
    xargs --no-run-if-empty -n1 git update-ref -d
  echo "Wipe out reflog..."
  git reflog expire --expire-unreachable=now --all
  git repack -q -A -d
  echo "GC prune..."
  git gc --aggressive --prune=now
}
# License
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this script; if not, write to the
# Free Software Foundation, Inc., 59 Temple Place, Suite 330,
# Boston, MA  02111-1307  USA
git-forget-blob "$@"
```

# Přepis historie, zmenšení repository

[https://rtyley.github.io/bfg-repo-cleaner/](https://rtyley.github.io/bfg-repo-cleaner/)

```
java -jar c:\apps\bfg\bfg-1.13.0.jar --delete-folders "dev" bimain.git
git reflog expire --expire=now --all && git gc --prune=now --aggressive
```
