```python
import pandas as pd
from pandas import DataFrame
```


```python
# sosni si PDC metadata
import cx_Oracle
con = cx_Oracle.connect('BIDEV/censored@(DESCRIPTION=(ADDRESS=(COMMUNITY=tcp.world)(PROTOCOL=TCP)(HOST=hxpdc2dbpkg.ux.to2cz.cz)(PORT=1533))(CONNECT_DATA=(SID=PDC2)))')
cur = con.cursor()
```


```python
# dataframe s těmi joby, které na sabe navazují
q = pd.read_sql_query(con = con, sql = """
    select a.stream_name as export_stream, a.runplan as export_plan, count(*) over (partition by a.stream_name) as stream_check, b.parent_stream_name as ld_stream, c.parent_stream_name as trf_stream
    from pdc.ctrl_stream_plan_ref  a
    inner join ( 
        select stream_name, parent_stream_name 
        from pdc.ctrl_stream_dependency 
        where stream_name like 'EP_OUT%' 
            and not parent_stream_name in ('EP_OUT.DATABASE_STARTING.00', 'INIT_EDW_00') 
        ) b on a.stream_name = b.stream_name
    inner join ( 
        select stream_name, parent_stream_name 
        from pdc.ctrl_stream_dependency 
        where stream_name like 'EP_OUT%' 
            and not parent_stream_name in ('EP_OUT.DATABASE_STARTING.00', 'INIT_EDW_00') 
        ) c on b.parent_stream_name = c.stream_name
    where a.stream_name like 'EP_OUT%TPT_EXPORT%' and runplan <> 'WR001007E'
    order by a.stream_name
""")
q.head()

```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>EXPORT_STREAM</th>
      <th>EXPORT_PLAN</th>
      <th>STREAM_CHECK</th>
      <th>LD_STREAM</th>
      <th>TRF_STREAM</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>EP_OUT.TPT_EXPORT.GITA_CDR_ODCH_VTA.00</td>
      <td>MR001001E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_GITA_CDR_ODCH_VTA.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_GITA_CDR_ODCH_VTA...</td>
    </tr>
    <tr>
      <th>1</th>
      <td>EP_OUT.TPT_EXPORT.OIC_PROVIZE_OUT.00</td>
      <td>MR010010E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_OIC_PROVIZE_OUT.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_OIC_PROVIZE_TDA.00</td>
    </tr>
    <tr>
      <th>2</th>
      <td>EP_OUT.TPT_EXPORT.RES_CRIBIS_OBJECT.00</td>
      <td>WR003003E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_RES_CRIBIS_OBJECT.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_RES_CRIBIS_OBJECT.00</td>
    </tr>
    <tr>
      <th>3</th>
      <td>EP_OUT.TPT_EXPORT.SE_CRB_CRIBIS_INDEX.00</td>
      <td>WR003003E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_SE_CRB_CRIBIS_INDEX.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_SE_CRB_CRIBIS_IND...</td>
    </tr>
    <tr>
      <th>4</th>
      <td>EP_OUT.TPT_EXPORT.SE_CRB_OBJECT.00</td>
      <td>WR003003E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_SE_CRB_OBJECT.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_SE_CRB_OBJECT.00</td>
    </tr>
  </tbody>
</table>
</div>




```python
# dataframe všech kalendářů
calendars = pd.read_sql_query(con=con, sql="select stream_name, runplan from pdc.ctrl_stream_plan_ref")
calendars.head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>STREAM_NAME</th>
      <th>RUNPLAN</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>EP_TGT.IL_HISTORIZATION_ASSET_PROD_250200907.00</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>1</th>
      <td>EP_TGT.IL_TRANSFORMATION_TGT_ASSET_PROD_907_SE...</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>2</th>
      <td>EP_TGT.IL_TRANSFORMATION_TGT_ASSET_PROD_915_AS...</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>3</th>
      <td>EP_TGT.IL_HISTORIZATION_ASSET_PROD_250200915.00</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>4</th>
      <td>EP_TGT.IL_TRANSFORMATION_TGT_ASSET_PROD_915_FA...</td>
      <td>WR001007E</td>
    </tr>
  </tbody>
</table>
</div>




```python
# inner join ... dohledej si stávající runplan
df_inner = pd.merge(left = q, right = calendars, how='inner', left_on='LD_STREAM', right_on='STREAM_NAME', suffixes='XL')
df_inner = pd.merge(left = df_inner, right = calendars, how='inner', left_on='TRF_STREAM', right_on='STREAM_NAME', suffixes='XT')
df_inner.head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>EXPORT_STREAM</th>
      <th>EXPORT_PLAN</th>
      <th>STREAM_CHECK</th>
      <th>LD_STREAM</th>
      <th>TRF_STREAM</th>
      <th>STREAM_NAMEX</th>
      <th>RUNPLANX</th>
      <th>STREAM_NAMET</th>
      <th>RUNPLANT</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>EP_OUT.TPT_EXPORT.GITA_CDR_ODCH_VTA.00</td>
      <td>MR001001E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_GITA_CDR_ODCH_VTA.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_GITA_CDR_ODCH_VTA...</td>
      <td>EP_OUT.LOADED_GITA_CDR_ODCH_VTA.00</td>
      <td>WR001007E</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_GITA_CDR_ODCH_VTA...</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>1</th>
      <td>EP_OUT.TPT_EXPORT.OIC_PROVIZE_OUT.00</td>
      <td>MR010010E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_OIC_PROVIZE_OUT.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_OIC_PROVIZE_TDA.00</td>
      <td>EP_OUT.LOADED_OIC_PROVIZE_OUT.00</td>
      <td>WR001007E</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_OIC_PROVIZE_TDA.00</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>2</th>
      <td>EP_OUT.TPT_EXPORT.RES_CRIBIS_OBJECT.00</td>
      <td>WR003003E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_RES_CRIBIS_OBJECT.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_RES_CRIBIS_OBJECT.00</td>
      <td>EP_OUT.LOADED_RES_CRIBIS_OBJECT.00</td>
      <td>WR001007E</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_RES_CRIBIS_OBJECT.00</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>3</th>
      <td>EP_OUT.TPT_EXPORT.SE_CRB_CRIBIS_INDEX.00</td>
      <td>WR003003E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_SE_CRB_CRIBIS_INDEX.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_SE_CRB_CRIBIS_IND...</td>
      <td>EP_OUT.LOADED_SE_CRB_CRIBIS_INDEX.00</td>
      <td>WR001007E</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_SE_CRB_CRIBIS_IND...</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>4</th>
      <td>EP_OUT.TPT_EXPORT.SE_CRB_OBJECT.00</td>
      <td>WR003003E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_SE_CRB_OBJECT.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_SE_CRB_OBJECT.00</td>
      <td>EP_OUT.LOADED_SE_CRB_OBJECT.00</td>
      <td>WR001007E</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_SE_CRB_OBJECT.00</td>
      <td>WR001007E</td>
    </tr>
  </tbody>
</table>
</div>




```python
# přejmenování sloupců, které jsou zbastardizované po joinu
df_inner = df_inner.rename(columns={'RUNPLANX':'LOADED_RUNPLAN', 'RUNPLANT':'TRF_RUNPLAN'})
# drop sloupců které jsou po joinu a které nechceme
df_inner = df_inner.drop('STREAM_NAMEX',axis=1)
df_inner = df_inner.drop('STREAM_NAMET',axis=1)
df_inner
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>EXPORT_STREAM</th>
      <th>EXPORT_PLAN</th>
      <th>STREAM_CHECK</th>
      <th>LD_STREAM</th>
      <th>TRF_STREAM</th>
      <th>LOADED_RUNPLAN</th>
      <th>TRF_RUNPLAN</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>EP_OUT.TPT_EXPORT.GITA_CDR_ODCH_VTA.00</td>
      <td>MR001001E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_GITA_CDR_ODCH_VTA.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_GITA_CDR_ODCH_VTA...</td>
      <td>WR001007E</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>1</th>
      <td>EP_OUT.TPT_EXPORT.OIC_PROVIZE_OUT.00</td>
      <td>MR010010E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_OIC_PROVIZE_OUT.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_OIC_PROVIZE_TDA.00</td>
      <td>WR001007E</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>2</th>
      <td>EP_OUT.TPT_EXPORT.RES_CRIBIS_OBJECT.00</td>
      <td>WR003003E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_RES_CRIBIS_OBJECT.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_RES_CRIBIS_OBJECT.00</td>
      <td>WR001007E</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>3</th>
      <td>EP_OUT.TPT_EXPORT.SE_CRB_CRIBIS_INDEX.00</td>
      <td>WR003003E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_SE_CRB_CRIBIS_INDEX.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_SE_CRB_CRIBIS_IND...</td>
      <td>WR001007E</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>4</th>
      <td>EP_OUT.TPT_EXPORT.SE_CRB_OBJECT.00</td>
      <td>WR003003E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_SE_CRB_OBJECT.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_SE_CRB_OBJECT.00</td>
      <td>WR001007E</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>5</th>
      <td>EP_OUT.TPT_EXPORT.SE_CRB_SUBJECT.00</td>
      <td>WR003003E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_SE_CRB_SUBJECT.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_SE_CRB_SUBJECT.00</td>
      <td>WR001007E</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>6</th>
      <td>EP_OUT.TPT_EXPORT.SE_CRB_WARNING.00</td>
      <td>WR003003E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_SE_CRB_WARNING.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_SE_CRB_WARNING.00</td>
      <td>WR001007E</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>7</th>
      <td>EP_OUT.TPT_EXPORT.SE_CUST_INFO_D.00</td>
      <td>WR001006E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_SE_CUST_INFO_D.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_SE_CUST_INFO_D.00</td>
      <td>WR001007E</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>8</th>
      <td>EP_OUT.TPT_EXPORT.SE_CUST_INFO_F.00</td>
      <td>WR007007E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_SE_CUST_INFO_F.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_SE_CUST_INFO_F.00</td>
      <td>WR001007E</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>9</th>
      <td>EP_OUT.TPT_EXPORT.ST_ASSET_PARAM.00</td>
      <td>MR012012E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_ST_ASSET_PARAM.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_ST_ASSET_PARAM_CU...</td>
      <td>MR012012E</td>
      <td>MR012012E</td>
    </tr>
    <tr>
      <th>10</th>
      <td>EP_OUT.TPT_EXPORT.ST_ASSET_STAT_CRM.00</td>
      <td>MR012012E</td>
      <td>2</td>
      <td>EP_OUT.LOADED_ST_ASSET_STAT_CRM.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_ST_ASSET_STAT_CRM...</td>
      <td>MR012012E</td>
      <td>MR012012E</td>
    </tr>
    <tr>
      <th>11</th>
      <td>EP_OUT.TPT_EXPORT.ST_ASSET_STAT_CRM.00</td>
      <td>MR012012E</td>
      <td>2</td>
      <td>EP_OUT.LOADED_ST_ASSET_STAT_CRM.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_ST_ASSET_STAT_CRM...</td>
      <td>MR012012E</td>
      <td>MR012012E</td>
    </tr>
    <tr>
      <th>12</th>
      <td>EP_OUT.TPT_EXPORT.ST_ASSET_TYPE.00</td>
      <td>MR012012E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_ST_ASSET_TYPE.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_ST_ASSET_TYPE_400...</td>
      <td>MR012012E</td>
      <td>MR012012E</td>
    </tr>
    <tr>
      <th>13</th>
      <td>EP_OUT.TPT_EXPORT.ST_BILL_STMT.00</td>
      <td>MR012012E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_ST_BILL_STMT.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_ST_BILL_STMT_400_...</td>
      <td>MR012012E</td>
      <td>MR012012E</td>
    </tr>
    <tr>
      <th>14</th>
      <td>EP_OUT.TPT_EXPORT.ST_BILL_STMT_CHRG.00</td>
      <td>MR012012E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_ST_BILL_STMT_CHRG.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_ST_BILL_STMT_CHRG...</td>
      <td>MR012012E</td>
      <td>MR012012E</td>
    </tr>
    <tr>
      <th>15</th>
      <td>EP_OUT.TPT_EXPORT.ST_BSCS_PC_SOC_INV_GROUP.00</td>
      <td>MR012012E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_ST_BSCS_PC_SOC_INV_GROUP.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_ST_BSCS_PC_SOC_INV_GR...</td>
      <td>WR001007E</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>16</th>
      <td>EP_OUT.TPT_EXPORT.ST_BSCS_PC_SOC_ZONE_MAP.00</td>
      <td>MR012012E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_ST_BSCS_PC_SOC_ZONE_MAP.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_ST_BSCS_PC_SOC_ZONE_M...</td>
      <td>WR001007E</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>17</th>
      <td>EP_OUT.TPT_EXPORT.ST_CETIN_RADO_INVOICE.00</td>
      <td>MR015015E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_ST_CETIN_RADO_INVOICE.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_ST_CETIN_RADO_INVOICE...</td>
      <td>MR012012E</td>
      <td>MR012012E</td>
    </tr>
    <tr>
      <th>18</th>
      <td>EP_OUT.TPT_EXPORT.ST_CHRG_CLAS.00</td>
      <td>MR012012E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_ST_CHRG_CLAS.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_ST_CHRG_CLAS_400_...</td>
      <td>MR012012E</td>
      <td>MR012012E</td>
    </tr>
    <tr>
      <th>19</th>
      <td>EP_OUT.TPT_EXPORT.ST_GM_DATA_CAPACITY.00</td>
      <td>MR012012E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_ST_GM_DATA_CAPACITY.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_ST_GM_DATA_CAPACITY_M...</td>
      <td>MR012012E</td>
      <td>MR012012E</td>
    </tr>
    <tr>
      <th>20</th>
      <td>EP_OUT.TPT_EXPORT.ST_NA_DRCTN.00</td>
      <td>MR012012E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_ST_NA_DRCTN.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_ST_NA_DRCTN_400_T...</td>
      <td>MR012012E</td>
      <td>MR012012E</td>
    </tr>
    <tr>
      <th>21</th>
      <td>EP_OUT.TPT_EXPORT.ST_NA_INB_AGG.00</td>
      <td>MR012012E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_ST_NA_INB_AGG.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_ST_NA_INB_AGG_400...</td>
      <td>WR001007E</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>22</th>
      <td>EP_OUT.TPT_EXPORT.ST_ORGN_TYPE.00</td>
      <td>MR012012E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_ST_ORGN_TYPE.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_ST_ORGN_TYPE_400_...</td>
      <td>MR012012E</td>
      <td>MR012012E</td>
    </tr>
    <tr>
      <th>23</th>
      <td>EP_OUT.TPT_EXPORT.ST_PARTY_CUST.00</td>
      <td>MR012012E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_ST_PARTY_CUST.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_ST_PARTY_CUST_400...</td>
      <td>MR012012E</td>
      <td>MR012012E</td>
    </tr>
    <tr>
      <th>24</th>
      <td>EP_OUT.TPT_EXPORT.ST_PROD.00</td>
      <td>MR012012E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_ST_PROD.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_ST_PROD_400_TGT.00</td>
      <td>MR012012E</td>
      <td>MR012012E</td>
    </tr>
    <tr>
      <th>25</th>
      <td>EP_OUT.TPT_EXPORT.ST_PROD_CAT_HIER.00</td>
      <td>MR012012E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_ST_PROD_CAT_HIER.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_ST_PROD_CAT_HIER_...</td>
      <td>MR012012E</td>
      <td>MR012012E</td>
    </tr>
    <tr>
      <th>26</th>
      <td>EP_OUT.TPT_EXPORT.ST_RTD_USG.00</td>
      <td>MR012012E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_ST_RTD_USG.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_ST_RTD_USG_400_TG...</td>
      <td>WR001007E</td>
      <td>WR001007E</td>
    </tr>
    <tr>
      <th>27</th>
      <td>EP_OUT.TPT_EXPORT.ST_SEG.00</td>
      <td>MR012012E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_ST_SEG.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_ST_SEG_400_TGT.00</td>
      <td>MR012012E</td>
      <td>MR012012E</td>
    </tr>
    <tr>
      <th>28</th>
      <td>EP_OUT.TPT_EXPORT.ST_STAT_TYPE.00</td>
      <td>MR012012E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_ST_STAT_TYPE.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_ST_STAT_TYPE_400_...</td>
      <td>MR012012E</td>
      <td>MR012012E</td>
    </tr>
    <tr>
      <th>29</th>
      <td>EP_OUT.TPT_EXPORT.ST_ZIS_AP_ACCESS.00</td>
      <td>MR012012E</td>
      <td>1</td>
      <td>EP_OUT.LOADED_ST_ZIS_AP_ACCESS.00</td>
      <td>EP_OUT.IL_TRANSFORMATION_ST_ZIS_AP_ACCESS_ZIS_...</td>
      <td>MR012012E</td>
      <td>MR012012E</td>
    </tr>
  </tbody>
</table>
</div>




```python
# záznamy, kde exportní stream má plán odlišný od loaded a od trf streamu
fixLoaded = df_inner[ df_inner['EXPORT_PLAN'] != df_inner['LOADED_RUNPLAN'] ][['EXPORT_PLAN','LD_STREAM']]
fixLoaded.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>EXPORT_PLAN</th>
      <th>LD_STREAM</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>MR001001E</td>
      <td>EP_OUT.LOADED_GITA_CDR_ODCH_VTA.00</td>
    </tr>
    <tr>
      <th>1</th>
      <td>MR010010E</td>
      <td>EP_OUT.LOADED_OIC_PROVIZE_OUT.00</td>
    </tr>
    <tr>
      <th>2</th>
      <td>WR003003E</td>
      <td>EP_OUT.LOADED_RES_CRIBIS_OBJECT.00</td>
    </tr>
    <tr>
      <th>3</th>
      <td>WR003003E</td>
      <td>EP_OUT.LOADED_SE_CRB_CRIBIS_INDEX.00</td>
    </tr>
    <tr>
      <th>4</th>
      <td>WR003003E</td>
      <td>EP_OUT.LOADED_SE_CRB_OBJECT.00</td>
    </tr>
  </tbody>
</table>
</div>




```python
fixTrf = df_inner[ df_inner['EXPORT_PLAN'] != df_inner['TRF_RUNPLAN'] ][['EXPORT_PLAN','TRF_STREAM']]
fixTrf.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>EXPORT_PLAN</th>
      <th>TRF_STREAM</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>MR001001E</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_GITA_CDR_ODCH_VTA...</td>
    </tr>
    <tr>
      <th>1</th>
      <td>MR010010E</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_OIC_PROVIZE_TDA.00</td>
    </tr>
    <tr>
      <th>2</th>
      <td>WR003003E</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_RES_CRIBIS_OBJECT.00</td>
    </tr>
    <tr>
      <th>3</th>
      <td>WR003003E</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_SE_CRB_CRIBIS_IND...</td>
    </tr>
    <tr>
      <th>4</th>
      <td>WR003003E</td>
      <td>EP_OUT.IL_TRANSFORMATION_OUT_SE_CRB_OBJECT.00</td>
    </tr>
  </tbody>
</table>
</div>




```python
# odjeď SQL
for i,r in fixTrf.iterrows():
    print ("update ctrl_stream_plan_ref set runplan='%s' where stream_name ='%s';" % ( r['EXPORT_PLAN'], r['TRF_STREAM'] ))
for i,r in fixLoaded.iterrows():
    print ("update ctrl_stream_plan_ref set runplan='%s' where stream_name ='%s';" % ( r['EXPORT_PLAN'], r['LD_STREAM'] ))
```

    update ctrl_stream_plan_ref set runplan='MR001001E' where stream_name ='EP_OUT.IL_TRANSFORMATION_OUT_GITA_CDR_ODCH_VTA_TGT_400_TGT.00';
    update ctrl_stream_plan_ref set runplan='MR010010E' where stream_name ='EP_OUT.IL_TRANSFORMATION_OUT_OIC_PROVIZE_TDA.00';
    update ctrl_stream_plan_ref set runplan='WR003003E' where stream_name ='EP_OUT.IL_TRANSFORMATION_OUT_RES_CRIBIS_OBJECT.00';
    update ctrl_stream_plan_ref set runplan='WR003003E' where stream_name ='EP_OUT.IL_TRANSFORMATION_OUT_SE_CRB_CRIBIS_INDEX.00';
    update ctrl_stream_plan_ref set runplan='WR003003E' where stream_name ='EP_OUT.IL_TRANSFORMATION_OUT_SE_CRB_OBJECT.00';
    update ctrl_stream_plan_ref set runplan='WR003003E' where stream_name ='EP_OUT.IL_TRANSFORMATION_OUT_SE_CRB_SUBJECT.00';
    update ctrl_stream_plan_ref set runplan='WR003003E' where stream_name ='EP_OUT.IL_TRANSFORMATION_OUT_SE_CRB_WARNING.00';
    update ctrl_stream_plan_ref set runplan='WR001006E' where stream_name ='EP_OUT.IL_TRANSFORMATION_OUT_SE_CUST_INFO_D.00';
    update ctrl_stream_plan_ref set runplan='WR007007E' where stream_name ='EP_OUT.IL_TRANSFORMATION_OUT_SE_CUST_INFO_F.00';
    update ctrl_stream_plan_ref set runplan='MR012012E' where stream_name ='EP_OUT.IL_TRANSFORMATION_ST_BSCS_PC_SOC_INV_GROUP_BSCS_PC_SOC_INV_GROUP.00';
    update ctrl_stream_plan_ref set runplan='MR012012E' where stream_name ='EP_OUT.IL_TRANSFORMATION_ST_BSCS_PC_SOC_ZONE_MAP_BSCS_PC_SOC_ZONE_MAP.00';
    update ctrl_stream_plan_ref set runplan='MR015015E' where stream_name ='EP_OUT.IL_TRANSFORMATION_ST_CETIN_RADO_INVOICE_AP_CHRG.00';
    update ctrl_stream_plan_ref set runplan='MR012012E' where stream_name ='EP_OUT.IL_TRANSFORMATION_OUT_ST_NA_INB_AGG_400_TGT.00';
    update ctrl_stream_plan_ref set runplan='MR012012E' where stream_name ='EP_OUT.IL_TRANSFORMATION_OUT_ST_RTD_USG_400_TGT.00';
    update ctrl_stream_plan_ref set runplan='MR001001E' where stream_name ='EP_OUT.LOADED_GITA_CDR_ODCH_VTA.00';
    update ctrl_stream_plan_ref set runplan='MR010010E' where stream_name ='EP_OUT.LOADED_OIC_PROVIZE_OUT.00';
    update ctrl_stream_plan_ref set runplan='WR003003E' where stream_name ='EP_OUT.LOADED_RES_CRIBIS_OBJECT.00';
    update ctrl_stream_plan_ref set runplan='WR003003E' where stream_name ='EP_OUT.LOADED_SE_CRB_CRIBIS_INDEX.00';
    update ctrl_stream_plan_ref set runplan='WR003003E' where stream_name ='EP_OUT.LOADED_SE_CRB_OBJECT.00';
    update ctrl_stream_plan_ref set runplan='WR003003E' where stream_name ='EP_OUT.LOADED_SE_CRB_SUBJECT.00';
    update ctrl_stream_plan_ref set runplan='WR003003E' where stream_name ='EP_OUT.LOADED_SE_CRB_WARNING.00';
    update ctrl_stream_plan_ref set runplan='WR001006E' where stream_name ='EP_OUT.LOADED_SE_CUST_INFO_D.00';
    update ctrl_stream_plan_ref set runplan='WR007007E' where stream_name ='EP_OUT.LOADED_SE_CUST_INFO_F.00';
    update ctrl_stream_plan_ref set runplan='MR012012E' where stream_name ='EP_OUT.LOADED_ST_BSCS_PC_SOC_INV_GROUP.00';
    update ctrl_stream_plan_ref set runplan='MR012012E' where stream_name ='EP_OUT.LOADED_ST_BSCS_PC_SOC_ZONE_MAP.00';
    update ctrl_stream_plan_ref set runplan='MR015015E' where stream_name ='EP_OUT.LOADED_ST_CETIN_RADO_INVOICE.00';
    update ctrl_stream_plan_ref set runplan='MR012012E' where stream_name ='EP_OUT.LOADED_ST_NA_INB_AGG.00';
    update ctrl_stream_plan_ref set runplan='MR012012E' where stream_name ='EP_OUT.LOADED_ST_RTD_USG.00';
    


```python

```
