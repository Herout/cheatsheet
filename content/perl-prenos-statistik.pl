#! /usr/bin/perl
#---------------------------------------------------------------------------------------------------------
# preamble
#---------------------------------------------------------------------------------------------------------
use v5.24;                        # implicit strict
use utf8;                         # source is utf-8 encoded
use warnings;                     # simple sanity
use warnings qw( FATAL utf8 );    # fatalize all unicode warnings
use open qw/:std :utf8/;          # UTF-8 is default on all handles (+STDIN/STDOUT/STDERR)
use Encode qw/encode decode/;
#---------------------------------------------------------------------------------------------------------
# modules
#---------------------------------------------------------------------------------------------------------
use Carp;    # carp=warn, croak=die, confess=die
use Path::Tiny;
use File::Find::Rule;

#use Moo;
#use CLI::Osprey;

#---------------------------------------------------------------------------------------------------------
# main
#---------------------------------------------------------------------------------------------------------
my $tgt  = path('c:\BI_Domain\bimain\pkg\main\sprint_20_03_04\BIDEV-751-L2I-stats\DB\Teradata\20_stats');
my $src = path('c:\BI_Domain\bimain\pkg\main\sprint_20_03_03-02-WDAY\BIDEV_00529_L2i_FMS_O2_Pojisteni_HW\DB\teradata\20_new_object');
my $ddls = path('c:\BI_Domain\ProductionMaster\db\Teradata\DDL');
my @db = ('EP_IND','EP_IND_WRK');    

my @files = map { path($_)->absolute } File::Find::Rule->file()->name(qr/[.]sql$/i)->in(
    map { $src->child($_)->stringify } @db
);

foreach my $f (@files) {
    my $t = $f->basename(qr/[.]sql$/i);
    my $db = $f->parent->basename;
    next if $db eq 'EP_IND_WRK' and $t =~ /_W$/i;    
    my $ddl = $ddls->child($db, 'Tables', "$t.sql");
    if (! $ddl->is_file) {
        say $ddl;
        next;
    }
    
    # načti DDL skript
    my $sql = decode ('UCS-2LE', $ddl->slurp_raw, Encode::FB_CROAK);
    my ($stats, $seen);
    foreach my $l (split /\R/, $sql) {
        $seen = 1 if $l =~ /collect /i;
        next unless $seen;
        next if $l =~ /^\s*comment on/i;
        $stats .= "$l\n";
    }
    if ($stats) {
        my $ts = $tgt->child($db, "$t.sql");
        $stats = ".SET ERRORLEVEL 3810 SEVERITY 0;\n.SET ERRORLEVEL 3706 SEVERITY 0;\n$stats\nSELECT 1;\n.SET ERRORLEVEL 3810 SEVERITY 8;\n.SET ERRORLEVEL 3706 SEVERITY 8;\n";
        $ts->parent->mkpath;
        $ts->spew($stats);
    }
}
