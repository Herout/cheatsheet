---
title: 'make current commit the initial commit'
tags:  [ git, history, commit, initial ]
---

# How to make current commit the initial commit

## Alternative 1

**HINT**: 

- tohle nezrušilo historii, stále je vidět v `git log --all`, a to i když jsem znovu dané repo odklonoval. 
- je nutný čistý clone, pull nefunguje
- nejde o neprůstřelný postup

The only solution that works for me (and keeps submodules working) is

    git checkout --orphan newBranch
    git add -A                              # Add all files and commit them
    git commit
    git branch -D master                    # Deletes the master branch
    git branch -m master                    # Rename the current branch to master
    git push -f origin master               # Force push master branch to github
    git gc --aggressive --prune=all         # remove the old files

Deleting .git/ always causes huge issues when I have submodules. Using git rebase --root would somehow cause conflicts for me (and take long since I had a lot of history).

## Alternative 2

**HINT:** postup opakovat pro jednotlivé branche (develop, master)

- kompletně zruší historii pro danou branch
    -- drop branches in origin
    git push --remove master
        
    -- Remove the history from 
    rm -rf .git
    
    -- recreate the repos from the current content only
    git init
    git add .
    git commit -m "Initial commit"
    
    
    
    -- push to the github remote repos ensuring you overwrite history
    git remote add origin git@github.com:<YOUR ACCOUNT>/<YOUR REPOS>.git
    git push -u --force origin master