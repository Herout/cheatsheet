# Airflow

## Airflow - how it works

### Single Node

![airflow-architecture](./airflow-resources/airflow-architecture.png)

Best practice:

- nody:
  - scheduler + web server
  - workers
  - metadata db
  - fronta

![airflow-multiple-nodes](./airflow-resources/airflow-multiple-nodes.png)

### Instalace

Vřele doporučuje jít přes virtualEnv se zamraženým seznamem závislostí, protože čas od času dojde k nekompatibilní změně

```
source .sandbox/bin/activate
```

Závislosti si googlíme - apache airflow requirements; nebo [github](https://github.com/apache/airflow/tree/master/requirements)

```
wget https://github.com/apache/airflow/blob/master/requirements/requirements-python3.7.txt
```

Následně instalace přes pip:

```
pip install "apache-airflow[mysql, crypto, rabbit-mq, postgress, celery, redis]"==1.10.10 --constraint requirements-python3.7.txt
```

Co to dělá:

- instalace base balíčku, a jeho _podbalíčků_ v "hranatých závorkách"
- zamrazí to verzi na `==1.10.10`
- zamrazí to verze requirements na to, co je uvedeno na gitlabu

```
airflow initdb
```

Co to dělá: inicializuje databázi,

- `airflow/airflow.cfg` - základní konfigurace
- `airflow/airflow.db` - embedded databáze pro testy; sqlite

```
# zjiti si cestu k adresáři pro dags
grep dags_folder airflow.cfg
# ve správném umístění!
mkdir dags
```

Check cfg

```
vim airflow.cfg
# najdi si load_examples, a nastav tam False

airflow resetdb
# protože chceš vyhodit samples z DB, bacha, přijdeš o všechno!
```

### Start

```
airflow scheduler
airflow webserever
```

## UI & CLI

- schedule: DAG seběhne poté, co uběhne definovaný interval (schedule interval) od posledního běhu; pokud není poslední běh, tak od `start_date` parametru na úrovni DAG.
- execution date: vždy je v UTC, a vžy je zaokrouhlené DOLŮ na schedule interval

| CLI                                      | what?                                      |
| ---------------------------------------- | ------------------------------------------ |
| `airflow initdb`                         | inicializace DB                            |
| `airflow resetdb`                        | destrukce předchozí verze metadat          |
| `airflow webserver`                      | nahodí UI                                  |
| `airflow scheduler`                      | nahodí scheduler                           |
| `airflow worker`                         | nahodí worker                              |
| `airflow list_dags`                      | jako LS - všechny DAGs o kterých se ví     |
| `airflow list_tasks <dag>`               | všechny tasky pod DAG o kterých se ví      |
| `airflow list_tasks <dag> --tree`        | ... a navíc jejich dependence              |
| `airflow test <dag> <task> <yyyy-mm-dd>` | sjede task bez zápisu metadat za daný date |


### DAG skeleton

Minimální DAG skeleton

```
# nutnost
from airflow import dag

# with používáme protože zajišťuje uvolnění zdrojů, pokud něco padne
# povinné parametry:
#   dag_id - musí být unikátní přes celý systém
#   schedule_interval - buď jako Cron Expression, nebo jako time_delta object; 
#       doporučuje se cron expression, protože není náchylná na problémy v timezóně, a je srozumitelnější
#       existují "predefined cron expressions", jako @daily; je to airflow specifikum,
#       https://airflow.apache.org/docs/stable/scheduler.html#dag-runs

with DAG(dag_id="dag_skeleton", schedule_interval="@daily") as dag:
    None
```

## Airflow Operátory

Požadavky

- operátor definuje jeden task
- operátor je nezávislý na ostatních (kromě dependencí)
- operátor je restartovatelný, a produkuje stejný výstup bez ohledu na to, kolikrát běží

Virtuální konstrukty

- BaseOperator - z ní se odvozují všechny ostatní třídy
- BaseSensorOperator - základ pro sniffery

Základní operátory

- BashOperator
- PythonOperator
- EmailOperator
- MySqlOperator, SqliteOperator, PostgreOperator, ...

Typy operátorů

- Action
- Transfer - přesun dat mezi systémy; **nepoužívat** pro velká data, protože se kompletní datový set načítá do paměti!
- Sensor - sniffer


### File Sensor

```
from airflow import dag
with DAG(dag_id="dag_skeleton", schedule_interval="@daily") as dag:
    None
```