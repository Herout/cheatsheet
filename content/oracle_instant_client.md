---
title:  'Oracle instant client'
tags:   [oracle, client, instaclient, DBI]
---
# Oracle instant client

[download at](https://www.oracle.com/technetwork/database/database-technologies/instant-client/downloads/index.html)

Potřebujete:
* Base package
* SQL*Plus Package
* SDK Package
* ODBC Package

Postup instalace
* rozbalit do jednoho adresáře, v dalším textu je označen jako `<root>`
* spoustit v něm `odbc_install.exe`
* vytvořit  `<root>/network/admin/tnsnames.ora`
* zadefinovat proměnné prostředí
    * `ORACLE_HOME` se odkazuje `<root>`
    * `PATH` se odkazuje na `<root>`
    * `TNSADMIN` se odkazuje na `<root>/network/admin`
    
# DBI

Instalace

```
cpanm DBI
cpanm DBD::Oracle
```