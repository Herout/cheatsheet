# Pár věcí k akciovému trhu

## Broker

Účet mám otevřený tady: [https://www.degiro.cz/](https://www.degiro.cz/). Veškerá komunikace je online,
všechno se dá vyřešit na dálku.  Tady si o nich můžeš něco přečíst:

- [finex](https://finex.cz/recenze/degiro/?gclid=Cj0KCQjwmdzzBRC7ARIsANdqRRnK_NdL1TXKqLUY857bVckZVu9aiWJDhfap0vudAbLZk09BEd3vAPsaAgK2EALw_wcB)
- [pfolio](https://pfolio.cz/degiro-recenze/)

Zatím jim nemám co vytknout. Například ve státech je poměrně oblíbený RobinHood, a v době kdy vrcholila
na burze panika se nim položila infrastruktura, a spousta lidí která chtěla prodat (nebo koupit) tuhle
možnost prostě neměla. Tady jsem nic podobného nezaregistroval.

**Dividendy** - jsou ti vypláceny na účet u toho brokera, a máš možnost si je stáhnout na účet v bance.
Potvrzuju že dividendy vyplácí, mám malou investici u Whirlpoolu, a pár dolarů jsem od nich už dostal.

Tak a teď pár tipů. Předem musím říct, že v tomhle okamžiku jsem v zisku jenom na Slacku (ostatní pozice co mám jsou v tenhle okamžik ve ztrátě - čekal jsem propad, ale nečekal jsem tak velkou paniku). Hodlám postupně přikupovat.

## Tipy na růstové firmy

Tohle jsou pohopitelně investice, které jsou ze svojí povahy rizikovější. Jde o firmy, které zatím nevyplácí dividendu, některé z nich nejsou zatím v zisku, ale je u nich předpoklad že se do zisku v horizontu 2-3 let dostanou. Pochopitelně, jeden nikdy neví... Tyhle tipy pochází od lidí, kterým se podařilo například předpovědět co vyroste z Netflixu, takže určitá zkušenost za ni mi je.

### TradeDesk

Ticker `TTD`. [https://www.thetradedesk.com/](https://www.thetradedesk.com) 

Firma se zabývá "chytrou reklamou" na kanálech jako je internetová televize, mobilní aplikace, apod.
Tržní kapitalizace 8 miliard. Za rok 2019 tržby 1 miliarda, 35% růst proti roku 2018. Zisk 108 milionů dolarů. Osobně si myslím, že tohle je investice s dobrým potenciálem na návratnost pod dva roky.

- PE: 74
- já jsem nastoupil nadvakrát, jedna transakce na 170$, druhá na 140$

### Zoom Video Communications, Inc. 

Ticker `ZM`. Nabízí telekonferenční software, a je to jedna z firem která na současné situaci vydělává. 

[Yahoo Finance](https://finance.yahoo.com/quote/ZM/financials?p=ZM)

### Slack

Ticker `WORK`. Firma se zabývá vývojem online kooperativní platformy ("kecátko"). Software sám používám,
a podle mě má dobrou budoucnost. Mezi zákazníky patří například Uber nebo IBM.

Tržní kapitalizace 12 miliard. Momentálně jsou ztrátoví. Tohle je investice tak na 5 let.

[Yahoo Finance](https://finance.yahoo.com/quote/WORK/financials?p=WORK)

### Luckin Coffee

**UPDATE** - za dva kvartály roku 2019 se dopustili podvodu v reportingu čísel o prodejích. Cena akcie se okamžitě a drasticky propadla. Jsou "out".

Ticker `LK`. Jde o firmu která podniká na čínském trhu, založena 2017. Konkurují řetězcům jako je Starbucks. Provozují kavárny formou kiosků v kancelářských budovách, a hodně sází na provázání s online nákupy (a zdá se že to funguje). Od založení neustálý růst tržeb, poslední kvartál 2019 měli tržby 219 milionů, kvartál předtím měli tržby 133 milionů.

Tržní kapitalizace 6 miliard, v tomhle okamžiku jsou ve ztrátě. 

[Yahoo Finance](https://finance.yahoo.com/quote/LK/financials?p=LK)

### JD.com

Ticker `JD`. Retailer v Číně. V době, kdy tam propukl SARS, přešli na obchodní model který má Amazon. V době kdy byl uzavřen Wuhan fungovali dál, dodávky zboží řešili pomocí autonomních vozidel.

Tržní kapitalizace 55 miliard. Jsou v zisku, PE je 30.5.

[Yahoo Finance](https://finance.yahoo.com/quote/JD/financials?p=JD)

## Tipy na dividendové firmy

Tohle jsou pochopitelně ze svojí povahy mnohem bezpečnější investice. 

### 3M 

Ticker `MMM` - dělají "skoro všechno". Zdravotnictví, průmyslová výroba, ochranné prostředky, kancelářské potřeby. Je to "dividendový aristokrat", momentálně P/E 15, pravděpodobně půjdou ještě dolů.

Momentálně (k dnešnímu dni) je jim odhadován dividendový výnos 4,28%, pochopitelně nikdo neví, jestli nějaká dividenda bude... Ve státech v tomhle okamžiku propuká šílenstcí.

[Yahoo Finance](https://finance.yahoo.com/quote/MMM/financials?p=MMM)

### Leggett & PLatt

Ticker `LEG`. Je to primárně výrobce nábytku. Snaží se být soběstační, tj komponenty které potřebuj si vyrábět sami - chytrá pěna, pružiny, ocelové trubky, apod. Část téhle jejich produkce se přeprodává dál. Momentálně akcie dostala "výprask", zřejmě proto že poměrně významná část jejich produkce je umístěná v Číně, částečně možná protože to co prodávají bych já vnímal jako "luxusní zboží".

Dividendový aristokrat, procházel jsem si výroční zprávy, a jejich management má provize přímo navázané na dividendu a na cenu akcie - tj hodnota pro investory je veřejně komunikovaná jako součást jejich strategie (na to jsem u jiných firem nenarazil).

Momentálně se jin odhaduje dividendový výnos asi 6%.

PE = 9.65

[Yahoo Finance](https://finance.yahoo.com/quote/LEG/financials?p=LEG)

### Delta Air Lines

Ticker `DAL`. Pokud vím, nakupoval je teď i Warren Buffet za cenu někde kolem 36 za akcii. Tahle firma pochopitelně dostane krátkodobě neskutečný výprask, a asi se dá čekat že jejich akcie půjdou ještě dolů. 

[Yahoo Finance](https://finance.yahoo.com/quote/DAL/financials?p=DAL)


### Intell

Ticker `INTC`. Asi není třeba popoisovat co dělají. Momentálně se obchoduje na úrovni týdenního průměru za 200 týdnů.

[Yahoo Finance](https://finance.yahoo.com/quote/LEG/financials?p=INTC)

### Exxon mobile

Ticker `XOM`. Momentálně je za absurdně nízkou cenu ("ropná válka"). Tady si nejsem jistý, jak to vnímám, nevím jestli bych do nich šel.

[Yahoo Finance](https://finance.yahoo.com/quote/XOM/financials?p=XOM)

### Nvidia

Grafické čipy, handheldy, počítače. Ticker `NVDA`. Podívej se na vývoj ceny, už tam malou pozici mám, pokud by spadla na úroveň kolem 170, přikoupil bych.

[Yahoo Finance](https://finance.yahoo.com/quote/NVDA/financials?p=NVDA)