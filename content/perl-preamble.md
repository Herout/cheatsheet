---
title: 'Perl, preamble'
tags: [perl]
---

```perl
#! /usr/bin/perl                                                    # shebang is best practise
#---------------------------------------------------------------------------------------------------------
# preamble
#---------------------------------------------------------------------------------------------------------
use v5.24;                                                          # implicit strict
use utf8;                                                           # source is utf-8 encoded
use warnings;                                                       # simple sanity
use warnings qw( FATAL utf8 );                                      # fatalize all unicode warnings
use open qw/:std :utf8/;                                            # UTF-8 is default on all handles (+STDIN/STDOUT/STDERR)

#---------------------------------------------------------------------------------------------------------
# modules
#---------------------------------------------------------------------------------------------------------
use Carp;                                                           # carp=warn, croak=die, confess=die
use Path::Tiny;
use File::Find::Rule; 
#use Moo;
#use CLI::Osprey;

#---------------------------------------------------------------------------------------------------------
# main
#---------------------------------------------------------------------------------------------------------
my $root = path($0)->parent;
my @files = map { path($_)->absolute } File::Find::Rule->file()->name( qr/[.]sql$/i )->in( "$root" );
foreach my $f (@files) {
    my $b = $f->basename(qr/[.]sql$/i);
}
```