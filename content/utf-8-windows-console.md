---
title:  'Změna kódování konzole na Windows'
tags:   [perl, data, structures]
---

# Jak změnit znakovou sadu v CMD na windows

To change the codepage for the console only, do the following:

    Start -> Run -> regedit
    Go to [HKEY_LOCAL_MACHINE\Software\Microsoft\Command Processor\Autorun]
    Change the value to chcp 65001

