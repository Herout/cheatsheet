---
title: 'jak na mds_man_lokup'
tags:   [ mds ]
---

# mds_man_lookup

Konkrétní záznam:

```sql
call UEP_TGT_LOAD.proc_LOOKUP_TO_TGT('EP_TGT','PARTY_SEG_ROLE', 11, err_cd);                                            
```

Init:

```sql
 SELECT 'call UEP_TGT_LOAD.proc_LOOKUP_TO_TGT(''' || TRIM(db_cd) ||''','''||TRIM(entity_cd) ||''', ' || CAST(edw_sk AS VARCHAR(10)) || ', err_cd);' (TITLE '')
 FROM 
    (
    SELECT  edw_sk
    ,db_cd
    ,entity_cd
    FROM ep_stg.mds_man_lookup 
    WHERE key_domain_cd = '-99' AND entity_cd<>'SEG' and entity_cd = 'PARTY_SEG_ROLE'
                                                        /*--------------------------*/
    ) x 
 ORDER BY 1
 ;
```