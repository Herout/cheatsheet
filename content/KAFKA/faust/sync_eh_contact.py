import faust
import sync_session
import teradata
import slack
from dataclasses import dataclass

SLEEPTIME = 60 # number of seconds we sleep before next attempt to write the record to Teradata
TABLE = "ed_tgt.eh_contact_record"

#create multiset table ed_tgt.eh_contact_record (
#    customer_id integer not null,
#    customer_name varchar(30) character set unicode
#) primary index (customer_id);

class EhContactRecord(faust.Record, serializer="json"):
    customer_id: int
    customer_name: str

@dataclass
class EhContactRecordTeraMerger:
    session : sync_session.TeradataSession
    slack : slack.WebClient
    

    def slack_message(self,message):        
        if self.slack is None:
            return
        try:
            slack.chat_postMessage(channel="#operations", text=message)
        except slack.SlackApiError as e:
            pass        

    def to_teradata(self, record: EhContactRecord):        
        # teradata udaexec.session
        # this will loop indefinitely until connection suceeds, if we already were connected
        s = self.session.session
        
        # let us try to insert the record into database
        while True:
            print(record)
            try:
                # session.execute("SELECT * FROM ${table}", queryTimeout=60, continueOnError=True) # timeout in sec; do not raise
                s.execute(f"MERGE INTO {TABLE} AS _TGT" + """
                USING (
                    SELECT 
                        ? as customer_id,
                        ? as customer_name
                ) as _SRC
                ON _TGT.customer_id = _SRC.customer_id
                WHEN MATCHED THEN UPDATE
                    SET customer_name = _SRC.customer_name
                WHEN NOT MATCHED THEN INSERT (
                    customer_id,
                    customer_name
                ) VALUES (
                    _SRC.customer_id,
                    _SRC.customer_name
                )
                ;""", (
                    record.customer_id, 
                    record.customer_name
                    ))
                break
            
            # TODO: parsing typu chyby, reconnect na ztracené konexi
            except teradata.DatabaseError as e:
                self.slack_message("kafkaSync: " + str(e))
                raise(e)