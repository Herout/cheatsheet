import faust
import slack
from sync_session import TeradataSession
from sync_eh_contact import EhContactRecord, EhContactRecordTeraMerger

# konfigurace Kafky
conf_consumer_group ="edw_group"
conf_broker = "kafka://127.0.0.1:9092"
conf_topic  = "test"

# teradata
session = TeradataSession()    

# kafka init
app = faust.App(conf_consumer_group,broker=conf_broker)
topic = app.topic(conf_topic, value_type=EhContactRecord)

# teradata init and connect
tera =  TeradataSession()
session = tera.session

# slack init
slack = slack.WebClient(token="xoxb-1187044052707-1199502372001-RDAhQN47kWQPirxv5umXzdb3")

# serializer init
# TODO: slack integrace, a odložení "lost message" někam do souboru
#       tak aby bylo možné je znovu přehrát
print("consumer: create EhContactRecordTeraMerger instance")
serializer = EhContactRecordTeraMerger(session=tera,slack=slack)
print("consumer: ready")


# agent, který synchronizuje messages do Teradaty
@app.agent(topic)
async def consume(stream):
    async for event in stream:
        serializer.to_teradata(event)

