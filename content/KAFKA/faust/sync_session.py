import teradata
import time
from dataclasses import dataclass

# defaults
SLEEPTIME = 60 # number of seconds we sleep before next attempt to connect to Teradata


@dataclass
class TeradataSession:
    tdpid : str = 'edwtest.cz.o2'
    username : str = 'UED_TGT_LOAD'
    password : str = 'UED_TGT_LOAD'
    authentication : str = "LDAP"
    driver : str = "Teradata Database ODBC Driver 16.20"
    _first_con_attempt:  bool = True

    @property
    def session(self):
        # init teradata driver        
        u = getattr(self, "_udaexec", None)
        if u is None:
            print("TeradataSession: set udaexec instance")
            u = teradata.UdaExec()
            self._udaexec = u
        
        # get session; if we were already connected, keep trying until we suceed, else raise
        # the number of retries is unlimited at the moment, we sleep for 60 seconds before next try        
        while True:
            # first, see if we already have a session
            # if not, try to get it (and ping it)
            try:                
                sess = getattr(self, "_session", None)
                if sess is None:
                    self._session = u.connect("${dataSourceName}")                                
                    self._session.execute("select 1;")                            
            # if either connection or ping failed, teradata.DatabaseError is raised
            # in that case, if this is the first run, raise the error (configuration is incorrect)
            # otherwise, re-try after SLEEPTIME seconds
            except teradata.DatabaseError as e:
                self._session = None
                if self._first_con_attempt:
                    raise e
                print("TeradataSession: error, will retry in", SLEEPTIME, "seconds:", e)                
                self._session = None                            
            if self._session is not None:
                break            
            time.sleep(SLEEPTIME)        
        self._first_con_attempt = False
        return self._session        

    