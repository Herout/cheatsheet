---
title:  'go fmt'
tags:   [go, golang, fmt]   
---
[Source](https://medium.com/go-walkthrough/go-walkthrough-fmt-55a14bbbfc53 "Permalink to fmt – Go Walkthrough – Medium")

# fmt – Go Walkthrough – Medium

![Go to the profile of Ben Johnson][1]

In the [last post][2] we looked at fast, primitive encoding using [strconv][3] but in this post we'll take a higher level approach by using templates with the [fmt][4] package. The [fmt][4] package builds on top of our knowledge of primitive formatting options but packages it in a way that's easier to use.

This post is part of a series of walkthroughs to help you understand the standard library better. While generated documentation provides a wealth of information, it can be difficult to understand packages in a real world context. This series aims to provide context of how standard library packages are used in every day applications. If you have questions or comments you can reach me at [@benbjohnson][5] on Twitter.

#### Important note before we continue

Before we go any further, I should note that the "fmt" package is pronounced "fumpt". This surprises a lot of people.

I know… it sounds ridiculous.

However, if you get in a conversation with a fellow Gopher and refer to the "format" package or the "ef-em-tee" package then they may give you a blank stare.

### What are templates?

The key concept in the [fmt][4] package is the format template. This is a string that contains the text you want to print plus some placeholders (called _verbs_) that tell [fmt][4] where to insert your variables.

These format strings are based on C's [printf][6]() so they use a _%_ symbol and a letter to indicate a placeholder. For example, "_%s"_ is a placeholder for a string.

#### Verbs

The [_Printing_][7]_ _section of the [fmt][4] godoc has a lengthy explanation of all the options for these verbs but I'll give a summary of the most useful ones:

* _%v_ is a generic placeholder. It will automatically convert your variable into a string with some default options. This is typically useful when printing primitives such as strings or numbers and you don't need specific options.
* _%#v_ prints your variable in Go syntax. This means you could copy the output and paste it into your code and it'll syntactically correct. I find this most useful when working with structs and slices because it will print out the types and field names.
* _%T_ prints your variable's type. This is really useful for debugging if your data is passed as an _interface{}_ and you want to see what its concrete type.
* _%d _prints an integer in base-10. You can do the same with _%v _but this is more explicit.
* _%x _and _%X_ print an integer in base-16. One nice trick though is that you can pass in a byte slice and it'll print each byte as a two-digit hex number.
* _%f_ prints a floating point number without an exponent. You can do the same with _%v_ but this becomes more useful when we add width and precision flags.
* _%q _prints a quoted string. This is useful when your data may have invisible characters (such as [zero width space][8]) because the quoted string will print them as escape sequences.
* _%p_ prints a pointer address of your variable. This one is really useful when you're debugging code and you want to check if different pointer variables reference the same data.

#### Width & precision

We can make formatting more useful by adding various flags to the verb. This is especially important for floating-point numbers where you typically need to round them to a specific number of decimal places.

The precision can be specified by adding a period and a number after the _%_ sign. For example, we can use _%.2f_ to specify two decimal places of precision so formatting _100.567_ would print as "_100.57". _Note that the second decimal place is rounded.

The width specifies the total number of characters your formatted string will take up. If your formatted value is less than width then it will pad with spaces. This is useful when you're printing tabular data and you want fields to line up. For example, we can add to our previous format and set the width to _8_ by adding the number before the decimal place: _%8.2f_. Printing _100.567_ with this format will return "••100.57" (where • is a space).

We can map this out in a table to show how it works for various widths and precisions:
    
    
    %8.0f ➡ "     101"  
    %8.1f ➡ "   100.6"  
    %8.2f ➡ "  100.57"  
    %8.3f ➡ " 100.567"

#### Left alignment

In our previous example our values were right-aligned. This works well for financial applications where you may want the decimal places lined up on the right. However, if you want to left-align your fields you can use the "-" flag:
    
    
    %-8.0f ➡ "101     "  
    %-8.1f ➡ "100.6   "  
    %-8.2f ➡ "100.57  "  
    %-8.3f ➡ "100.567 "

#### Zero padding

Sometimes you want to pad using zeros instead of spaces. For instance, you may need to generate fixed-width strings from an number. We can use the zero ('_0')_ flag to do this. Printing the number _123_ with an 8-byte width and padded with zeros looks like this:
    
    
    %08d ➡ "00000123"

#### Spacing

When you print out byte slices using the _%x_ verb it comes out as one giant string of hex numbers. You can delimit the bytes with a space by using the _space _flag (_' '_).

For example, formatting _[]byte{1,2,3,4}_ with and without the space flag:
    
    
    %x  ➡ "01020304"  
    % x ➡ "01 02 03 04"

#### Other verbs & flags

There are still a bunch more verbs and flags that I didn't cover and you can read about them in detail in the [_Printing_][7] section of the [fmt][4] godoc. The ones I presented here are the ones I use the vast majority of the time.

### Printing

The primary use of the fmt package is to format strings. These formatting functions are grouped by their output type — STDOUT, io.[Writer][9], & string.

Each of these groups has 3 functions — default formatting, user-defined formatting, and default formatting with a new line appended.

#### Printing to STDOUT

The most common use of formatting is to print to a terminal window through STDOUT. This can be done with the _Print_ functions:
    
    
    func Print(a ...interface{}) (n [int][10], err [error][11])  
    func Printf(format [string][12], a ...interface{}) (n [int][10], err [error][11])  
    func Println(a ...interface{}) (n [int][10], err [error][11])

The [Print][13]() function simply prints a list of variables to STDOUT with default formatting. The [Printf][14]() function allows you to specify the formatting using a format template. The [Println][15]() function works like [Print][13]() except it inserts spaces between the variables and appends a new line at the end.

I typically use [Printf][14]() when I need specific formatting options and [Println][15]() when I want default options. I almost always want a new line appended so I don't personally use [Print][13]() much. One exception is if I am requesting interactive input from a user and I want the cursor immediately after what I print. For example, this line:
    
    
    fmt.Print("What is your name? ")

Will output to the terminal with the cursor immediately after the last space:
    
    
    What is your name? █

#### Printing to io.Writer

If you need to print to a non-STDOUT output (such as STDERR or a buffer) then you can use the _Fprint_ functions. The _"F"_ in these functions comes _FILE_ which was the argument type used in C's [_fprintf_][6]_() _function.
    
    
    func Fprint(w [io][16].[Writer][9], a ...interface{}) (n [int][10], err [error][11])  
    func Fprintf(w [io][16].[Writer][9], format [string][12], a ...interface{}) (n [int][10], err [error][11])  
    func Fprintln(w [io][16].[Writer][9], a ...interface{}) (n [int][10], err [error][11])

These functions are just like the _Print_ functions except you specify the writer as the first argument. In fact, the _Print_ functions are just small wrappers around the _Fprint_ functions.

I typically abstract STDOUT away from my components so I use _Fprint_ functions a lot. For example, if I have a component that logs information then I'll add a _LogOutput_ field:
    
    
    type MyComponent struct {  
    LogOutput io.Writer  
    }

That way I can attach STDOUT when I use it in my application:
    
    
    var c MyComponent  
    c.LogOutput = os.Stdout

And I can attach a buffer when I use it in my tests so I can validate it:
    
    
    var c MyComponent
    
    
    var buf bytes.Buffer  
    c.LogOutput = &buf  
    c.Run()
    
    
    if strings.Contains(buf.String(), "component finished") {  
    t.Fatalf("unexpected log output: %s", buf.String())  
    }

#### Formatting to a string

Sometimes you need to work with strings instead of writers. You could use the the _Fprint_ functions to write to a buffer and convert it to a string but that's a lot of work. Fortunately there are the _Sprint_ convenience functions:
    
    
    func Sprint(a ...interface{}) [string][12]  
    func Sprintf(format [string][12], a ...interface{}) [string][12]  
    func Sprintln(a ...interface{}) [string][12]

The "S" here stands for "String". These functions take the same arguments as the _Print() _functions except they return a _string_.

While these functions are convenient, they can be a bottleneck if you're frequently generating strings. If you profile your application and find that you need to optimize it then reusing a bytes.[Buffer][17] with the _Fprint()_ functions can be much faster.

#### Error formatting

One last formatting function that doesn't quite fit into the other groups is [Errorf][18]():
    
    
    func Errorf(format [string][12], a ...interface{}) [error][11]

This is literally just a wrapper for errors.[New][19]() and [Sprintf][20]():
    
    
    func Errorf(format string, a ...interface{}) error {  
    return errors.New(Sprintf(format, a...))  
    }

### Scanning

We can also read our formatted data and parse it back into our original variables. This is called _scanning_. These are also broken up into similar groups as the printing functions — read from STDIN, read from an io.[Reader][21], and read from a string.

#### Disclaimer

I personally almost never use the scan functions in my applications. Most input for my applications come from CLI flags, environment variables, or API calls. These are typically already formatted as a basic primitives and I can use [strconv][3] to parse them. That being said, I'll do my best to explain these with limited experience.

#### Scanning from STDIN

The basic _Scan_ functions operate on STDIN just as the basic _Print_ functions operate on STDOUT. These also come in 3 types:
    
    
    func Scan(a ...interface{}) (n [int][10], err [error][11])  
    func Scanf(format [string][12], a ...interface{}) (n [int][10], err [error][11])  
    func Scanln(a ...interface{}) (n [int][10], err [error][11])

The [Scan][22]() function reads in space-delimited values into variables references passed into the function. It treats newlines as spaces. The [Scanf][22]() does the same but lets you specify formatting options using a format string. The [Scanln][23]() function works [Scan][22]() except that it does not treat newlines as spaces.

For example, you can use [Scan][22]() to read in successive values:
    
    
    var name string  
    var age int
    
    
    if _, err := fmt.Scan(&name, &age); err != nil {  
    fmt.Println(err)  
    os.Exit(1)  
    }
    
    
    fmt.Printf("Your name is: %sn", name)  
    fmt.Printf("Your age is: %dn", age)

You can run this in your _main()_ function and execute:
    
    
    $ go run main.go  
    Jane 25  
    Your name is: Jane  
    Your age is: 25

Again, these functions aren't terribly useful because you will likely pass in data via flags, environment variables, or configuration files.

#### Scanning from io.Reader

You can use the _Fscan_ functions to scan from a reader besides STDIN:
    
    
    func Fscan(r [io][16].[Reader][21], a ...interface{}) (n [int][10], err [error][11])  
    func Fscanf(r [io][16].[Reader][21], format [string][12], a ...interface{}) (n [int][10], err [error][11])  
    func Fscanln(r [io][16].[Reader][21], a ...interface{}) (n [int][10], err [error][11])

#### Scanning from a string

Finally, you can use the _Sscan _functions to scan from an in-memory string:
    
    
    func Sscan(str [string][12], a ...interface{}) (n [int][10], err [error][11])  
    func Sscanf(str [string][12], format [string][12], a ...interface{}) (n [int][10], err [error][11])  
    func Sscanln(str [string][12], a ...interface{}) (n [int][10], err [error][11])

### Stringers

Go has a style convention where the names of interfaces are created by taking the name of the interface's function and adding _"er"_. Because of this convention we get funny names like _Stringer._

The [Stringer][24] interface allows objects to handle how they convert themselves into a human readable format:
    
    
    type Stringer interface {  
    String() [string][12]  
    }

This is used all over the place in the standard library such as net.[IP][25].[String][26]() or bytes.[Buffer][17].[String][27](). This format is used when using the _%s_ formatting verb or when passing a variable into [Print][13]().

#### Formatting to Go

There is also a related interface called [GoStringer][28] which allows objects to encode themselves in Go syntax.
    
    
    type GoStringer interface {  
    GoString() [string][12]  
    }

This is used when the "_%#v"_ verb is used in print functions. It's uncommon to need to use this interface since the default implementation of that verb usually gives a good representation.

### User-defined types

One of the more obscure parts of the [fmt][4] package is in user-defined formatter and scanner types. These give you full control over how an object gets formatted or scanned when using the _Printf _and_ Scanf_ functions.

#### Formatters

Custom formatters can be added to your types by implementing fmt.[Formatter][29]:
    
    
    type Formatter interface {  
    Format(f [State][30], c [rune][31])  
    }

The Format() function accepts a [State][30] object which lists the options specified in the verb associated with your variable. This includes the width, precision, and other flags. The _c_ rune specifies the character used in the verb.

#### Concrete example

A concrete example will make more sense. Honestly, I couldn't think of a good example of when to use this so we'll make a silly one. Let's say we have a _Header _type that is simply text that we want to be able to decorate using characters before and after. Here's our example usage:
    
    
    hdr := Header("GO WALKTHROUGH")  
    fmt.Printf("%2.3sn", hdr)

Here we're just printing _hdr_ with the verb _"%2.3s" _meaning that we want 2 characters before the header and 3 characters after. Just for fun we'll use "#" before the text and snowmen (☃) after the text. I know… dumb example but bear with me.

Here's our _Header_ type with its custom [Formatter][29] implementation:
    
    
    // Header represents formattable header text.  
    type Header string
    
    
    // Format decorates the header with pounds and snowmen.  
    func (hdr Header) Format(f fmt.State, c rune) {  
    wid, _ := f.Width()  
    prec, _ := f.Precision()
    
    
            f.Write([]byte(strings.Repeat("#", wid)))  
    f.Write([]byte(hdr))  
    f.Write([]byte(strings.Repeat("☃", prec)))  
    }

In our _Format()_ function we're extracting the _width_ and _precision_ from the verb (_2_ & _3_, respectively) and then printing back into the _State_ object. This will get written to our [Printf][14]() output.

You can [run this example here][32] and see the following output:
    
    
    ##GO WALKTHROUGH☃☃☃

Inside the standard library the formatter is used for special math types such as big.[Float][33] & big.[Int][34]. Those seem like legitimate use cases but I really can't think of another time to use this.

#### Scanners

On the scanning side there is a [Scanner][35] interface:
    
    
    type Scanner interface {  
    Scan(state [ScanState][36], verb [rune][31]) [error][11]  
    }

This works similarly to the [Formatter][29] except that you're reading from your _state_ instead of writing to it.

### Review

Let's review some of the do's and don'ts of using the [fmt][4] package:

* _Do _pronounce the package as "fumpt"!
* _Do_ use the _%v_ placeholder if you don't need formatting options.
* _Do_ use the width & precision formatting options — especially for floating-point numbers.
* _Don't_ use _Scan_ functions in general. There's probably a better method of user input.
* _Do_ define _String()_ functions on your types if the default implementation isn't useful.
* _Don't_ use custom formatters & scanners. There's not a lot of good use cases unless you're implementing mathematical types.
* Don't use [fmt][4] if you've found it to be a performance bottleneck. Drop down to [strconv][3] when you need to optimize. **However, only do this after profiling!**

### Conclusion

Printing output into a human readable format is a key part of any application and the [fmt][4] package makes it easy to do. It provides a variety of formatting options and verbs depending on the type of data you're displaying. It also provides scanning functions and custom formatting if you ever happen to need that.

_If you liked this, click the💚 below so other people will see this here on Medium._

[1]: https://cdn-images-1.medium.com/fit/c/100/100/0*Tt5_zERCmn2SZ2pv.jpeg
[2]: https://medium.com/@benbjohnson/go-walkthrough-strconv-7a24632a9e73
[3]: https://golang.org/pkg/strconv/
[4]: https://golang.org/pkg/fmt/
[5]: https://twitter.com/benbjohnson
[6]: http://pubs.opengroup.org/onlinepubs/009695399/functions/fprintf.html
[7]: https://golang.org/pkg/fmt/#hdr-Printing
[8]: http://www.fileformat.info/info/unicode/char/200b/index.htm
[9]: https://golang.org/pkg/io/#Writer
[10]: https://golang.org/pkg/builtin/#int
[11]: https://golang.org/pkg/builtin/#error
[12]: https://golang.org/pkg/builtin/#string
[13]: https://golang.org/pkg/fmt/#Print
[14]: https://golang.org/pkg/fmt/#Printf
[15]: https://golang.org/pkg/fmt/#Println
[16]: https://golang.org/pkg/io/
[17]: https://golang.org/pkg/bytes/#Buffer
[18]: https://golang.org/pkg/fmt/#Errorf
[19]: https://golang.org/pkg/errors/#New
[20]: https://golang.org/pkg/fmt/#Sprintf
[21]: https://golang.org/pkg/io/#Reader
[22]: https://golang.org/pkg/fmt/#Scan
[23]: https://golang.org/pkg/fmt/#Scanln
[24]: https://golang.org/pkg/fmt/#Stringer
[25]: https://golang.org/pkg/net/#IP
[26]: https://golang.org/pkg/net/#IP.String
[27]: https://golang.org/pkg/bytes/#Buffer.String
[28]: https://golang.org/pkg/fmt/#GoStringer
[29]: https://golang.org/pkg/fmt/#Formatter
[30]: https://golang.org/pkg/fmt/#State
[31]: https://golang.org/pkg/builtin/#rune
[32]: https://play.golang.org/p/XB2MEbC8x2
[33]: https://golang.org/pkg/math/big/#Float
[34]: https://golang.org/pkg/math/big/#Int
[35]: https://golang.org/pkg/fmt/#Scanner
[36]: https://golang.org/pkg/fmt/#ScanState

  