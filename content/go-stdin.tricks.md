---
title:  'go files'
tags:   [go, file, files, path]   
---

# Tipy a triky pro práci s STDIN

## Press ENTER to continue...

```
// var 1
bufio.NewReader(os.Stdin).ReadBytes('\n') 

// var 2
var input string
fmt.Scanln(&input)
```

## Press any key to continue

```
bufio.NewReader(os.Stdin).ReadRune()
```

## Ask for confirmation

```
package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

// askForConfirmation asks the user for confirmation. A user must type in "yes" or "no" and
// then press enter. It has fuzzy matching, so "y", "Y", "yes", "YES", and "Yes" all count as
// confirmations. If the input is not recognized, it will ask again. The function does not return
// until it gets a valid response from the user.
func askForConfirmation(s string) bool {
	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Printf("%s [y/n]: ", s)

		response, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}

		response = strings.ToLower(strings.TrimSpace(response))

		if response == "y" || response == "yes" {
			return true
		} else if response == "n" || response == "no" {
			return false
		}
	}
}

func main() {
	c := askForConfirmation("Do you really want to reset your system?")
	if c {
		fmt.Println("Sorry dude, I need root rights...")
		return
	}
}
```