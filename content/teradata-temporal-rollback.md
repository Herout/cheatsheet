---
title:  'Temporální rollback on Teradata'
tags:   [teradata,temporal,rollback]
---

# Postup provedení temporal rollbacku na Teradatě


```sql
--
-- temporal tabulku nelze vytvořit s daty, protože dojde k zprasení transakčních časů
--
create table ed0_tgt.prod_param as ep_tgt.prod_param with no data and stats;

--
-- níže uvedený časový řez je čas dojetí loadu dne, kdy byly data naposledy správně
-- přeneseme všechny záznamy, které vznikly v daném (či předchozím)  loadu
--
nontemporal insert into ed0_tgt.prod_param
select * from ep_tgt.prod_param
where hist_type = 251202905
and tt_start_dttm <= timestamp '2019-01-18 07:33:35.999999+01:00';

--
-- následně je potřeba znovu otevřít záznamy, které některý z **NÁSLEDUJÍCÍCH** loadů uzavřel
--
nontemporal update ed0_tgt.prod_param
set tt_end_dttm = until_closed
where tt_end_dttm > timestamp '2019-01-18 07:33:35.999999+01:00';

colect stats ed0_tgt.prod_param;
```