---
title:  ''
tags:   [go, runtime, concurrency]
---

# Počet jader

```
//Get the number of available logical cores. Usually this is 2*c where c is the number of physical cores and 2 is the number of hyperthreads per core. 
n := runtime.GOMAXPROCS(0)
```