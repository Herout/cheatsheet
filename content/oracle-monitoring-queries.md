---
tags:   [oracle,monitoring]
title:  'Oracle monitoring'
---

## Oracle: in flight SQL

```sql
    SELECT 
            nvl(ses.username,'ORACLE PROC') 
                    ||' (' 
                    ||ses.sid 
                    ||')' USERNAME 
        , SID 
        , SERIAL# 
        , MACHINE 
        , REPLACE(SQL.SQL_TEXT,CHR(10),'') STMT 
        , ltrim(to_char(floor(SES.LAST_CALL_ET/3600), '09')) 
                    || ':'
                    || ltrim(to_char(floor(MOD(SES.LAST_CALL_ET, 3600)/60), '09')) 
                    || ':'
                    || ltrim(to_char(MOD(SES.LAST_CALL_ET, 60), '09')) RUNT
    FROM     V$SESSION                                                   SES 
        , V$SQLtext_with_newlines                                     SQL
    WHERE 
            SES.STATUS             = 'ACTIVE'
    AND      SES.USERNAME IS NOT NULL
    AND      SES.SQL_ADDRESS        = SQL.ADDRESS
    AND      SES.SQL_HASH_VALUE     = SQL.HASH_VALUE
    AND      Ses.AUDSID            <> userenv('SESSIONID')
    ORDER BY 
            runt DESC 
        , 1 
        , sql.piece;
```

## Oracle: kill session

```sql
    ALTER SYSTEM KILL SESSION '<insert_SID>,<insert_SERIAL#>';
```

   
### Oracle: stats

    ANALYZE TABLE <table_name> COMPUTE STATISTICS;

    EXECUTE dbms_stats.gather_schema_stats('PDC_NEW_1', cascade=>TRUE);

    SELECT * FROM ALL_TAB_STATS_HISTORY

    SELECT owner, TABLE_NAME, NUM_ROWS,TO_CHAR(LAST_ANALYZED,'DD.MM.YYYY HH24:MI:SS') FROM DBA_TABLES

    select table_name, column_name, count(*) 
    from dba_histograms  
    where owner='PDC_NEW_3'
        and table_name='CTRL_JOB'
    group by table_name, column_name;
 
## Oracle: long queries

```sql
    SELECT 
             s.username 
           , s.sid 
           , s.serial# 
           , s.last_call_et/60 mins_running 
           , q.sql_text 
    FROM     v$session               s
    JOIN     v$sqltext_with_newlines q
    ON       s.sql_address = q.address
    WHERE 
             status      ='ACTIVE'
    AND      TYPE       <>'BACKGROUND'
    AND      last_call_et> 60
    ORDER BY 
             sid 
           , serial# 
           , q.piece
```

## Oracle: blocked tables

```sql
    SELECT B.Owner, B.Object_Name, A.Oracle_Username, A.OS_User_Name  
    FROM V$Locked_Object A, All_Objects B
    WHERE A.Object_ID = B.Object_ID
```

## Oracle: locks

```sql
SELECT
         object_name
       , object_type
       , session_id
       , TYPE
       , -- Type or system/user lock
         lmode
       , -- lock mode in which session holds lock
         REQUEST
       , block
       , ctime -- Time since current mode was granted
FROM     v$locked_object
       , all_objects
       , v$lock
WHERE
         v$locked_object.object_id = all_objects.object_id
AND      v$lock.id1                = all_objects.object_id
AND      v$lock.sid                = v$locked_object.session_id
ORDER BY
         session_id
       , ctime DESC
       , object_name
```

## Oracle: source of block
   
```sql   
    COLUMN host FORMAT a6 COLUMN username FORMAT a10 COLUMN os_user FORMAT a8 COLUMN program FORMAT a30 COLUMN tsname FORMAT a12
    SELECT
           b.machine  host 
         , b.username username 
         , b.server 
         , b.osuser          os_user 
         , b.program         program 
         , a.tablespace_name ts_name 
         , row_wait_file#    file_nbr 
         , row_wait_block#   block_nbr 
         , c.owner 
         , c.segment_name 
         , c.segment_type
    FROM   dba_data_files a 
         , v$session      b 
         , dba_extents    c
    WHERE
           b.row_wait_file# = a.file_id
    AND    c.file_id        = row_wait_file#
    AND    row_wait_block# BETWEEN c.block_id AND    c.block_id + c.blocks - 1
    AND    row_wait_file# <> 0
    AND    TYPE            ='USER' ;
```

## Oracle: blocking sessions

```sql
SELECT 
         /*+ RULE */
         s.sid 
       , s.serial# 
       , p.spid "OS SID" 
       , s.sql_hash_value "HASH VALUE" 
       , s.username "ORA USER" 
       , s.status 
       , s.osuser "OS USER" 
       , s.machine 
       , s.terminal 
       , s.type 
       , s.program 
       , s.logon_time 
       , s.last_call_et 
       , s.sql_id 
       , l.id1 
       , l.id2 
       , decode(l.block,0,'WAITING',1,'BLOCKING')                                                                                 block 
       , decode( l.LMODE,1,'No Lock', 2,'Row Share', 3,'Row Exclusive', 4,'Share', 5,'Share Row Exclusive', 6,'Exclusive',NULL)   lmode 
       , decode( l.REQUEST,1,'No Lock', 2,'Row Share', 3,'Row Exclusive', 4,'Share', 5,'Share Row Exclusive', 6,'Exclusive',NULL) REQUEST 
       , round(l.ctime/60,2) "MIN WAITING" 
       , l.type
FROM     v$process p 
       , v$session s 
       , v$Lock    l
WHERE 
         p.addr = s.paddr
AND      s.sid  =l.sid
AND     (l.id1,l.id2,l.type) IN
         (SELECT 
                 l2.id1 
               , l2.id2 
               , l2.type
         FROM    V$LOCK l2
         WHERE 
                 l2.request<>0)
ORDER BY 
         l.id1 
       , l.id2 
       , l.block DESC;
```    
    
## Oracle: temp tablespace

```sql
    set pages 999
    set lines 400
    col FILE_NAME format a75
    select d.TABLESPACE_NAME, d.FILE_NAME, d.BYTES/1024/1024 SIZE_MB, d.AUTOEXTENSIBLE, d.MAXBYTES/1024/1024 MAXSIZE_MB, d.INCREMENT_BY*(v.BLOCK_SIZE/1024)/1024 INCREMENT_BY_MB
    from dba_temp_files d,
     v$tempfile v
    where d.FILE_ID = v.FILE#
    order by d.TABLESPACE_NAME, d.FILE_NAME;
```

```sql    
    ALTER TABLESPACE TEMP ADD TEMPFILE 'F:\ORACLE\ORADATA\BIDEVELOP\TEMP04.DBF' SIZE 5120M REUSE AUTOEXTEND ON NEXT 1M MAXSIZE 5120M;
```