---
title: "Teradata unused objects"
tags: [teradata, unused, nupi, index, object, dbi]
---

# Které tabulky byly použité za posledních 90 dní?

```
SELECT
                a.objectdatabasename
              , a.objecttablename

FROM            pdcrdata.dbqlobjtbl_hst AS a
WHERE           a.collecttimestamp (DATE) > Current_Date - 90
AND             a.objecttype              = 'tab'
GROUP BY 1,2
```

# Které indexy byly použity v průběhu uplynulých 90 dní, jak a kým ?

Viz [dbc.dbhltblobj](https://docs.teradata.com/reader/wada1XMYPkZVTqPKz2CNaw/O4Zfk2Mik~D4T~oCE1w_Iw).

```sql
SELECT
                a.objectdatabasename
              , a.objecttablename
              , a.objectcolumnname
                /* pokud jde o index, vrací se sloupce na indexu */
              , a.objectid
              , a.objectnum
              , a.typeofuse
              , a.queryid
              , b.userName
              , COUNT(*) OVER (
               PARTITION BY
                               a.objectdatabasename
                             , a.objecttablename
                             , a.objectcolumnname
                             , a.objectid
                             , a.objectnum
                             , a.queryid ) AS pocet_vet
FROM            pdcrdata.dbqlobjtbl_hst    AS a
LEFT OUTER JOIN pdcrdata.dbqlogtbl_hst     AS b
ON              a.queryid = b.queryid
WHERE
                a.collecttimestamp (DATE) > CURRENT_DATE - 90
AND             a.objecttype              = 'idx'
AND             a.objectdatabasename      = 'EP_IND'
AND             a.objecttablename      LIKE 'CCR%'
QUALIFY
                ROW_NUMBER() OVER (
                   PARTITION BY
                                   a.objectdatabasename
                                 , a.objecttablename
                                 , a.objectcolumnname
                                 , a.objectid
                                 , a.objectnum
                   ORDER BY
                                   a.collecttimestamp DESC ) = 1 ;

```

**objecttype**

| type  | comment                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ----- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `Agg` | User-defined aggregate function                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| `AgS` | User-defined aggregate STAT function                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| `Aut` | Security authorization                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| `Col` | Column                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| `DB`  | Database                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| `GLP` | GLOP set                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| `HIx` | Hash index                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `Idx` | Index. For each index, there is a database name, table name, and column name. The ObjectId column is the identifier of the table and the ObjectNum column is the number of the index in that table. For multi-column indexes, there is one row for each column of the index that a query used. For example, if an index consists of three columns and the query uses all three, there will be three rows, each with a different column name. The column name will be null for an index for statements such as COLLECT STATISTICS, ALTER PROCEDURE, SHOW PROCEDURE, or SELECT COUNT(\*). |
| `JIx` | Join index. For each join index, there is a database name and join index name in the ObjectTableName field. For these rows, the ColumnName indicates a column referred to by the join index. ObjectType is ‘JIx.’ ObjectId matches the ID of the join index. ObjectNum is 0.                                                                                                                                                                                                                                                                                                            |
| `Jrl` | Journal                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `Mac` | Macro                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| `NoT` | No type (unknown)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `SP`  | Stored procedure                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| `Sta` | User-defined STAT function                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `Tab` | Table                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| `TbF` | Table function                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `Tmp` | Temporary                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| `TbO` | Table operator                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `TbC` | Contract function                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `Trg` | Trigger                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `UDF` | User-defined function                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| `UDM` | User-defined method                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| `UDT` | User-defined type                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `Viw` | View                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| `Vol` | Volatile                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| `XSP` | External stored procedure                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |

**type of use**

- 1 = Found in the resolver
- 2 = Accessed during query processing
- 4 = Found in a conditional context
- 8 = Found in inner join condition
- 16 = Found in outer join condition
- 32 = Found in a sum node
- 64 = Found in a full outer join condition
