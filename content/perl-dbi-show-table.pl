use v5.24;    # implicit strict
use utf8;
use open qw/:std :utf8/;   # UTF-8 default on all handles (+STDIN/STDOUT/STDERR)
use Carp;                  # carp=warn, croak=die, confess=die
use Path::Tiny;
use DBI;
use Data::Dumper;
use Type::Params qw(compile_named_oo);
use Types::Standard qw(FileHandle HasMethods Str Object);
use Encode;
use Try::Tiny;

my $root = path($0)->parent;

my $show_types = {
    'P' => 'procedure',
    'T' => 'table',
    'V' => 'view',
    'O' => 'table'     ,
            'Q' => 'table'     ,
            'M' => 'macro'     ,
            'G' => 'trigger'   ,
            'I' => 'join index',    
};

my $dbh = DBI->connect('dbi:ODBC:EDWPROD_64BIT_TECH', $ENV{TO2_DOMAIN_USER}, $ENV{TO2_DOMAIN_PASSWORD}, {RaiseError => 1, LongReadLen => 1024*1024});
my @rows = $dbh->selectall_array(q/select trim(databasename) as databasename, trim(tablename), tablekind as tablename from dbc.tables where databasename = 'UEP_E22_LOAD'/);

foreach my $o (@rows) {
    my ($databasename, $tablename, $tablekind) = $o->@*;
    my $ddl = teradata_show( dbh => $dbh, databasename => $databasename, tablename => $tablename, tablekind => $tablekind);
    
    # do souboru
    #$root->child( $show_types->{$tablekind} )->mkpath;
    my $cp1250_ddl = encode ('cp1250', $ddl, Encode::FB_CROAK);
    $root->child( (uc $tablename) . ".sql" )->spew_raw( $cp1250_ddl );
}

sub teradata_show {
    state $check = compile_named_oo(
      dbh        =>  Object,
      databasename   => Str,
      tablename  => Str,
      tablekind => Str,
    );
    my $arg = $check->(@_);
    $show_types->{$arg->{tablekind}} or confess "Can not translate '". $arg->{tablekind} . "', how do I show it?";
    
    my $dbh = $arg->{dbh};
    my $table = $arg->{tablename};
    my $database = $arg->{databasename};
    my $objectType = $show_types->{$arg->{tablekind}};
        
    # definice
    my $sql = 'show '.$objectType . '"' . $database .'"."' . $table .'";';
    my $result = _exec_and_return($dbh, $sql);

    # koment��e
    if ($objectType eq 'table' or $objectType eq 'view') {
        my @comments;
        my $sql = "LOCKING ROW FOR ACCESS SELECT CommentString  FROM DBC.TablesV  WHERE DatabaseName = '$database' and tablename = '$table' and coalesce(CommentString,'')<>''";
        foreach my $rec ($dbh->selectall_arrayref($sql)->@*) {
            $rec->[0] =~s/'/''/g;
            push @comments, qq/COMMENT ON TABLE $database."$table" is '/ . $rec->[0] . "';"
        }
        
        $sql = "LOCKING ROW FOR ACCESS SELECT trim(ColumnName) as columnname, CommentString  FROM DBC.Columns  WHERE DatabaseName = '$database' and tablename = '$table' and coalesce(CommentString,'')<>'' order by columnId asc";
        foreach my $rec ($dbh->selectall_arrayref($sql)->@*) {
            $rec->[1] =~s/'/''/g;
            push @comments, qq/COMMENT ON COLUMN $database."$table"."/ . $rec->[0] . q/" is '/ . $rec->[1] . "';"
        }
        
        if (@comments) {
            $result .= "\n" . join "\n", '', "-- comments ", @comments;            
        }
    }
    
    # statistiky
    if ($objectType eq 'table') {
        my $sql = 'show stats on "' . $database .'"."' . $table .'";';
        my $ddl = _exec_and_return($dbh, $sql);
        if ($ddl) {
            $result .= "\n" . join "\n", '', "-- $sql", $ddl;
        }
    }
    
    
    return $result;
}

sub _exec_and_return {
    my ($dbh, $sql) = @_;
    my $sth; 
    my $ok = 1;
    try {
        $sth = $dbh->prepare($sql);
        $sth->execute();
    } catch {
        $ok = 0;
    };
    return unless $ok;
    my $result = '';    
    # 
    # "show table", for reasons I cannot fathom, returns a single row with carriage returns (\r). 
    # When you display these in Unix, they return the cursor to the beginning of the line without advancing to the next line; 
    # hence you see only the last "line", along with leftover junk from previous lines. Just s/\r/\n/g and the problem is solved.
    while (my $text = $sth->fetchrow_array) {        
       $result .= join "\n", split /\R/, $text;
    }
    return $result;
}