-------------------------------------------------------------------------------------------------
string.lower(), string.upper(), str.strip(), string.replace(s, old, new[, maxreplace])
print(f"nějaká {var}", "nějaká %" % (var), "nějaká {}".format(var))
-------------------------------------------------------------------------------------------------
from pathlib import Path
-------------------------------------------------------------------------------------------------
scriptDir  = Path(__file__).parent                      | 
file       = Path('/home/gahjelle/realpython/test.md')  | fileRoot   = file.anchor
name       = file.name             # test.md            | nameWoExt  = file.stem             # test
fileParent = file.parent           # nadřazená složka   | fileExt    = file.suffix           # .md
files      = dir.rglob("*.sql")    # recurse glob       | 

scriptDir.relative_to(someOtherDir)                                     # relpath
dir.mkdir(mode=0o777, parents=False, exist_ok=False)                    # make dir
subDirs = [ Path(sd.path) for sd in os.scandir(devDir) if sd.is_dir ]   # subdirs

-------------------------------------------------------------------------------------------------
import shutil
-------------------------------------------------------------------------------------------------
shutil.copy(str(path1), str(path2))                 # copies the file data and permission mode
shutil.copy2(str(path1), str(path2))                # copies all metadata (creation date ...)
shutil.copytree(src, dst, dirs_exist_ok=False,...)  
shutil.rmtree(path, ignore_errors=False, onerror=None)
shutil.move(src, dst, copy_function=copy2)          # Recursively move a file or directory
shutil.which(cmd)                                   # whereis cmd ...

-------------------------------------------------------------------------------------------------
import re
-------------------------------------------------------------------------------------------------
matcher    = re.compile(r"(?P<skupina>m)", re.I)
if m:=matcher.fullmatch("text")                     # match pro celý string 
if m:=matcher.match("text")                         # match od začátku, ale jen část
if m:=matcher.search("text")                        # match kdekoliv
for m in matcher.findall("text)                     # všechny výskyty v textu
    m.groupdict()                                   # key/val dict obsahující všechny match skupiny
    m.string                                        # matched string
    m.group("skupina")                              
-------------------------------------------------------------------------------------------------
# string je immutable, replacement vrací jeho novou verzi, narozdíl od Perlu !
text      = matcher.sub(r"\g<GRP1>AND \g<GRP2>", text)
text, cnt = re.subn(r"some (?P<GRP1> text)", lambda m: "|"+m.group("GRP1")+"|", text, flags=re.I)          

-------------------------------------------------------------------------------------------------
from datetime import datetime, timedelta
-------------------------------------------------------------------------------------------------
load_dttm = datetime.strptime( "2020-01-31 00:00:00", "%Y-%m-%d %H:%M:%S" )

-------------------------------------------------------------------------------------------------
import subprocess
-------------------------------------------------------------------------------------------------
git = subprocess.run([ "git", "fh", str(file) ], cwd=cwd, capture_output=True, encoding="UTF-8")
if git.returncode != 0:
    raise Exception(git.stderr)
first_stdout_line = git.stdout.split("\n")[0]    
