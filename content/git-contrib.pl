use v5.24;
use Data::Dumper;

#my @lines = qx/git log --date=format:"%Y-%m-%d" --format="<COMMIT>;%f;%ad;%aE" --stat --all --since="2 months ago"/;

my @dirs = (
    'c:\BI_Domain\bimain',
    'c:\BI_Domain\bimeta',
    'c:\BI_Domain\buildbot',
);

my @lines;
foreach (@dirs) {
    warn "dir=$_\n";
    my @l = qx/cd $_ && git log --date=format:"%Y-%m-%d" --format="<COMMIT>;%f;%ad;%aE" --all/;
    warn "lines=" . (scalar @l) . "\n";
    push @lines, @l;
}

=pod

<COMMIT>2020-04-15;petr.borek@o2.cz

 meta/mdl/Intraday/EP_IND.pdm                       | 30884 +++++++++++++------
 .../BIDEV-789_SAP_EP_IND/_Install.txt              |    18 +
 .../Teradata/01_replace_views/EP_IND/SAP_A006.SQL  |    59 +
 .../01_replace_views/EP_IND/SAP_ACCOUNT.SQL        |    38 +
 .../Teradata/01_replace_views/EP_IND/SAP_AUART.SQL |    33 +
 .../01_replace_views/EP_IND/SAP_BILLING_HEADER.SQL |    83 +
 .../01_replace_views/EP_IND/SAP_BILLING_ITEMS.SQL  |    80 +
 .../01_replace_views/EP_IND/SAP_CURR_COEF.SQL      |    41 +
 .../01_replace_views/EP_IND/SAP_CURR_LIST.SQL      |    41 +
 .../EP_IND/SAP_CUSTOMER_HEADER.SQL                 |    83 +
 .../EP_IND/SAP_DELIVERY_HEADER.SQL                 |    44 +
 .../01_replace_views/EP_IND/SAP_DELIVERY_ITEMS.SQL |    65 +
 .../EP_IND/SAP_DELIVERY_SERIAL1.SQL                |    38 +
 .../EP_IND/SAP_DELIVERY_SERIAL2.SQL                |    47 +
 .../Teradata/01_replace_views/EP_IND/SAP_FKART.SQL |    33 +
 .../Teradata/01_replace_views/EP_IND/SAP_FKREL.SQL |    33 +
 
=cut

my @wanted_authors = qw/frajbis.miroslav@o2.cz	jan.herout@o2.cz	jiri.dobsik@o2.cz	martin.zalepa@o2.cz	michal.dlouhy@o2.cz	petr.borek@o2.cz	stanislava.knoppova@o2.cz	tomas.stochmal@o2.cz	veronika.sakhanchuk@o2.cz/;

# kontext a data
my $data;
my $data_only_count;
my ($author, $date, $subject);
my $skip;

my $dates;

foreach my $line (@lines) {
    chomp $line;
        
    # pokud jde o ��dku s commitem, nastav si kontext
    if ($line =~ /^<COMMIT>/) {
        ($_, $subject, $date, $author) = split ';', $line;
        $author = lc $author;
        $author = 'jan.herout@o2.cz' if $author eq 'jan.herout@gmail.com';
        my ($yyyy, $mm, $dd) = split '-', $date;
        $dates->{"$yyyy$mm$dd"} = $date;
        $data->{$author}->{$date}->{count}++;
        # merge commity p�eskakujeme
        $skip = ( $subject =~ /merge/i ) ? 1 : 0;
        warn $line if ($author =~ /herout/i and "$yyyy$mm$dd" eq '20200423');
    } 
    # p�esko� pr�zdn� ��dky
    # p�esko� zm�ny na modelu, jsou matouc�, nepo��tej je
    elsif ($line =~ /\|/) {
        next if $skip;
        my ($file, $change) = split /\|/, $line;
        $change =~ s/\s*(\d+).*/$1/;
        
        # p�esko� zm�ny na modelu, jsou matouc�, nepo��tej je
        next if $file =~ /[.]pdm/i;        
        
        # zapo��tej po�et ��dek do statistiky
        $data->{$author}->{$date}->{change} += $change;
        $data->{$author}->{$date}->{count} += $change;
        
    }    
}

# report
print "date";
foreach my $a (@wanted_authors) {
    print ";$a";
}
print "\n";


foreach my $d (reverse sort keys $dates->%*) {
    my $d = $dates->{$d};
    print "$d";
    foreach my $a (@wanted_authors) {
        print ";" . ( $data->{$a}->{$d}->{count} // '0' );
    }
    print "\n";
}


__DATA__
foreach my $a ( sort keys $data->%* ) {
    foreach my $d ( reverse sort keys $dates->%* ) { 
        $d = $dates->{$d};
    #foreach my $d ( sort keys $data->{$a}->%* ) { 
        print "$d";
        #foreach my $a (sort keys $authors->%*) {
        foreach my $a (@wanted_authors) {
            print ";" . ( $data->{$a}->{$d}->{count} // '0' );
        }
        print "\n";        
    }
}
