package main

// Tento program je návodem jak korektně provést dlouhotrvající task
// představuje postup jak synchronizovat práci mezi vícero procesy přes global state

/*

aktuální stav: korektně se detekuje ukončení read operace, ale ticker a exit handler nedostávají informaci o tom, že mají končit => visí.

*/

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

// globalConf představuje konfiguraci
type globalConf struct {
	trfCount int
	filename string
	gswg     sync.WaitGroup
}

type transfBytes struct {
	valid bool
	bytes []byte
}


// globalState je sdílená datová struktura používaná pro synchronizaci mezi paralelními procesy v rámci programu
type globalState struct {
	// synchronizační primitiva
	wg sync.WaitGroup // wait group používaná pro zajištění konzistentního ukončení všech gorutin

	// globální konfigurace
	c globalConf

	// signalizace požadavku na ukončení jednotlivých kanálů
	// každá použitá gorutina tady má mít vlastní kanál
	sigEndTicker  chan bool
	sigEndReader  chan bool
	sigEndGoodW   chan bool
	sigEndBadW    chan bool
	sigEndTransf  []chan bool
	sigEndHandler chan bool

	// signalizace chyby. při první chybě exitHandler vyvolá konec pro všechny procesy a dojde k ukončení programu.
	chanErr chan error

	// fronty pro předávání dat
	queRead chan *transfBytes
	queBad  chan *transfBytes
	queGood chan *transfBytes

	// tady můžou být další globální proměnné používané všemi gorutinamy
	// zamykáme je přes mutex uvedený níže
	m    *sync.Mutex // PATTERN - mutex uvádím jako pointer, protože ho není možné kopírovat - a chci si na to zvyknout
	iter int
}

// newGlobalState je konstruktor pro global state. Je potřeba ho upravit vždy když připravujeme nový signalizační kanál.
func newGlobalState(c globalConf) (*globalState, error) {
	gs := &globalState{}
	gs.m = &sync.Mutex{} // inicializuji mutex, protože je to pointer
	gs.m.Lock()
	defer gs.m.Unlock()

	if c.trfCount < 1 {
		return gs, fmt.Errorf("number of transformers must be >= 1")
	}

	if c.filename == "" {
		return gs, fmt.Errorf("empty filename")
	}

	gs.c = c

	// inicializace chybového kanálu
	// buffered protože pokud se pokusíme odeslat chybu, a exitHandler už je dole,
	// dostali bychom se do deadlocku.
	gs.chanErr = make(chan error, 2)

	// inicializace front pro předávání dat
	gs.queBad = make(chan *transfBytes, 10)
	gs.queGood = make(chan *transfBytes, 10)
	gs.queRead = make(chan *transfBytes, 10)

	// inicializuji jednotlivé signalizační kanály
	// důvod, proč je na nich buffer je ten, že při ukončení programu vyvoláním chyby
	// gorutina která chybu vyvolala již nemusí být aktivní, a pokus o odeslání signálu konce
	// by zablokoval program.
	gs.sigEndTicker = make(chan bool, 2)
	gs.sigEndReader = make(chan bool, 2)
	gs.sigEndGoodW = make(chan bool, 2)
	gs.sigEndBadW = make(chan bool, 2)
	gs.sigEndHandler = make(chan bool, 2)
	gs.sigEndTransf = make([]chan bool, c.trfCount)
	for i := 0; i < c.trfCount; i++ {
		log.Println("making sigEndTransf", i)
		gs.sigEndTransf[i] = make(chan bool, 2)
	}
	log.Println("config done")
	return gs, nil
}

// gs je instance pro globalState; vedu jí jako globální proměnnou, aby byla dostupná napříč celým programem
var (
	gs  *globalState
	err error
)

func main() {
	log.Println("start") // -------------------------------------------------------------
	gc := globalConf{trfCount: 1, filename: "pokus.dat"}
	gs, err = newGlobalState(gc) // inicializuj globalState
	if err != nil {
		log.Fatal(err)
	}

	go exitHandler()   // inicializuj exit handler; registruje se na gs.wg
	go prepareTicker() // v této sekci provedeme reálnou práci; funkce se registruje na gs.wg
	go prepareReader()
	go prepareBadW()
	go prepareGoodW()

	for i := 0; i < gc.trfCount; i++ {
		log.Println(i)
		go prepareTrf(i)
	}

	log.Println("Waiting for all processes to end")
	gs.wg.Wait()
	log.Println("end") // -------------------------------------------------------------
}

//-----------------------------------------------------------------------------
// WORKERS
//-----------------------------------------------------------------------------

//
func prepareReader() {
	gs.wg.Add(1)
	defer gs.wg.Done()

	// inicializace. Lokalizuj si proměnné
	gs.m.Lock()
	fn := gs.c.filename
	gs.m.Unlock()

	// inicializace. otevři soubor pro čtení (s bufferem)
	fh, err := os.Open(fn)
	if err != nil {
		gs.chanErr <- fmt.Errorf("reader: %s: %w", fn, err)
		return
	}
	defer fh.Close()

	scanner := bufio.NewScanner(bufio.NewReader(fh))
scan_loop:
	// načti soubor řádek za řádkem, ukončeno za LF (je otázka co udělat s CR pokud je na poslední pozici)
	for scanner.Scan() {
		gs.queRead <- &transfBytes{valid: true, bytes: scanner.Bytes()}
		select {
		case <-gs.sigEndReader:
			break scan_loop
		default:
		}
	}
	log.Println("closing queRead")
	close(gs.queRead)
	log.Println("> shutting down reader")

}

func prepareGoodW() {
	gs.wg.Add(1)
	defer gs.wg.Done()
	
	for	{
		select {
			case record, ok := <- gs.queGood:
			if ok && record.valid {
				log.Println("got record")				
			}
			if !ok {
				log.Println("channel closed") // todo - tohle nejde protože mám N producentů
			}
			case <-gs.sigEndGoodW:
				break
		}
	}
	log.Println("> shutting down good writer")

}

func prepareBadW() {
	gs.wg.Add(1)
	defer gs.wg.Done()

	<-gs.sigEndBadW
	log.Println("> shutting down bad writer")

}


func split(line []byte) [][]byte {
	return bytes.Split(line, []byte{' '})
}

func validate([][]byte) err {
	return nil
}

func prepareTrf(trfId int) {
	gs.wg.Add(1)
	defer gs.wg.Done()
	log.Println("starting transformer ", trfId)
trf_loop:
	for {
		select {
		case bytes, ok := <-gs.queRead:
			if ok && bytes.valid {
				// zpracování daného řádku začíná zde
				// tady by asi měl být call funkce která vrátí zpátky datovou strukturu
				// v tomhle okamžiku aproximace přes pouhý slice
				inputPortValues := split(bytes.bytes)
				err := validate(inputPortValues)
				if err != nil {
					// TODO mock, tady bych měl posílat sadu chybových kódů a informaci o záznamu
					gs.queBad <- bytes
					continue trf_loop
				}
				
				// TODO mock. tady bych měl posílat opracovanou hodnotu portů
				gs.queGood <- bytes
			}
			if !ok {
				log.Println("channel was closed, exit, id ", trfId)
				break trf_loop
			}
		case _ = <-gs.sigEndTransf[trfId]:
			break trf_loop
		}
	}
	log.Println("> transformer ended : ", trfId)
}

// ticker provede ping na stdout co 5 vteřin
func prepareTicker() {
	log.Println("prepare ticker")
	gs.wg.Add(1) // zaregistruj se jako proces na jehož ukončení má program čekat
	defer gs.wg.Done()

	// hlavní smyčka simulující reálnou práci
tick_loop:
	for {
		select {
		// PATTERN: modifikace sdílených prostředků a mutex
		case _ = <-time.Tick(3 * time.Second):
			log.Println("tick")

		// PATTERN: pokud nemáš co dělat, a přišel signál konce, skonči
		case _ = <-gs.sigEndTicker:
			log.Println("> shutting down ticker")
			break tick_loop
		}
	}
}

//-----------------------------------------------------------------------------
// EXIT HANDLER
//-----------------------------------------------------------------------------

func signalEndToAll() {
	log.Println("... signal end ticker")
	gs.sigEndTicker <- true
	log.Println("... signal end reader")
	gs.sigEndReader <- true
	log.Println("... signal end good writer")
	gs.sigEndGoodW <- true
	log.Println("... signal end bad writer")
	gs.sigEndBadW <- true
	log.Println("... signal end exit handler")
	gs.sigEndHandler <- true
	for i := 0; i < gs.c.trfCount; i++ {
		log.Println("... signal end transformer", i)
		gs.sigEndTransf[i] <- true
	}

}

// SetupInterruptHandler číhá na Ctrl+C a odesílá informaci o tomto interruptu dál (channels v glogalState)
func exitHandler() {
	log.Println("setting up Ctrl+C handler")

	// PATTERN - zaregistruj se jako proces, který musí skončit
	gs.wg.Add(1)
	defer gs.wg.Done()

	// zavěs se na kanál a čekej na SIGTERM nebo na SIGKILL
	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM|syscall.SIGKILL)

	select {
	case <-c:
		log.Println("Ctrl+C pressed in Terminal, sending cancellation signal.")
		signalEndToAll()
	case err := <-gs.chanErr:
		log.Printf("ERR: %s", err)
		signalEndToAll()
	case <-gs.sigEndHandler:
		log.Println("Request to end...")
		signalEndToAll()
	}
	log.Println("> exit handler ended")
}
