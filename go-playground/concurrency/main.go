package main

// Tento program je návodem jak korektně provést dlouhotrvající task
// představuje postup jak synchronizovat práci mezi vícero procesy přes global state

import (
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

// globalState je sdílená datová struktura používaná pro synchronizaci mezi paralelními procesy v rámci programu
type globalState struct {
	// synchronizační primitiva
	m  *sync.Mutex    // PATTERN - mutex uvádím jako pointer, protože ho není možné kopírovat - a chci si na to zvyknout
	wg sync.WaitGroup // wait group používaná pro zajištění konzistentního ukončení všech gorutin

	// signalizace požadavku na ukončení jednotlivých kanálů
	// každá použitá gorutina tady má mít vlastní kanál
	sigEndTicker chan bool

	// tady můžou být další globální proměnné používané všemi gorutinamy
	// zamykáme je přes mutex uvedený výše
	iter int
}

// newGlobalState je konstruktor pro global state. Je potřeba ho upravit vždy když připravujeme nový signalizační kanál.
func newGlobalState() globalState {
	gs := globalState{}
	gs.m = &sync.Mutex{}              // inicializuji mutex, protože je to pointer
	gs.sigEndTicker = make(chan bool) // inicializuji jednotlivé signalizační kanály
	return gs
}

// gs je instance pro globalState; vedu jí jako globální proměnnou, aby byla dostupná napříč celým programem
var gs globalState

func main() {
	log.Println("start")  // -------------------------------------------------------------
	gs = newGlobalState() // inicializuj globalState
	exitHandler()         // inicializuj exit handler; registruje se na gs.wg
	go prepareTicker()    // v této sekci provedeme reálnou práci; funkce se registruje na gs.wg
	gs.wg.Wait()          // zde blohujeme a čekáme na signál, že všechy pracovní procesy jsou dole
	log.Println("end")    // -------------------------------------------------------------
}

//-----------------------------------------------------------------------------
// WORKERS
//-----------------------------------------------------------------------------

// ticker provede ping na stdout co 5 vteřin
func prepareTicker() {
	log.Println("prepare ticker")
	gs.wg.Add(1) // zaregistruj se jako proces na jehož ukončení má program čekat

	// hlavní smyčka simulující reálnou práci
	for {
		select {
		// PATTERN: modifikace sdílených prostředků a mutex
		case _ = <-time.Tick(1 * time.Second):
			gs.m.Lock()                    // zamkni si sdílené  prostředky
			log.Println("tick: ", gs.iter) // použij je
			gs.iter++                      // modifikuj stav
			gs.m.Unlock()                  // odblokuj je

		// PATTERN: pokud nemáš co dělat, a přišel signál konce, skonči
		case _ = <-gs.sigEndTicker:
			log.Println("shutting down ticker")
			gs.wg.Done()
			return
		}
	}
}

//-----------------------------------------------------------------------------
// EXIT HANDLER
//-----------------------------------------------------------------------------

// SetupInterruptHandler číhá na Ctrl+C a odesílá informaci o tomto interruptu dál (channels v glogalState)
func exitHandler() {
	log.Println("setting up Ctrl+C handler")

	// PATTERN - zaregistruj se jako proces, který musí skončit
	gs.wg.Add(1)

	// zavěs se na kanál a čekej na SIGTERM nebo na SIGKILL
	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM|syscall.SIGKILL)

	// nahoď proces, který blokuje a čeká na SIGTERM nebo na SIGKILL
	// teprve po jeho ukončení se odregistruj jako Done
	// pokud by tady tohle nebylo, main() bude končit předčasně
	go func() {
		<-c
		log.Println("Ctrl+C pressed in Terminal, sending cancellation signal.")
		gs.sigEndTicker <- true
		gs.wg.Done() // PATTERN - odregistruj se
	}()
}
