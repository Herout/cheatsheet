class Null:
    """Null objects always and reliably "do nothing." The object accepts any method or property access.
    
    Replace this:
        def compute(x, y):
            try: "lots of computation here to return some appropriate object"
            except SomeError: return None
    
    With this:
        def compute(x, y):
            try: "lots of computation here to return some appropriate object"
            except SomeError: return Null( )
    
    And then you can safely do, without raising an exception:
        for x in xs:
            for y in ys:
            compute(x, y).somemethod(y, x)
    
    These also become equivalent
        someVal == Nothing
        isinstance(someVal, Null)
            
    """

    def __init__(self, *args, **kwargs): pass
    def __call__(self, *args, **kwargs): return self
    def __repr__(self): return "Null(  )"
    def __nonzero__(self): return 0

    def __getattr__(self, name): return self
    def __setattr__(self, name, value): return self
    def __delattr__(self, name): return self